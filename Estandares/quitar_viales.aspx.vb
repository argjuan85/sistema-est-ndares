﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Estandares_quitar_viales
    Inherits System.Web.UI.Page
    Dim codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(64, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                Dim es As Estandares
                Label1.Text = ""
                Label2.Text = ""
                Label3.Text = ""

                codigo = Request.QueryString("ID")
                codigo = DecryptText(Request.QueryString("ID"))
                Button1.Attributes.Add("onclick", " return confirma_descarga();")

                es = New Estandares()
                es.Estandares(conexion)

               
                'si el estandar se cerro no puedo quitar viales

                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se puede quitar viales"

                End If
                'por seguridad hago doble control del sector al que pertenece el estandar

                If (es.obtenersector(codigo) <> Session("sector")) Then
                    Form1.Visible = False
                    Button1.Visible = False


                End If
            Catch
                Label2.Text = "Error al cargar datos de los viales"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If


    End Sub

    Sub descarga(ByVal n_vial As Integer)
        Dim r As Boolean
        Dim es As Estandares
        Dim v As Viales
        Dim idv As Integer
        Dim l As logs
        Dim estado_viejo As String
        Dim xnum As String
        Dim xlot1 As String
        Dim xnom1 As String
        Dim xfecha As String


        Try
            v = New Viales()
            v.Viales(conexion)
            es = New Estandares()
            es.Estandares(conexion)
            idv = v.obteneridvial(n_vial, codigo)
            'optimizar
            xlot1 = es.obtenerlote(codigo)
            xnom1 = es.obtenernombre(codigo)
            estado_viejo = v.obtenerestado(idv)
            If (idv <> "-1") Then
                If ((v.obtenerestado(idv) <> "D") And (v.obtenerestado(idv) <> "U")) Then
                    'label
                    Label2.Text = "El estado del vial no es disponible o en uso , no se puede realizar la descarga"
                Else

                    'cambio el estado del vial 
                    r = v.cambiarestadovial(idv, "R")
                    xnum = v.obtener_numerovial(idv)

                    'log
                    l = New logs()
                    l.logs(conexion)


                    'se puede agregar errro aca si no cambia el estado
                    If (r) Then


                        'debo actualizar el stock
                        r = es.actualizarstock(codigo)


                        If (r = "1") Then



                            'log actualizacion de stock

                            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por descarga de vial: " + xnum, "Estandares", codigo, xnom1 + " - " + xlot1)



                            'chequeo si se acabo el estándar con este envio
                            r = es.chequeocierre(codigo)

                            If (r = 0) Then
                                'log de cierre

                                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado de V a F", "Estandard", codigo, xnom1 + " - " + xlot1)


                                Label3.Text = "El stock del estándar es 0. Se cambió a estado F"
                            End If
                            Label1.Text = "Se realizó la descarga correctamente."
                        Else
                            Label2.Text = "Error al actualizar stock del vial."
                        End If
                    Else
                        Label2.Text = "No se puede cambiar el estado del vial seleccionado."
                    End If
                End If
            Else
                Label2.Text = "Ingreso un numero de vial inválido."
            End If






        Catch ex As Exception
            Label2.Text = "Error al realizar la descarga"
        End Try
    End Sub
    Sub corregir_cantidad(ByVal cantidad_nueva As String)
        Dim r As Boolean
        Dim es As Estandares
        Dim v As Viales
        Dim idulvial As Integer
        Dim cont As Integer = 0
        Dim cont2 As Integer = 0
        Dim cont3 As Integer = 0
        Dim cont4 As Integer = 0
        Dim aux As Integer
        Dim band As String = 0
        Dim band1 As String = 0
        Dim estado_viejo As String
        'para logs
        Dim l As logs
        Dim nomreg As String


        'log
        l = New logs()
        l.logs(conexion)

        Dim xnum As String
        Dim XID As String
        Dim nom As String
        Dim lot As String
        Dim xfecha As String
        Try
            v = New Viales()
            v.Viales(conexion)
            es = New Estandares()
            es.Estandares(conexion)

            'cuento la cantidad de viales del estandar sin importar el estado
            cont = es.obtenercantidadviales(codigo)

            'verifico cuantos viales disponibles al final del estandar tengo
            cont2 = v.obtenercantultimovial(codigo)


            'cantidad de estandares a eliminar
            aux = cont - cantidad_nueva

            'si tengo la cantidad suficiente de disponibles procedo a eliminar
            If ((aux > 0) And (aux <= cont2)) Then
                cont3 = aux
                While (cont3 > "0")

                    'obtengo el  ultimo vial disponible del estandar

                    idulvial = v.obteneridultimovial(codigo)

                    'IMPORTANTE : necesito que el ultimo vial del estandar este disponible, si tiene otro estado pueden quedar inconsistencias y no debo permitirlo

                    If (v.obtenerestado(idulvial) <> "D") Then
                        ' no puedo seguir
                        band = 1

                    Else
                    

                        'log eliminacion 
                        nomreg = es.obtenernombre1(codigo) + " Vial N°: " + v.obtener_numerovial(idulvial)
                        'prosigo y elimino
                        v.BorrarVial(idulvial)

                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Vial por corrección de cantidad", "Viales", idulvial, nomreg)

                        xnum = v.obtener_numerovial(idulvial)
                        XID = idulvial
                    End If
                    cont3 -= 1
                End While

                'debo actualizar el stock


                nom = es.obtenernombre(codigo)
                lot = es.obtenerlote(codigo)

                r = es.actualizarstock(codigo)

                'log actualizacion de stock

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por correccion de cantidad de viales, cantidad nueva: " + cantidad_nueva, "Estandares", codigo, nom + " - " + lot)


                'actualizo la cantidad pedida 
                r = es.actualizacantidad(codigo)


                'registrolog



                l = New logs()
                l.logs(conexion)



                'chequeo si se acabo el estándar con este envio
                estado_viejo = es.obtenerestado(codigo)
                r = es.chequeocierre(codigo)



                If (r = 0) Then

                    'log de cierre

                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                    l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado de " + estado_viejo + " a F", "Estandares", codigo, nom + " - " + lot)

                    Label2.Text = "El stock del estándar es 0. Se cambió a estado F"
                End If
                If band Then
                    Label2.Text = "No es posible corregir la cantidad de viales de este estándar"
                Else
                    Label1.Text = "Se realizó la corrección de cantidad correctamente."
                End If

            Else
                Label2.Text = "El estandar, no dispone de viales disponibles suficientes para realizar el cambio. No se puede corregir cantidad"
            End If
          



        Catch ex As Exception
            Label2.Text = "Error al realizar la correccion de cantidades!"
        End Try
    End Sub



    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged

        If DropDownList1.SelectedValue = "1" Then
            label_cantidad.InnerText = "Número de Vial"
            titulo_seccion.InnerText = "Ingrese el número de vial a descargar"
            cantidad.Value = ""
        ElseIf DropDownList1.SelectedValue = "2" Then
            label_cantidad.InnerText = "Cantidad Total de viales"
            titulo_seccion.InnerText = "Ingrese la cantidad total correcta"
            cantidad.Value = ""
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If (DropDownList1.SelectedValue = 1) Then
            descarga(cantidad.Value)
        Else
            corregir_cantidad(cantidad.Value)
        End If
        cantidad.Value = ""
        DropDownList1.SelectedIndex = "0"
        'Response.Redirect("../Estandares/modifica_estandar.aspx?ID=" + codigo)
    End Sub
End Class
