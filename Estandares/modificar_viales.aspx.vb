﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Estandares_modificar_viales
    Inherits System.Web.UI.Page
    Dim codigo As String

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        codigo = DecryptText(Request.QueryString("ID"))
        If (nombre1.Value > 0) And (nombre1.Value <= maxviales) Then
            agregarviales()
            nombre1.Value = ""
        Else
            If (nombre1.Value > 50) Then
                Label2.Text = "Solo puede cargar hasta 50 viales"
            Else
                Label2.Text = "No puede ingresar valores negativos o nulos"
            End If

        End If

    End Sub

    Sub agregarviales()
        Dim y As Viales
        Dim n As nombres
        Dim r As String
        Dim es As Estandares
        Dim stock_actual As Integer
        Dim cantidad As Integer
        Dim auxcant As String
        Dim cantidad_p As Integer
        Dim cont As Integer = 0
        Dim presentacion As String
        Dim xnom As String
        Dim l As logs
        Dim xlot As String
        Dim nomreg As String
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)
        n = New nombres()
        n.Nombres(conexion)

        Try
            Label1.Text = ""
            Label2.Text = ""
            cantidad = nombre1.Value
            auxcant = cantidad
            y = New Viales()
            y.Viales(conexion)
            es = New Estandares()
            es.Estandares(conexion)
            
            xnom = n.obtenernombre(es.obtenernombre(codigo))
            xlot = es.obtenerlote(codigo)

            presentacion = es.obtenerpresentacion(codigo)
            cantidad_p = es.Obtenercantidadpedida(codigo)
            stock_actual = es.obtenerstock(codigo)


            'genero los nuevos viales
            r = y.generaviales(cantidad, codigo, presentacion, cantidad_p)


            'log alta vial
            Dim cadenas As String()

            cadenas = r.Split("-")
            Dim word As String
            For Each word In cadenas
                'log alta vial
                nomreg = es.obtenernombre1(codigo) + " Vial N°: " + y.obtener_numerovial(word)

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Alta Vial.", "Viales", word, nomreg)

            Next



            'Actualizo el stock del estándar
            r = es.actualizarstock(codigo)
            If (r = "1") Then
                'log actualizacion de stock

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por agregado de: " + auxcant + " viales.", "Estandares", codigo, xnom + " - " + xlot)


                'nueva_cantidad = cantidad_p + cantidad
                r = es.actualizacantidad(codigo)

                Label1.Text = "Se agregaron correctamente los viales"
            Else
                Label2.Text = "Se produjo un error al actualizar el stock del estandar"
            End If


        Catch ex As Exception
            Label2.Text = "Se produjo un error al agregar el vial"
        End Try
    End Sub

  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(32, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(Request.QueryString("ID"))
                Dim es As Estandares
                es = New Estandares()
                es.Estandares(conexion)
                'si el estandar se cerro no puedo seguir consumiento

                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se puede agregar viales"

                End If

                'por seguridad hago doble control del sector al que pertenece el estandar

                If (es.obtenersector(codigo) <> Session("sector")) Then
                    Form1.Visible = False
                    Button1.Visible = False

                End If

                Button1.Attributes.Add("onclick", " return confirma_grabar();")
            Catch
                Label2.Text = "Error al cargar datos de los viales"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If

    End Sub
End Class
