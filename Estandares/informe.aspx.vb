﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports globales

    Partial Class Estandares_vb
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(8, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

        End If

    End Sub

        Private Function GetData(ByVal query As String) As DataTable
            Dim conString As String = ConfigurationManager.ConnectionStrings("EstandaresConnectionString").ConnectionString
            Dim cmd As New SqlCommand(query)
            Using con As New SqlConnection(conString)
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con

                    sda.SelectCommand = cmd
                    Using dt As New DataTable()
                    sda.Fill(dt)

                        Return dt
                    End Using
                End Using
            End Using
        End Function
        Protected Sub GenerateReport(ByVal sender As Object, ByVal e As EventArgs) Handles btnReport.Click
        Dim sql As String
        Dim ult As String
        Dim cantidad As Integer = 0
        Dim indice As Integer = 0
        Dim checks As Boolean = False
        Try
            sql = "SELECT CodEst ,nombres.nombre, LotePro, Tipo, Prese, Uni, Est, FeVto, Acond, Stock FROM Estandard inner join nombres on nombre_id= Estandard.nombre where Estandard.Borrado = 'N' and Estandard.Tipo = '" & DropDownList1.Text & "' and Estandard.sector = '" & Session("sector") & "'"
            If (CheckBox1.Checked = True) And (checks = False) Then
                sql = sql & " and (Estandard.Est = 'V'"

                checks = True
            ElseIf (CheckBox1.Checked = True) Then
                sql = sql & " or Estandard.Est = 'V'"
            End If
            If (CheckBox2.Checked = True) And (checks = False) Then
                sql = sql & " and (Estandard.Est = 'D'"

                checks = True
            ElseIf (CheckBox2.Checked = True) Then
                sql = sql & " or Estandard.Est = 'D'"

            End If
            If (CheckBox3.Checked = True) And (checks = False) Then
                sql = sql & " and (Estandard.Est = 'H'"

                checks = True
            ElseIf (CheckBox3.Checked = True) Then
                sql = sql & " or Estandard.Est = 'H'"

            End If
            If (CheckBox4.Checked = True) And (checks = False) Then
                sql = sql & " and (Estandard.Est = 'F'"

                checks = True
            ElseIf (CheckBox4.Checked = True) Then
                sql = sql & " or Estandard.Est = 'F'"

            End If
            If (CheckBox5.Checked = True) And (checks = False) Then
                sql = sql & " and (Estandard.Est = 'I'"
                checks = True
            ElseIf (CheckBox5.Checked = True) Then
                sql = sql & " or Estandard.Est = 'I'"

            End If
            ult = sql.Substring(sql.Length - 1, 1)
            If ((ult <> ")") And checks = True) Then
                sql = sql & ")"
            End If
            Dim dr As DataRow = GetData(sql).Rows(0)
            Dim document As New Document(PageSize.A4.Rotate(), 88.0F, 88.0F, 10.0F, 10.0F)
            Dim NormalFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)
            Using memoryStream As New System.IO.MemoryStream()
                Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
                Dim phrase As Phrase = Nothing
                Dim cell As PdfPCell = Nothing
                Dim table As PdfPTable = Nothing
                Dim color__1 As Color = Nothing
                Dim test As Integer = 0
                Dim j, k As Integer
                Dim ds As DataSet
                Dim cn As SqlConnection
                Dim cm As SqlCommand
                Dim da As SqlDataAdapter
                Dim aux As String = 0
                Dim dtres As SqlDataReader
                Dim es As Estandares
                Dim pagina As Integer = 1

                'traigo los valores de la bd
                'sql = "SELECT CodEst ,nombre, LotePro, Tipo, Prese, Uni, Est, FeVto, Acond, Stock FROM Estandard where Estandard.Tipo = '" & DropDownList1.Text & "' and Estandard.sector = '" & Session("sector") & "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)


                document.Open()

                ' este bucle determina el numero de paginas del documento, registro actual guarda los registros ya impresos y lo compara versus el total que trae la consulta
                While (registro_actual < ds.Tables(0).Rows.Count)

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F, 0.33F, 0.33F})

                    ' para armar la cabecera
                    Dim tipoestand As String
                    Dim fogl As String
                    Select Case DropDownList1.Text
                        Case "EP"
                            tipoestand = "Estándares Primarios"
                            fogl = informe_EP
                        Case "ES"
                            tipoestand = "Estándares Secundarios"
                            fogl = informe_ES
                        Case "PP"
                            tipoestand = "Patrón Primario"
                            fogl = informe_PP
                        Case "ET"
                            tipoestand = "Estándares de Trabajo"
                            fogl = informe_ET
                        Case Else

                    End Select

                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Pagina : " & pagina & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    phrase.Add(New Chunk(fogl & vbLf & vbLf, FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)))
                    phrase.Add(New Chunk("Departamento : Control de Calidad " & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Fecha de emisión : " & String.Format("{0:dd/MM/yyyy H:mm:ss}", Date.Now()) & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))

                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)



                    'Columna2



                    phrase = New Phrase()
                    phrase.Add(New Chunk("Listado de " + tipoestand & vbLf & vbLf, FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                    table.AddCell(cell)
                    document.Add(table)

                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador


                    'Linea
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    DrawLine(writer, 25.0F, document.Top - 70.0F, document.PageSize.Width - 25.0F, document.Top - 70.0F, color__1)


                    'Titulos de las columnas
                    table = New PdfPTable(9)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    'Ancho columnas
                    table.SetWidths(New Single() {0.9F, 0.44F, 0.13F, 0.15F, 0.13F, 0.13F, 0.15F, 0.27F, 0.18F})



                    'nombre

                    table.AddCell(PhraseCell(New Phrase("Nombre", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Lote Prov", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Tipo", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Pres", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Unidad", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Estado", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Vto.", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Acondicionamiento", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Viales C.", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))


                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 1, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separacion

                    'Registros 

                    'defino tabla
                    table = New PdfPTable(9)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True

                    'Ancho columnas
                    'table.SetWidths(New Single() {0.9F, 0.44F, 0.13F, 0.15F, 0.18F, 0.15F, 0.15F, 0.2F, 0.18F})
                    table.SetWidths(New Single() {0.9F, 0.44F, 0.13F, 0.15F, 0.13F, 0.13F, 0.15F, 0.27F, 0.18F})

                    table.SpacingBefore = 8.0F
                    phrase = New Phrase()

                    For j = 0 To 30

                        'este condicional es para cortar en la ultima pagina cuando tengo menos de 30 registros
                        If (registro_actual + j <= (ds.Tables(0).Rows.Count - 1)) Then


                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                            For k = 0 To ds.Tables(0).Rows(registro_actual + j).ItemArray.Length - 1

                                Select Case k

                                    Case "9"
                                        aux = 0
                                        es = New Estandares()
                                        es.Estandares(conexion)
                                        dtres = es.Obtenervialdisponible(ds.Tables(0).Rows(registro_actual + j).ItemArray(0).ToString)

                                        While (dtres.Read())
                                            aux += 1
                                        End While
                                        dtres.Close()

                                        table.AddCell(PhraseCell(New Phrase(aux, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                        'table.AddCell(PhraseCell(New Phrase("viab", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                        'table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))


                                    Case "1"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "2"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "3"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "4"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "5"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "6"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "7"

                                        table.AddCell(PhraseCell(New Phrase(invertirfecha(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "8"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case Else
                                        '...
                                End Select


                            Next


                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                        Else
                            'relleno con espacios 

                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                        End If



                    Next




                    'agrego lo generado en el case
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})

                    phrase = New Phrase()

                    'en base a la cantidad de registros son los espacios en blanco que genero para que el pie de pagina quede bien
                    'como la cant de registros por pagina es 30 hago modulo 30 por si son consultas con muchos registros resultantes
                    cantidad = ds.Tables(0).Rows.Count

                    If (cantidad < "31") Then
                        If ((cantidad > "0") And (cantidad < "6")) Then
                            indice = "5"
                        ElseIf ((cantidad > "5") And (cantidad < "11")) Then
                            indice = "5"
                        ElseIf ((cantidad > "10") And (cantidad < "16")) Then
                            indice = "4"

                        ElseIf ((cantidad > "15") And (cantidad < "21")) Then
                            indice = "3"

                        ElseIf ((cantidad > "20") And (cantidad < "26")) Then
                            indice = "2"

                        ElseIf ((cantidad > "25") And (cantidad < "31")) Then
                            indice = "1"
                        End If
                    Else
                        indice = "1"
                        cantidad = cantidad - "30"
                    End If




                    For k = 0 To indice

                        phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        'phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                        ' phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                    Next

                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)

                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'fin separacion



                    'pie de pagina


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk(sops, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'agrego una nueva pagina

                    document.NewPage()
                    pagina = pagina + 1

                    registro_actual += 30
                End While

                document.Close()
                Dim bytes As Byte() = memoryStream.ToArray()
                memoryStream.Close()
                cn.Close()
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", "attachment; filename=Estandares.pdf")
                Response.ContentType = "application/pdf"
                Response.Buffer = True
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.BinaryWrite(bytes)
                Response.[End]()
                Response.Close()
            End Using
        Catch ex As Exception
            If (ex.Message = "No hay ninguna fila en la posición 0.") Then
                Label2.Text = "No hay estandares que cumplan con el criterio seleccionado"
            Else
                Label2.Text = "Error al generar el informe"
            End If


        End Try

    End Sub

        Private Shared Sub DrawLine(ByVal writer As PdfWriter, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal color As Color)
            Dim contentByte As PdfContentByte = writer.DirectContent
            contentByte.SetColorStroke(color)
            contentByte.MoveTo(x1, y1)
            contentByte.LineTo(x2, y2)
            contentByte.Stroke()
        End Sub
        Private Shared Function PhraseCell(ByVal phrase As Phrase, ByVal align As Integer) As PdfPCell
            Dim cell As New PdfPCell(phrase)
            cell.BorderColor = Color.WHITE
            cell.VerticalAlignment = PdfCell.ALIGN_TOP
            cell.HorizontalAlignment = align
            cell.PaddingBottom = 2.0F
            cell.PaddingTop = 0.0F
            Return cell
        End Function
        Private Shared Function ImageCell(ByVal path As String, ByVal scale As Single, ByVal align As Integer) As PdfPCell
            Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path))
            image.ScalePercent(scale)
            Dim cell As New PdfPCell(image)
            cell.BorderColor = Color.WHITE
            cell.VerticalAlignment = PdfCell.ALIGN_TOP
            cell.HorizontalAlignment = align
            cell.PaddingBottom = 0.0F
            cell.PaddingTop = 0.0F
            Return cell
        End Function

End Class







