﻿Imports System
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Data.SqlClient 'no nos va traer problema con la cantidad de conexiones, como si sucede con odbc, odbc no nos trae drama con la paginacion por ejemplo
Imports System.Data
Imports System.IO
Imports System.Data.Sql
Imports globales
Imports Encryption64



Partial Class Estandares_modifica_estandar
    Inherits System.Web.UI.Page
    Public codigo As String
    Dim xaux As String = "0"
    Dim band As Integer = "1"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(16, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                Dim es As Estandares
                Dim uni As String

                xaux = "0"
                codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                Button12.Attributes.Add("onclick", " return confirma_elimina();")
                Label1.Text = ""
                Label2.Text = ""
                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarestandar(codigo) 'carga la interfaz

                End If

                es = New Estandares()
                es.Estandares(conexion)
                uni = es.obtenerunidad(codigo)
                label_presentacion.InnerText = "Presentacion (" & uni & ")"
                label_stock.InnerText = "Stock (" & uni & ")"
                label_alarma_stock.InnerText = "Alarma Stock (" & uni & ")"
                label_remanente.InnerText = "Remanente Vial Actual (" & uni & ")"
                'por seguridad hago doble control del sector al que pertenece el estandar

                If ((es.obtenersector(codigo) <> Session("sector"))) Then

                    Button1.Visible = False
                    Button8.Visible = False
                    Button9.Visible = False
                    Button10.Visible = False
                    Button12.Visible = False

                End If



                ' oculto ciertos campos para los analistas
                If (Session("nivel") = nivel4) Then
                    Button1.Visible = False
                    ' idalarma_stock.Visible = False
                    ' label_alarma_stock.Visible = False
                    'idcantidad_pedida.Visible = False
                    ' label_cantidad_pedida.Visible = False
                    ' idcod_qad.Visible = False
                    ' label_cod_qad.Visible = False
                    ' idfecha_creacion.Visible = False
                    ' label_fecha_creacion.Visible = False
                    ' idfecha_ingreso.Visible = False
                    ' label_fecha_ingreso.Visible = False
                    ' idobservaciones.Visible = False
                    ' label_observaciones.Visible = False

                    'los restantes son de solo lectura
                    idnombre.Enabled = False
                    idlote.Disabled = True
                    proveedor.Enabled = False
                    idtipo.Disabled = True
                    idpresentacion.Disabled = True
                    idcod_qad.Disabled = True
                    idfecha_ingreso.Disabled = True
                    idfecha_creacion.Disabled = True
                    idobservaciones.Disabled = True
                    idalarma_stock.Disabled = True
                    idcantidad_pedida.Disabled = True
                    idunidad.Disabled = True
                    idn_cas.Disabled = True
                    idacondicionamiento.Disabled = True
                    idfecha_vencimiento.Disabled = True
                    idestado.Disabled = True
                    idcatalogo.Disabled = True
                    idvial_actual.Disabled = True
                    idremanente.Disabled = True
                    idviales_cerrados.Disabled = True
                    idsedronar.Enabled = False
                    idaviso.Enabled = False
                    Button4.Visible = False
                    Button6.Visible = False
                    Button5.Visible = False
                    Button13.Visible = False



                End If

                'user consulta solo puede leer datos , no importa el estado del estandar, no puede tocar nada
                If (Session("nivel") = nivel2) Then
                    Button10.Visible = False
                    Button8.Visible = True  ' habilito para consulta de consumos
                    Button9.Visible = True  ' habilito para consulta de viales
                    Button1.Visible = False
                    Button4.Visible = False
                    Button5.Visible = False
                    Button13.Visible = False
                    Button6.Visible = False
                    idnombre.Enabled = False
                    proveedor.Enabled = False
                    idlote.Disabled = True
                    idn_cas.Disabled = True
                    idcatalogo.Disabled = True
                    idtipo.Disabled = True
                    idpresentacion.Disabled = True
                    idunidad.Disabled = True
                    idfecha_vencimiento.Disabled = True
                    idacondicionamiento.Disabled = True
                    idfecha_ingreso.Disabled = True
                    idalarma_stock.Disabled = True
                    idfecha_creacion.Disabled = True
                    idstock.Disabled = True
                    idremanente.Disabled = True
                    idvial_actual.Disabled = True
                    idviales_cerrados.Disabled = True
                    idobservaciones.Disabled = True
                    idcantidad_pedida.Disabled = True
                    idcod_qad.Disabled = True
                    idestado.Disabled = True
                    idarch.Disabled = True
                    idsedronar.Enabled = False
                    idaviso.Enabled = False

                End If

            Catch
                Label2.Text = "Error al cargar datos del estandar"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try

        End If
         

    End Sub
    Private WithEvents m_PrintDocument As PrintDocument
    Dim count As Integer = 0
    Dim registro_actual As Integer = 0


    Sub buscarestandar(ByVal codigo As String)
        
        Dim u As Estandares

        Try

            u = New Estandares()
            u.Estandares(conexion)
            u = u.Consultarestandares1_test(codigo)

            Button10.Enabled = True ' solicitaron que todos puedan enviar... no estaba planteado asi al principio
            label_certificado.Visible = True
            label_hoja.Visible = True
            Button12.Visible = False
            FileUpload1.Visible = False
            FileUpload2.Visible = False
            confirma_certificado.Text = ""
            confirma_hoja.Text = ""


          


            'antes de traer los datos chequeo consistencia de stock y cantidad entre tabla estandares  y viales
            'If (((u.estado = "H") Or (u.estado = "I") Or (u.estado = "F")) Or ((u.chequeaconsistenciastock(codigo) = True) And (u.chequeaconsistenciacant(codigo) = True))) Then
            If (((u.estado = "H") Or (u.estado = "I") Or (u.estado = "F")) Or ((True = True) And (True = True))) Then

                idstock.Disabled = True
                idremanente.Disabled = True
                idvial_actual.Disabled = True
                idviales_cerrados.Disabled = True

                Select Case u.tipo
                    Case "EP"

                        Hidden3.Value = u.id
                        idnombre.SelectedValue = u.nombre
                        idlote.Value = u.lote
                        proveedor.SelectedValue = u.proveedor
                        idn_cas.Value = u.cas
                        idcatalogo.Value = u.catalogo
                        idtipo.Value = u.tipo
                        idpresentacion.Value = u.presentacion.Replace(",", ".")
                        idunidad.Value = u.unidad.ToLower()   'convertir a minuscula
                        idfecha_vencimiento.Value = invertirfecha(u.fecha_vencimiento)
                        idacondicionamiento.Value = u.acondicionamiento  ' ver para el caso heladera y refrigerador en datos viejos
                        idfecha_ingreso.Value = invertirfecha(u.fecha_ingreso)
                        idcantidad_pedida.Value = u.cantidad_pedida
                        idcod_qad.Value = u.cod_qad
                        idestado.Value = u.estado
                        idarch.Value = u.arch
                        If idarch.Value <> Nothing Then
                            label_certificado.Visible = True
                            Button2.Visible = True
                        End If
                        idstock.Value = u.stock.Replace(",", ".")
                        idfecha_creacion.Value = invertirfecha(u.fecha_creacion)
                        idarch1.Value = u.arch1
                        If idarch1.Value <> Nothing Then
                            label_hoja.Visible = True
                            Button3.Visible = True
                        End If
                        idobservaciones.Value = u.observaciones
                        idalarma_stock.Value = u.alarma_stock
                        label_cantidad_pedida.InnerText = "Cantidad de Viales"
                        If (u.sedronar = "1") Then
                            idsedronar.Checked = True
                        Else
                            idsedronar.Checked = False
                        End If

                        If (u.aviso = "1") Then
                            idaviso.Checked = True
                        Else
                            idaviso.Checked = False
                        End If

                    Case "ES"
                        Button10.Enabled = True
                        Hidden3.Value = u.id
                        idnombre.SelectedValue = u.nombre
                        idlote.Value = u.lote
                        proveedor.SelectedValue = u.proveedor
                        idn_cas.Value = u.cas
                        idcatalogo.Disabled = True
                        idtipo.Value = u.tipo
                        idpresentacion.Value = u.presentacion.Replace(",", ".")
                        idunidad.Value = u.unidad.ToLower()   'convertir a minuscula
                        idfecha_vencimiento.Value = invertirfecha(u.fecha_vencimiento)
                        idacondicionamiento.Value = u.acondicionamiento  ' ver para el caso heladera y refrigerador en datos viejos
                        idfecha_ingreso.Value = invertirfecha(u.fecha_ingreso)
                        idalarma_stock.Value = u.alarma_stock

                        idcantidad_pedida.Value = u.cantidad_pedida
                        idcod_qad.Value = u.cod_qad
                        idestado.Value = u.estado
                        idarch.Value = u.arch
                        If idarch.Value <> Nothing Then
                            label_certificado.Visible = True
                            Button2.Visible = True
                        End If
                        idstock.Value = u.stock.Replace(",", ".")
                        idfecha_creacion.Value = invertirfecha(u.fecha_creacion)
                        idarch1.Value = u.arch1
                        If idarch1.Value <> Nothing Then
                            label_hoja.Visible = True
                            Button3.Visible = True
                        End If
                        idobservaciones.Value = u.observaciones
                        label_fecha_ingreso.InnerText = "Fecha Confección"
                        label_cantidad_pedida.InnerText = "Cantidad de Viales"
                        If (u.sedronar = "1") Then
                            idsedronar.Checked = True
                        Else
                            idsedronar.Checked = False
                        End If
                        If (u.aviso = "1") Then
                            idaviso.Checked = True
                        Else
                            idaviso.Checked = False
                        End If
                    Case "PP"

                        Hidden3.Value = u.id
                        idnombre.SelectedValue = u.nombre
                        idlote.Value = u.lote
                        proveedor.SelectedValue = u.proveedor
                        idn_cas.Value = u.cas
                        idcatalogo.Value = u.catalogo
                        idtipo.Value = u.tipo
                        idpresentacion.Value = u.presentacion.Replace(",", ".")
                        idunidad.Value = u.unidad.ToLower()   'convertir a minuscula
                        idfecha_vencimiento.Value = invertirfecha(u.fecha_vencimiento)
                        idacondicionamiento.Value = u.acondicionamiento  ' ver para el caso heladera y refrigerador en datos viejos
                        idfecha_ingreso.Value = invertirfecha(u.fecha_ingreso)
                        idalarma_stock.Value = u.alarma_stock
                        idcantidad_pedida.Value = u.cantidad_pedida
                        idcod_qad.Value = u.cod_qad
                        idestado.Value = u.estado
                        idarch.Value = u.arch
                        If idarch.Value <> Nothing Then
                            label_certificado.Visible = True
                            Button2.Visible = True
                        End If
                        idstock.Value = u.stock.Replace(",", ".")
                        idfecha_creacion.Value = invertirfecha(u.fecha_creacion)
                        idarch1.Value = u.arch1
                        If idarch1.Value <> Nothing Then
                            label_hoja.Visible = True
                            Button3.Visible = True
                        End If
                        idobservaciones.Value = u.observaciones
                        If (u.sedronar = "1") Then
                            idsedronar.Checked = True
                        Else
                            idsedronar.Checked = False
                        End If
                        If (u.aviso = "1") Then
                            idaviso.Checked = True
                        Else
                            idaviso.Checked = False
                        End If
                    Case "ET"
                        label_cantidad_pedida.InnerText = "Cantidad de Viales"
                        Hidden3.Value = u.id
                        idnombre.SelectedValue = u.nombre
                        idlote.Value = u.lote
                        proveedor.SelectedValue = u.proveedor
                        idn_cas.Value = u.cas
                        idtipo.Value = u.tipo
                        idcatalogo.Disabled = True
                        idpresentacion.Value = u.presentacion.Replace(",", ".")
                        idunidad.Value = u.unidad.ToLower()   'convertir a minuscula
                        idfecha_vencimiento.Value = invertirfecha(u.fecha_vencimiento)
                        idacondicionamiento.Value = u.acondicionamiento  ' ver para el caso heladera y refrigerador en datos viejos
                        idfecha_ingreso.Value = invertirfecha(u.fecha_ingreso)
                        idalarma_stock.Value = u.alarma_stock
                        idcantidad_pedida.Value = u.cantidad_pedida
                        idcod_qad.Value = u.cod_qad
                        idestado.Value = u.estado
                        idarch.Value = u.arch
                        If idarch.Value <> Nothing Then
                            label_certificado.Visible = True
                            Button2.Visible = True
                        End If
                        idstock.Value = u.stock.Replace(",", ".")
                        idfecha_creacion.Value = invertirfecha(u.fecha_creacion)
                        idarch1.Value = u.arch1
                        If idarch1.Value <> Nothing Then
                            label_hoja.Visible = True
                            Button3.Visible = True
                        End If
                        idobservaciones.Value = u.observaciones
                        If (u.sedronar = "1") Then
                            idsedronar.Checked = True
                        Else
                            idsedronar.Checked = False
                        End If
                        If (u.aviso = "1") Then
                            idaviso.Checked = True
                        Else
                            idaviso.Checked = False
                        End If
                    Case Else
                        '...
                End Select

                'solo puedo borrar estandares en estado h siendo administrador
                If ((u.estado = "H") And ((Session("nivel") = nivel1) Or Session("nivel") = nivel3)) Then
                    Button12.Visible = True
                End If


                ' si el estandar no esta vigente no puedo cargar consumos ni envios
                If (idestado.Value <> "V") Then
                    'esto hace q aparezca el boton de envio si hay cambio a vigente de un estandar ES


                    If ((u.tipo) = "ES" And (idestado.Value = "D")) Then
                        Button10.Visible = True
                    End If
                    Button10.Visible = False
                    Button8.Visible = False
                Else
                    'esto hace q aparezca el boton de envio si hay cambio a vigente de un estandar ES
                    If (u.tipo = "ES") Then
                        Button10.Visible = True
                    End If
                    Button8.Visible = True
                End If

                'por requerimiento, los administradores podran cargar envios en estado d
                If (idestado.Value = "D" And (Session("nivel") = nivel1 Or Session("nivel") = nivel3)) Then
                    Button10.Visible = True
                    idpresentacion.Disabled = True
                    idcantidad_pedida.Disabled = True

                End If


                ' estandares en estado I o F no podrán ser modificados. solo se puede consultar informacion
                If (idestado.Value = "F") Or (idestado.Value = "I") Then
                    'Button11.Visible = False
                    Button10.Visible = False
                    Button8.Visible = True  ' habilito para consulta de consumos
                    Button1.Visible = False
                    Button4.Visible = False
                    Button5.Visible = False
                    Button13.Visible = False
                    Button6.Visible = False
                    idnombre.Enabled = False
                    proveedor.Enabled = False
                    idlote.Disabled = True
                    idn_cas.Disabled = True
                    idtipo.Disabled = True
                    idpresentacion.Disabled = True
                    idunidad.Disabled = True
                    idcatalogo.Disabled = True
                    idfecha_vencimiento.Disabled = True
                    idacondicionamiento.Disabled = True
                    idfecha_ingreso.Disabled = True
                    idalarma_stock.Disabled = True
                    idfecha_creacion.Disabled = True
                    idstock.Disabled = True
                    idremanente.Disabled = True
                    idvial_actual.Disabled = True
                    idviales_cerrados.Disabled = True
                    idobservaciones.Disabled = True
                    idcantidad_pedida.Disabled = True
                    idcod_qad.Disabled = True
                    idestado.Disabled = True
                    idarch.Disabled = True
                    idsedronar.Enabled = False
                    idaviso.Enabled = False



                End If

                If (idestado.Value = "H") Then
                    Button9.Visible = False
                End If

                'calculo el vial actual y el remanente del mismo
                If (idestado.Value = "V") Then
                    idvial_actual.Value = u.calculavial_actual(codigo)
                    idremanente.Value = u.calcularemanente(codigo)
                    idremanente.Value = idremanente.Value.Replace(",", ".")
                    idcantidad_pedida.Disabled = True
                    idpresentacion.Disabled = True
                End If

                'calculo los viales cerrados del estandar
                Dim cant As Integer = 0


                idviales_cerrados.Value = u.calculaviales_cerrados(codigo)
                If (idestado.Value <> "V") Then
                    idvial_actual.Disabled = True
                    idremanente.Disabled = True
                    idviales_cerrados.Disabled = True
                End If


                'si el estandar esta vigente y no soy administrador no puedo modificar datos
                If ((u.sector <> Session("sector")) Or ((Session("nivel") = nivel2 Or Session("nivel") = nivel3) And (u.estado = "V"))) Then
                    If (u.sector <> Session("sector")) Then
                        Label2.Text = "El estandar que esta consultando pertenece a otro sector."
                    End If

                    Button1.Visible = False
                    idnombre.Enabled = False
                    idlote.Disabled = True
                    proveedor.Enabled = False
                    idtipo.Disabled = True
                    idpresentacion.Disabled = True
                    idunidad.Disabled = True
                    idn_cas.Disabled = True
                    idacondicionamiento.Disabled = True
                    idfecha_vencimiento.Disabled = True
                    idestado.Disabled = True
                    idvial_actual.Disabled = True
                    idremanente.Disabled = True
                    idviales_cerrados.Disabled = True
                    idalarma_stock.Disabled = True
                    idcod_qad.Disabled = True
                    idcantidad_pedida.Disabled = True
                    idfecha_ingreso.Disabled = True
                    idobservaciones.Disabled = True
                    idfecha_creacion.Disabled = True
                    idsedronar.Enabled = False
                    idaviso.Enabled = False
                    Button4.Visible = False
                    Button6.Visible = False
                    Button5.Visible = False
                    Button13.Visible = False
                End If

            Else
                Label2.Text = "Se detectó una inconsistencia en el stock del estandar o en su cantidad de viales, comunicarse con el administrador."
            End If

           

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim filename As String
            'Ambiente 
            'filename = rutapdf + idarch.Value
            'productivo
            filename = rutapdf + "\certificado\" + codigo + "-" + idarch.Value
            If (File.Exists(filename)) Then
                Response.Clear()
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", idarch.Value))
                Response.ContentType = "application/pdf"
                Response.WriteFile(filename)
                Response.End()
            Else
                filename = rutapdf + "\certificado\" + idarch.Value
                If (File.Exists(filename)) Then
                    Response.Clear()
                    Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", idarch.Value))
                    Response.ContentType = "application/pdf"
                    Response.WriteFile(filename)
                    Response.End()
                End If
            End If
        Catch ex As Exception
            Label2.Text = "Problemas al cargar Archivo: " & ex.Message
        End Try
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim filename As String
            
            filename = rutapdf + "\hoja\" + codigo + "-" + idarch1.Value
            If (File.Exists(filename)) Then
                Response.Clear()
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", idarch1.Value))
                Response.ContentType = "application/pdf"
                Response.WriteFile(filename)
                Response.End()
            Else
                filename = rutapdf + "\hoja\" + idarch1.Value
                If (File.Exists(filename)) Then
                    Response.Clear()
                    Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", idarch1.Value))
                    Response.ContentType = "application/pdf"
                    Response.WriteFile(filename)
                    Response.End()
                End If
            End If
       
        Catch ex As Exception
            Label2.Text = "Problemas al cargar Archivo: " & ex.Message
        End Try
    End Sub


    Sub actualizarestandar(ByVal arch As String, ByVal arch1 As String)
        'estas lineas las coloco para evitar inconvenientes con el trato de decimales con el tipo de datos double tanto en ambiente de desarrollo como subido en el servidor
        Dim ri As New Globalization.CultureInfo("es-ES")
        ri.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = ri
        Dim u As Estandares
        Dim n As nombres
        Dim y As Viales
        Dim r As String
        Dim re As String
        Dim stock As String
        Dim cant As String
        Dim cambio_estado As String = 0
        Dim estado_viejo As String
        Dim l As logs
        Dim nomreg As String
        Dim estado_Actual As String
        Dim sed As String
        Dim avi As String
        Dim xfecha As String
        Try
            'log
            l = New logs()
            l.logs(conexion)
            u = New Estandares()
            u.Estandares(conexion)


            'verifico si hay cambio de estado y si el nivel de user esta autorizado
            re = u.obtenerestado(codigo)
            r = u.validacambioestado(idestado.Value, re, Session("nivel"))
            If (r = True) Then

                If ((u.obtenerestado(codigo) <> "D") And (idestado.Value = "D")) Then
                    stock = idcantidad_pedida.Value * idpresentacion.Value
                    cant = idcantidad_pedida.Value
                    y = New Viales() ' nuevo objeto instancia
                    y.Viales(conexion)

                    r = y.generaviales(cant, Hidden3.Value, idpresentacion.Value, "0")
                    'log alta vial
                    Dim cadenas As String()

                    cadenas = r.Split("-")
                    Dim word As String
                    For Each word In cadenas
                        'log alta vial
                        nomreg = u.obtenernombre1(codigo) + " Vial N°: " + y.obtener_numerovial(word)

                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Alta Vial.", "Viales", word, nomreg)

                    Next
                Else
                    stock = idstock.Value
                End If

                estado_Actual = u.obtenerestado(codigo)

                ' colocar funcion q obtiene estado
                If (estado_Actual <> idestado.Value) Then
                    estado_viejo = estado_Actual
                    cambio_estado = 1
                End If

                ' si pongo el estandar en F cierro los viales.
                If (idestado.Value = "F") Then
                    u.cierraviales(codigo)
                End If

                If (idsedronar.Checked = True) Then
                    sed = "1"

                Else
                    sed = "0"
                End If

                If (idaviso.Checked = True) Then
                    avi = "1"

                Else
                    avi = "0"
                End If

                Select Case idtipo.Value
                    Case "EP"
                        r = u.Actualizarestandar(Hidden3.Value, idnombre.SelectedValue, idlote.Value, proveedor.Text, idcatalogo.Value, idn_cas.Value, idtipo.Value, idpresentacion.Value.Replace(",", "."), idunidad.Value, revertirfecha(idfecha_vencimiento.Value), idacondicionamiento.Value, revertirfecha(idfecha_ingreso.Value), idalarma_stock.Value, idcantidad_pedida.Value, idcod_qad.Value, "N", idestado.Value, arch, stock, revertirfecha(idfecha_creacion.Value), arch1, idobservaciones.Value, Session("sector"), sed, avi)


                    Case "ES"
                        r = u.Actualizarestandar(Hidden3.Value, idnombre.SelectedValue, idlote.Value, proveedor.Text, idcatalogo.Value, idn_cas.Value, idtipo.Value, idpresentacion.Value.Replace(",", "."), idunidad.Value, revertirfecha(idfecha_vencimiento.Value), idacondicionamiento.Value, revertirfecha(idfecha_ingreso.Value), idalarma_stock.Value, idcantidad_pedida.Value, idcod_qad.Value, "N", idestado.Value, arch, stock, revertirfecha(idfecha_creacion.Value), arch1, idobservaciones.Value, Session("sector"), sed, avi)

                    Case "PP"
                        r = u.Actualizarestandar(Hidden3.Value, idnombre.SelectedValue, idlote.Value, proveedor.Text, idcatalogo.Value, idn_cas.Value, idtipo.Value, idpresentacion.Value.Replace(",", "."), idunidad.Value, revertirfecha(idfecha_vencimiento.Value), idacondicionamiento.Value, revertirfecha(idfecha_ingreso.Value), idalarma_stock.Value, idcantidad_pedida.Value, idcod_qad.Value, "N", idestado.Value, arch, stock, revertirfecha(idfecha_creacion.Value), arch1, idobservaciones.Value, Session("sector"), sed, avi)

                    Case "ET"
                        r = u.Actualizarestandar(Hidden3.Value, idnombre.SelectedValue, idlote.Value, proveedor.Text, idcatalogo.Value, idn_cas.Value, idtipo.Value, idpresentacion.Value.Replace(",", "."), idunidad.Value, revertirfecha(idfecha_vencimiento.Value), idacondicionamiento.Value, revertirfecha(idfecha_ingreso.Value), idalarma_stock.Value, idcantidad_pedida.Value, idcod_qad.Value, "N", idestado.Value, arch, stock, revertirfecha(idfecha_creacion.Value), arch1, idobservaciones.Value, Session("sector"), sed, avi)


                    Case Else
                        '...
                End Select

                ' log modificacion de datos
                n = New nombres()
                n.Nombres(conexion)
                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Datos", "Estandares", codigo, n.obtenernombre(idnombre.SelectedValue) + " - " + idlote.Value)


                If (cambio_estado = 1) Then
                    'log cambio de estado
                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                    l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado de " + estado_viejo + " a " + idestado.Value, "Estandares", codigo, n.obtenernombre(idnombre.SelectedValue) + " - " + idlote.Value)

                End If

                Label1.Text = "Se modificaron correctamente los datos del estándar"
                xaux = "1"
            Else
                Label1.Text = "Cambio de estado no permitido o no tiene permisos para hacer cambios de estado"

            End If

        Catch ex As Exception
            Label2.Text = "Hubo un error al actualizar el estándar"
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim tam As Integer
            Dim nom As String
            Dim snom As String
            Dim uni As String
            Dim uni1 As String
            Dim druta As String
            Dim sext As String
            Dim extension As String
            Dim tam1 As Integer
            Dim nom1 As String
            Dim archivo As FileStream
            Dim inputValue As String
            Dim inputValue2 As String
            Dim u As Estandares
            'blanqueo la etiqueta de mensajes por que si actualizan con un mensaje ya emitido trae problemas
            Label1.Text = ""
            'Archivos PDF: si es "//" debo eliminar el pdf q estaba almacenado (guardar en blanco) si los campos vienen en blanco (dejar el que estaba) y si hay carga reemplazar
            If Me.Hidden1.Value = "//" Then
                inputValue = ""
               
            ElseIf Me.Hidden1.Value = Nothing And (FileUpload1.HasFile) Then
                inputValue = FileUpload1.FileName
                'Obtener parametros a utilizar 
                nom = FileUpload1.FileName
                tam = FileUpload1.FileBytes.Length
                'NOTA: EL ARCHIVO PDF PRIMERO LO GUARDO EN UNA CARPETA EN EL 
                'SERVIDOR Y LUEGO LO LEO LOCALMENTE PARA GUARDARLO EN LA BD (FOLDER) 
                sext = Path.GetExtension(nom)
                uni = InStr(nom, ",")
                uni1 = InStr(nom1, ",")
                If ((uni = "0") And (uni1 = "0")) Then
                    If sext = ".pdf" Then
                        'usar esta en ambiente de prueba
                        'Dim ruta As String = Server.MapPath(rutapdf & nom)

                        'este en ambiente productivo
                        Dim ruta As String = rutapdf & "\certificado\" & codigo & "-" & nom

                        'Dim ruta As String = Server.MapPath(rutapdf & nom) 'DIRECCION EN EL SERVIDOR DONDE SE GUARDAR EL ARCHIVO FISICO 

                        'Guardar Archivo en Carpeta Servidor 

                        FileUpload1.SaveAs(ruta)
                        'leer archivo 
                        archivo = New FileStream(ruta, FileMode.Open, FileAccess.Read)
                        Dim imagen(tam) As Byte
                        archivo.Read(imagen, 0, tam)
                        archivo.Close()
                    Else
                        extension = "no valida"
                    End If
                Else
                    Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
                End If
                Else
                    'debo eliminar
                    If idarch.Value = "-1" Then
                        'necesito el nombre de archivo guardaddo
                        u = New Estandares()
                        u.Estandares(conexion)
                        Dim archivos As SqlDataReader = Nothing
                        Dim bande As String = "0"

                        snom = u.obtenerarch(codigo)
                        'verifico si el mismo archivo esta asociado a otro registro en tal caso solo elimino la ruta
                        archivos = u.obtener_estandares_certificados(snom)



                        If (Not archivos Is Nothing) Then
                            If (archivos.HasRows) Then
                                While archivos.Read()
                                    If (archivos(0) <> codigo) Then
                                        bande = "1"
                                    End If
                                End While
                            End If
                            archivos.Close()
                        End If




                        'End If
                        'borro fisicamente solo si esta asociado a este estandar
                    If (bande = "0") Then
                        'rutas fisicas fuera 
                        druta = rutapdf & "\certificado\" & codigo & "-" & snom
                        If (File.Exists(druta)) Then
                            File.Delete(druta)
                        Else
                            druta = rutapdf & "\certificado\" & snom
                            File.Delete(druta)
                        End If

                    End If

                    'test dentro de la carpeta del proyeto
                    'File.Delete(Server.MapPath(rutapdf & snom))
                    inputValue = ""
                Else
                    inputValue = idarch.Value
                End If

                End If
                If Me.Hidden2.Value = "//" Then
                    inputValue2 = ""
                ElseIf Me.Hidden2.Value = Nothing And (FileUpload2.HasFile) Then
                    inputValue2 = FileUpload2.FileName
                    'Obtener parametros a utilizar 
                    nom1 = FileUpload2.FileName
                    tam1 = FileUpload2.FileBytes.Length
                    'NOTA: EL ARCHIVO PDF PRIMERO LO GUARDO EN UNA CARPETA EN EL 
                    'SERVIDOR Y LUEGO LO LEO LOCALMENTE PARA GUARDARLO EN LA BD (FOLDER) 
                    sext = Path.GetExtension(nom1)
                    uni = InStr(nom, ",")
                    uni1 = InStr(nom1, ",")
                    If ((uni = "0") And (uni1 = "0")) Then
                        If sext = ".pdf" Then
                            'usar esta en ambiente de prueba
                            'Dim ruta As String = Server.MapPath(rutapdf & nom1)

                            'este en ambiente productivo
                            Dim ruta As String = rutapdf & "\hoja\" & codigo & "-" & nom1

                            'Dim ruta As String = Server.MapPath(rutapdf & nom1) 'DIRECCION EN EL SERVIDOR DONDE SE GUARDAR EL ARCHIVO FISICO 
                            'Guardar Archivo en Carpeta Servidor 
                            FileUpload2.SaveAs(ruta)
                            'leer archivo 
                            archivo = New FileStream(ruta, FileMode.Open, FileAccess.Read)
                            Dim imagen(tam1) As Byte
                            archivo.Read(imagen, 0, tam1)
                            archivo.Close()
                        Else
                            extension = "no valida"
                        End If
                    Else
                        Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
                    End If
                Else
                    'debo eliminar
                    If idarch1.Value = "-1" Then
                        'necesito el nombre de archivo guardaddo
                        u = New Estandares()
                        u.Estandares(conexion)
                        Dim archivos1 As SqlDataReader
                        Dim bande As String = "0"
                        snom = u.obtenerarch1(codigo)

                        'verifico si el mismo archivo esta asociado a otro registro en tal caso solo elimino la ruta
                        archivos1 = u.obtener_estandares_hojas(snom)

                        If (Not archivos1 Is Nothing) Then
                            If (archivos1.HasRows) Then
                                While archivos1.Read()
                                    If (archivos1(0) <> codigo) Then
                                        bande = "1"
                                    End If
                                End While
                            End If
                            archivos1.Close()
                        End If




                        'borro fisicamente solo si esta asociado a este estandar
                        If (bande = "0") Then
                            'rutas fisicas fuera 
                        druta = rutapdf & "\hoja\" & codigo & "-" & snom
                        If (File.Exists(druta)) Then
                            File.Delete(druta)
                        Else
                            druta = rutapdf & "\hoja\" & snom
                            File.Delete(druta)
                        End If


                        End If



                        'rutas fisicas fuera 
                        'druta = rutapdf & "\" & snom
                        'File.Delete(druta)
                        'File.Delete(Server.MapPath(rutapdf & snom))
                        inputValue2 = ""
                    Else
                        inputValue2 = idarch1.Value
                    End If
                End If
                If (Page.IsPostBack) Then
                    uni = InStr(nom, ",")
                    uni1 = InStr(nom1, ",")
                    If ((uni = "0") And (uni1 = "0")) Then
                        If extension <> "no valida" Then
                            actualizarestandar(inputValue, inputValue2)
                            'vuelvo a cargar la interfaz para que tome los cambios
                            buscarestandar(codigo)
                        Else
                            Label2.Text = "Solo se pueden adjuntar archivos con extensión PDF"
                        End If
                    Else
                        Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
                    End If
                End If
        Catch ex As Exception
            Label2.Text = "Problemas al Guardar Archivo: " & ex.Message
        End Try
    End Sub

  
    
    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        FileUpload1.Visible = True
        confirma_certificado.Text = "Debe confirmar el cambio presionando el botón grabar"
    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        FileUpload2.Visible = True
        confirma_hoja.Text = "Debe confirmar el cambio presionando el botón grabar"
    End Sub

    Protected Sub Button8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button8.Click
        Response.Redirect("../Consumos/consulta_consumos.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button10.Click
        Response.Redirect("../Envios/consulta_envios.aspx?ID=" + EncryptText(codigo))
    End Sub
   

    Protected Sub Button12_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim u As Estandares
        Dim r As String
        Dim n As nombres
        Dim nomreg As String
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)

        u = New Estandares()
        u.Estandares(conexion)
        n = New nombres()
        n.Nombres(conexion)


        If (u.obtenerestado(codigo).ToLower = "h") Then
            r = u.Borrarestandar(codigo)
            'log eliminacion

            nomreg = "Estandar: " + n.obtenernombre(u.obtenernombre(codigo)) + " Lote: " + u.obtenerlote(codigo)


            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Estandar", "Estandares", codigo, nomreg)

            Response.Redirect("consulta_estandares.aspx")
        Else
            ' no se puede eliminar
            Label1.Text = "Solo se pueden eliminar estándares en estado H"

        End If

    End Sub

    Protected Sub Button9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button9.Click
        Response.Redirect("consulta_viales.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button13_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button13.Click
        idarch.Value = "-1"
        Button2.Visible = False
        confirma_certificado.Text = "Debe confirmar el cambio presionando el botón grabar"
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        idarch1.Value = "-1"
        Button3.Visible = False
        confirma_hoja.Text = "Debe confirmar el cambio presionando el botón grabar"
    End Sub
End Class
