﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="quitar_viales.aspx.vb" Inherits="Estandares_quitar_viales" title="Sistema Estándares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <link rel="stylesheet" href="../css/formularios.css" type="text/css" />
       <!-- librerias calendario y css -->
       <link rel="stylesheet" href="../Estandares/css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="../Estandares/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="../Estandares/js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
          
 <link href="../js/vanadium/style.css" rel="Stylesheet" type="text/css" />
   
   <script type="text/javascript">
        function confirma_descarga() {
           return confirm('Confirma la descarga del vial?');
        }
    </script>
   
    <script>
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="consulta_viales.aspx?"+ arr[0] } 



</script>  
   

    <div id="contenedor1">
    <div id="contenedorenvios">
    <div id="titulo_seccion">
    <label runat="server" id="titulo_seccion">Seleccione Motivo</label>
    </div>
    <form id="Form1"  runat ="server"> 
    
    <ul>
    <li class="paneles">
    <div>
  
     <span class="tercio">
     
      
   
    </span>
    <span class="dostercios">
     
         <label for="proveedor">Motivo</label>
       <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" class=":required :only on blur">
            <asp:ListItem Value="">Seleccione...</asp:ListItem>
            <asp:ListItem Value="1">Descarga Por Rotura</asp:ListItem>
            <asp:ListItem Value="2">Corrección Cantidad Total</asp:ListItem>
          
        </asp:DropDownList>
   
    </span>
 
  
 </div>
 </li>
   </ul>
   
                   
    <ul>  
    <li class="paneles">
   
    <div>
    
      <span class="tercio">
      
        <label runat="server" id="label_cantidad">Cantidad</label>
            <input id="cantidad" runat="server" name="cantidad" value="" class=":required :integer :only_on_blur"/>
        
     
    </span>
 
   
  </div>
  </li>
  </ul>
    
  
 
    
    <ul>
    <li class="panel_boton">
        <div>
      <span class="boton">
           <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" >
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
      </span></div>
    </li>
    </ul>
              
                   
  
 

  

    </form>
       
  </div>
  </div>
  <br >
  <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
     <div id="error">
      <asp:Label ID="Label2" runat="server" ></asp:Label>
     </div>
      <div id="notice">
      <asp:Label ID="Label3" runat="server" ></asp:Label>
     </div>
</asp:Content>

