﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_estandar.aspx.vb" Inherits="Estandares_Default" %>

  

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- librerias calendario y css -->
       <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
       

   
<script type="text/javascript" src="js/jquery.numeric.js"></script>

      <!--  calendarios  --> 
    <script type="text/javascript" >
    
	$(document).ready(function(){
	       $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	    
		$("#<%= fecha_ingreso.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		//Con este codigo indico que la fecha de ingreso no puede ser menor a la pedido
	

		$("#<%= fecha_vencimiento.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' , changeMonth: true, changeYear: true ,yearRange: '-10:+10', minDate: 'today', stepMonths: 0}).val();
		$("#<%= nombre.ClientID %>").removeClass(':required');
		
		
			 $('#ctl00_ContentPlaceHolder1_Button1').click(function(){
   if($("#<%= fecha_ingreso.ClientID %>").val().length < 1) {
    alert("El campo 'fecha ingreso/confección' es obligatorio");
    return false;
}
   
   });
		
		
	
});



   </script>
    
   
    <form id="form2" runat="server">
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
 Ingrese los datos del Estándar
 </div>
    <ul>
    <li class="paneles">
    <div>
    <span class="completo">
    <label for="nombre">Nombre(*)</label>
    
    <asp:DropDownList ID="nombre" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="nombre" DataValueField="nombre_id" 
            class=":required :only_on_blur">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    
      
    </span>
 
    <span class="tercio">
    <label for="tipo">Tipo Estandar</label>
    <input id="tipo" name="tipo" value="" runat="server" readonly="readonly" 
            maxlength="50"/>
     
    </span>
 
    <span class="dostercios">
      <label for="presentacion">Presentacion(*)</label>
      <input id="presentacion" name="presentacion"  value="" runat="server" 
            class="imediano :required :float  :only_on_blur" maxlength="10"/>
      
    </span>
 
    <span class="tercio">
      <label for="n_cas">Nº de CAS</label>
       <input id="n_cas" name="n_cas" value="" runat="server" maxlength="50" />
      
    </span>
 
   <span class="dostercios">
      <label for="cod_qad">Código Qad</label>
    <input id="cod_qad" name="cod_qad" value="" runat="server" maxlength="50" />
    
    
    </span>
    
      
 
 
   
 
    <span class="dostercios">
    
      <label for="fecha_ingreso" runat="server" id="label_fecha_ingreso">Fecha Ingreso(*)</label>
      <input id="fecha_ingreso" name="fecha_ingreso" value="" runat="server" 
             readonly="readonly"/>
            
       
    </span>
    
        <span class="tercio">
      <label for="fecha_vencimiento">Fecha de Vencimiento</label>
      <input id="fecha_vencimiento" name="fecha_vencimiento" value="" runat="server" 
            readonly="readonly"/>
     
    </span>
     
      <span class="completo">
      <label for="observaciones">Observaciones</label>
      <input id="observaciones" class="icompleto" name="observaciones" value="" 
            runat="server" maxlength="200"/>
      
    </span>
      <span class="completo">
      <label for="certificado">Certificado de Análisis</label>
      <asp:FileUpload ID="FileUpload1" runat="server" />
       
      
    </span>
    <span class="completo">
      <label for="hoja">Hoja de seguridad</label>
      <asp:FileUpload ID="FileUpload2" runat="server" />
      
     
    </span>
  </div>
  </li>
   <li class="paneles">
    <div>
    
    <span class="completo">
      <label for="proveedor">Proveedor</label>
      <asp:DropDownList ID="proveedor" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="nombre" DataValueField="licenciante_id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>
 
 
 
    <span class="tercio">
     <label for="unidad">Unidad(*)</label>
      <select id="unidad" name="unidad" runat="server" class=":required :only on blur">
        <option value=""></option>
        <option value="mg">mg</option>
        <option value="ml">ml</option>
      </select>
  
     
    </span>
 
   <span class="dostercios">
      <label for="alarma_stock">Alarma Stock (mg/ml)</label>
      <input id="alarma_stock" name="alarma_stock" value="" runat="server" 
            class=":integer :only_on_blur " maxlength="10"/> 
      
      
    </span>
  
 

    
 <span class="tercio">
   <label for="acondicionamiento">Acondicionamiento(*)</label>
      <select id="acondicionamiento" name="acondicionamiento"  runat="server" class=":required">
        <option value=""></option>
        <option value="refrigerador">Refrigerador</option>
        <option value="freezer">Freezer</option>
        <option value="ambiente">Ambiente</option>
      </select>
      
    </span>
    
   <span class="dostercios">
         
      <label for="lote"> Lote Proveedor(*)</label>
      <input id="lote" name="lote" value="" runat="server" 
            class=":required :only on blur" maxlength="50" />
      
    </span>
   
      <span class="tercio">
       <label runat="server" id="label_cantidad_pedida" for="cantidad_pedida">Cantidad Pedida (*)</label>
        <input id="cantidad_pedida" name="cantidad_pedida" value="" runat="server" 
            class=":required :integer" maxlength="4"/>
      
    
    </span>
       <span class="tercio">
      <label for="catalogo">N° de catalogo</label>   
  <input id="catalogo" name="catalogo" value="" runat="server" 
             />
      
    
    </span>
    <span class="tercio">
    <label for="sedronar">Sedronar</label>
        <asp:CheckBox ID="sedronar" runat="server" />
   </span>
        
           
    
 
   
  
  </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <span class="boton">
       
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" />
   
        
   <input type="button" value="Volver" id="volver" onclick="location.href='agregar_estandares.aspx'"></span>
   
   </div>

    </li>
                 <li>
                 </li>
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EstandaresConnectionString %>" 
            SelectCommand="SELECT [licenciante_id], [nombre] FROM [Licenciantes]">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EstandaresConnectionString %>" 
            
        SelectCommand="SELECT [nombre_id], [nombre] FROM [nombres] WHERE ([deshabilitado] = @deshabilitado)">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="N" Name="deshabilitado" 
                    QueryStringField="N" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>

</asp:Content>
