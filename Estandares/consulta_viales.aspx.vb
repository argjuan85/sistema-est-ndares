﻿Imports globales
Imports System.Data
Imports System.Data.SqlClient
Imports Encryption64
Partial Class Estandares_consulta_viales
    Inherits System.Web.UI.Page
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(4, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(Request.QueryString("ID"))
                Dim es As Estandares
                Dim xtipo As String
                es = New Estandares()
                es.Estandares(conexion)

                If (es.obtenersector(codigo) <> Session("sector")) Then
                    form1.Visible = False
                    Button1.Visible = False
                    Button2.Visible = False

                End If

                If ((es.obtenerestado(codigo) <> "D") And (es.obtenerestado(codigo) <> "V")) Then
                    Button1.Visible = False
                    Button2.Visible = False
                End If

                'xtipo = es.Obtenertipoestandar(codigo)
                'If ((xtipo = "PP") Or (xtipo = "ES")) Then
                ' Button2.Visible = False
                ' End If

                buscarviales()
                If (Session("nivel") = nivel2) Then
                    Button1.Visible = False
                    Button2.Visible = False

                End If
            Catch
                Label2.Text = "Error al cargar datos de los viales"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If
    End Sub


    Sub buscarviales()
        Dim u As Viales

        Try
            u = New Viales()
            u.Viales(conexion)
            repeater.DataSource = u.Consultarviales2(codigo)
            repeater.DataBind()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda de viales."

        End Try
    End Sub
    Protected Function Estado(ByVal objGrid As Object) As String
        Dim aux As String
        aux = objGrid
        Select Case aux
            Case "E"
                Return "Enviado"
            Case "U"
                Return "En Uso"
            Case "C"
                Return "Consumido"
            Case "D"
                Return "Disponible"
            Case "R"
                Return "Descartado por rotura"
            Case Else
                Return "Estado Invalido"
        End Select
        
        
    End Function
    Protected Function Apertura(ByVal est As Object, ByVal ape As Object) As String
        Dim estado As String

        estado = est

        If (estado = "E") Then
            Return "N/A"
        End If
        Return ape

    End Function
    Protected Function Remanente(ByVal est As Object, ByVal ape As Object) As String
        Dim estado As String

        estado = est

        If ((estado = "E") Or (estado = "R")) Then
            Return "N/A"
        End If
        Return ape

    End Function





    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.Redirect("modifica_estandar.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("modificar_viales.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Redirect("quitar_viales.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

    End Sub
End Class
