﻿Imports System.IO
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports globales
Imports System
Imports Encryption64


Partial Class Estandares_Default
    Inherits System.Web.UI.Page
    Public TE As String
    Dim band As Integer = "1"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(2, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

            TE = DecryptText(Request.QueryString("ID"))
            cargaformulario()
            Dim message As String = "Confirma la carga del Estandar?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())

        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'nuevoestandar()
    End Sub

    Sub cargaformulario()
        Label1.text = ""
        Label2.text = ""
        tipo.Value = TE
        Select Case TE
            Case "EP"

                label_cantidad_pedida.InnerText = "Cantidad de Viales(*)"
            Case "ES"
                label_cantidad_pedida.InnerText = "Cantidad de Viales(*)"
                label_fecha_ingreso.InnerText = "Fecha Confección(*)"
                catalogo.Disabled = True
               

            Case "PP"
                catalogo.Disabled = True
            
            Case "ET"
                catalogo.Disabled = True
                label_cantidad_pedida.InnerText = "Cantidad de Viales(*)"
            Case Else
                '...
        End Select


        If (Page.IsPostBack) Then
          

            If (band) Then
                nuevoestandar()
                'blanqueo los campos 
                Select Case TE
                    Case "EP"
                        nombre.SelectedIndex = "0"
                        presentacion.Value = ""
                        catalogo.Value = ""
                        n_cas.Value = ""
                        fecha_vencimiento.Value = ""
                        alarma_stock.Value = ""
                        lote.Value = ""
                        cantidad_pedida.Value = ""
                        cod_qad.Value = ""
                        fecha_ingreso.Value = ""
                        observaciones.Value = ""
                        proveedor.SelectedIndex = "0"
                        unidad.SelectedIndex = "0"
                        acondicionamiento.SelectedIndex = "0"

                        sedronar.Checked = False



                    Case "ES"
                        nombre.SelectedIndex = "0"
                        presentacion.Value = ""
                        alarma_stock.Value = ""
                        catalogo.Value = ""
                        n_cas.Value = ""
                        fecha_vencimiento.Value = ""
                        lote.Value = ""
                        cantidad_pedida.Value = ""
                        cod_qad.Value = ""
                        fecha_ingreso.Value = ""
                        observaciones.Value = ""
                        alarma_stock.Value = ""
                        proveedor.SelectedIndex = "0"
                        unidad.SelectedIndex = "0"
                        acondicionamiento.SelectedIndex = "0"
                        label_fecha_ingreso.InnerText = "Fecha Confección"
                        sedronar.Checked = False

                    Case "PP"
                        nombre.SelectedIndex = "0"
                        presentacion.Value = ""
                        catalogo.Value = ""
                        n_cas.Value = ""
                        alarma_stock.Value = ""
                        fecha_vencimiento.Value = ""
                        lote.Value = ""
                        cantidad_pedida.Value = ""
                        cod_qad.Value = ""
                        acondicionamiento.SelectedIndex = "0"
                        alarma_stock.Value = ""
                        proveedor.SelectedIndex = "0"
                        fecha_ingreso.Value = ""
                        observaciones.Value = ""
                        unidad.SelectedIndex = "0"
                        sedronar.Checked = False

                    Case "ET"
                        nombre.SelectedIndex = "0"
                        presentacion.Value = ""
                        catalogo.Value = ""
                        alarma_stock.Value = ""
                        n_cas.Value = ""
                        fecha_vencimiento.Value = ""
                        lote.Value = ""
                        cantidad_pedida.Value = ""
                        cod_qad.Value = ""
                        unidad.SelectedIndex = "0"
                        observaciones.Value = ""
                        alarma_stock.Value = ""
                        fecha_ingreso.Value = ""
                        acondicionamiento.SelectedIndex = "0"
                        sedronar.Checked = False
                    
                    Case Else
                        '...
                End Select
                nombre.SelectedIndex = "0"



            End If


        End If

    End Sub
  

   



    Sub nuevoestandar()
        'estas lineas las coloco para evitar inconvenientes con el trato de decimales con el tipo de datos double tanto en ambiente de desarrollo como subido en el servidor
        Dim ri As New Globalization.CultureInfo("es-ES")
        ri.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = ri
        Dim e As Estandares
        Dim n As nombres
        Dim r As Integer
        Dim sext As String
        Dim extension As String
        Dim tam As Integer
        Dim nom As String
        Dim tam1 As Integer
        Dim nom1 As String
        Dim sed As String
        Dim xfecha As String
        Dim uni As String
        Dim uni1 As String
        'Dim xtsdc As Double
  
        Dim archivo As FileStream
        'log
        Dim l As logs
        l = New logs()
        l.logs(conexion)
    
        Try



            e = New Estandares()
            e.Estandares(conexion)
            nom = FileUpload1.FileName
            nom1 = FileUpload2.FileName

            If (sedronar.Checked = True) Then
                sed = "1"
            Else
                sed = "0"
            End If

            'Calculo tsdc según corresponda
            If (TE = "PP") Or (TE = "EP") Then
                'xtsdc = tsdtc.Value
            Else
                'Dim a As Double
                'Dim b As Double = humedad.Value
                'Dim c As Double = porcentaje.Value
                'Dim d As Double = tsds.Value

                'TSDTC= TSBS x {[1-%(H+I)]/100}  
                'b = humedad.Value
                'c = porcentaje.Value
                'a = 1 - (b + c)

                'TextBox1.Text = a
                'TextBox2.Text = b
                'TextBox3.Text = c
                'TextBox4.Text = d
                'xtsdc = d * (a / 100)
            End If
            'xtsds = Request.Form("tsds").Replace(",", ".")
            'xtsdc = Math.Round(xtsdc, decimalestsdc)
            uni = InStr(nom, ",")
            uni1 = InStr(nom1, ",")
            If ((uni = "0") And (uni1 = "0")) Then
                If extension <> "no valida" Then
                    Select Case TE
                        Case "EP"
                            r = e.AgregarEstandar(nombre.Text, lote.Value, proveedor.Text, catalogo.Value, n_cas.Value, TE, presentacion.Value.Replace(",", "."), unidad.Value, revertirfecha(fecha_vencimiento.Value), acondicionamiento.Value, revertirfecha(fecha_ingreso.Value), alarma_stock.Value, cantidad_pedida.Value, cod_qad.Value, "N", "H", nom, Nothing, String.Format("{0:yyyy/MM/dd}", Date.Now()), nom1, observaciones.Value, Session("sector"), sed)

                        Case "ES"
                            r = e.AgregarEstandar(nombre.Text, lote.Value, proveedor.Text, catalogo.Value, n_cas.Value, TE, presentacion.Value.Replace(",", "."), unidad.Value, revertirfecha(fecha_vencimiento.Value), acondicionamiento.Value, revertirfecha(fecha_ingreso.Value), alarma_stock.Value, cantidad_pedida.Value, cod_qad.Value, "N", "H", nom, Nothing, String.Format("{0:yyyy/MM/dd}", Date.Now()), nom1, observaciones.Value, Session("sector"), sed)

                        Case "PP"
                            r = e.AgregarEstandar(nombre.Text, lote.Value, proveedor.Text, catalogo.Value, n_cas.Value, TE, presentacion.Value.Replace(",", "."), unidad.Value, revertirfecha(fecha_vencimiento.Value), acondicionamiento.Value, revertirfecha(fecha_ingreso.Value), alarma_stock.Value, cantidad_pedida.Value, cod_qad.Value, "N", "H", nom, Nothing, String.Format("{0:yyyy/MM/dd}", Date.Now()), nom1, observaciones.Value, Session("sector"), sed)

                        Case "ET"
                            r = e.AgregarEstandar(nombre.Text, lote.Value, proveedor.Text, catalogo.Value, n_cas.Value, TE, presentacion.Value.Replace(",", "."), unidad.Value, revertirfecha(fecha_vencimiento.Value), acondicionamiento.Value, revertirfecha(fecha_ingreso.Value), alarma_stock.Value, cantidad_pedida.Value, cod_qad.Value, "N", "H", nom, Nothing, String.Format("{0:yyyy/MM/dd}", Date.Now()), nom1, observaciones.Value, Session("sector"), sed)


                        Case Else
                            '...
                    End Select

                    'valido que se cargue correctamente el estandar
                    If (r <> "-1") Then
                        n = New nombres()
                        n.Nombres(conexion)
                        'log
                        ' registralog("Estandard", r, nombre.Value)
                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "", "Estandares", r, n.obtenernombre(nombre.SelectedValue) + " - " + lote.Value)

                        Label1.Text = "Se cargó el estandar con éxito"
                        'EP

                        'adjunto los certificados y hoja de seguridad
                        If (FileUpload1.HasFile) Then
                            Try
                                'Obtener parametros a utilizar 

                                tam = FileUpload1.FileBytes.Length
                                'NOTA: EL ARCHIVO PDF PRIMERO LO GUARDO EN UNA CARPETA EN EL 
                                'SERVIDOR Y LUEGO LO LEO LOCALMENTE PARA GUARDARLO EN LA BD (FOLDER) 

                                sext = Path.GetExtension(nom)

                                uni = InStr(nom, ",")
                                uni1 = InStr(nom1, ",")
                                If ((uni = "0") And (uni1 = "0")) Then
                                    If sext = ".pdf" Then


                                        'usar esta en ambiente de prueba
                                        'Dim ruta As String = Server.MapPath(rutapdf & nom)

                                        'este en ambiente productivo
                                        'para que no se sobre escriban los archivos en el servidor antepongo el id del estandar
                                        Dim ruta As String = rutapdf & "\certificado\" & r & "-" & nom

                                        'ruta = ruta & nom 'DIRECCION EN EL SERVIDOR DONDE SE GUARDAR EL ARCHIVO FISICO 
                                        'Guardar Archivo en Carpeta Servidor 
                                        FileUpload1.SaveAs(ruta)
                                        'leer archivo 
                                        archivo = New FileStream(ruta, FileMode.Open, FileAccess.Read)
                                        Dim imagen(tam) As Byte
                                        archivo.Read(imagen, 0, tam)
                                        archivo.Close()
                                    Else
                                        extension = "no valida"
                                    End If
                                Else
                                    Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
                                End If
                            Catch ex As Exception
                                Label2.Text = "Problemas al Guardar Archivo: " & ex.Message
                            End Try
                        End If
                        If (FileUpload2.HasFile) Then
                            Try
                                'Obtener parametros a utilizar 

                                tam1 = FileUpload2.FileBytes.Length

                                'NOTA: EL ARCHIVO PDF PRIMERO LO GUARDO EN UNA CARPETA EN EL 
                                'SERVIDOR Y LUEGO LO LEO LOCALMENTE PARA GUARDARLO EN LA BD (FOLDER) 

                                sext = Path.GetExtension(nom1)
                                uni = InStr(nom, ",")
                                uni1 = InStr(nom1, ",")
                                If ((uni = "0") And (uni1 = "0")) Then
                                    If sext = ".pdf" Then
                                        'usar esta en ambiente de prueba
                                        'Dim ruta As String = Server.MapPath(rutapdf & nom1)

                                        'este en ambiente productivo
                                        Dim ruta As String = rutapdf & "\hoja\" & r & "-" & nom1

                                        'Dim ruta As String = Server.MapPath(rutapdf & nom1) 'DIRECCION EN EL SERVIDOR DONDE SE GUARDAR EL ARCHIVO FISICO 
                                        'Guardar Archivo en Carpeta Servidor 
                                        FileUpload2.SaveAs(ruta)
                                        'leer archivo 
                                        archivo = New FileStream(ruta, FileMode.Open, FileAccess.Read)
                                        Dim imagen(tam1) As Byte
                                        archivo.Read(imagen, 0, tam1)
                                        archivo.Close()
                                    Else
                                        extension = "no valida"
                                    End If
                                Else
                                    Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
                                End If
                            Catch ex As Exception
                                Label2.Text = "Problemas al Guardar Archivo: " & ex.Message
                            End Try
                        End If
                        'fin adjuntar archivos



                    Else
                        Label2.Text = "Error al generar el estándar"
                    End If


                Else
                    Label2.Text = "Solo se pueden adjuntar archivos con extensión PDF"
                End If
            Else
                Label2.Text = "No se pueden adjuntar archivos con caracteres especiales. Por favor verifique el nombre de la hoja de datos y/o Certificado."
            End If

        Catch ex As Exception
            Label2.Text = "Error al generar el estándar"
        End Try
    End Sub


   
End Class
