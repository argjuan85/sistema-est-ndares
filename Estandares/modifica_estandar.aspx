﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="modifica_estandar.aspx.vb" Inherits="Estandares_modifica_estandar" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
<!-- librerias calendario y css -->
       <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
      
     
 <link href="../js/vanadium/style.css" rel="Stylesheet" type="text/css" />
      <!--  calendarios  --> 
    <script type="text/javascript" >
	$(document).ready(function(){
	   $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	
	
		$("#<%= idfecha_ingreso.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		
	
		$("#<%= idfecha_vencimiento.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy' , changeMonth: true, changeYear: true ,yearRange: '-10:+10', minDate: 'today', stepMonths: 0}).val();
	
	
});

  
	  function triggerFileUpload()
	  {
		document.getElementById("File1").click();
	  }
	  function triggerFileUpload2()
	  {
		document.getElementById("File2").click();
	  }  

	  function setHiddenValue()
	  {
		document.getElementById("Hidden1").value = document.getElementById("File1").value;
	  }
	    function setHiddenValue2()
	  {
		document.getElementById("Hidden2").value = document.getElementById("File2").value;
	  }
	      function setHiddenValue3()
	  {
		document.getElementById("Hidden1").value = "//";
	  }
	      function setHiddenValue4()
	  {
		document.getElementById("Hidden2").value = "//";
	  }
    


   </script>
  <script type="text/javascript">
        function confirma_elimina() {
           return confirm('Confirma la eliminacion del estandar?');
        }

     </script>   

      <form id="form2" runat="server">
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
 Modificar datos del Estándar
 </div>
    <ul>
    <li class="paneles">
    <div>
    <span class="completo">
    <label for="nombre">Nombre</label>
     <asp:DropDownList ID="idnombre" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="nombre" DataValueField="nombre_id" class=":required :only_on_blur">
        </asp:DropDownList>
    <input runat="server" id="Hidden3" type="hidden" />
      
      
    </span>
 
    <span class="tercio">
      <label for="tipo">Tipo Estandar</label>
     <input id="idtipo" name="idtipo" value="" runat="server" readonly="readonly"/>
     
     
    </span>
 
    <span class="dostercios">
      <label id="label_presentacion" runat="server" for="presentacion">Presentacion</label>
      <input id="idpresentacion" runat="server" name="presentacion" value="" class=":required :float  :only_on_blur"/>
     
    </span>
 
    <span class="tercio">
      <label for="n_cas">Nº de CAS</label>
      <input id="idn_cas" runat="server" name="n_cas" value="" />
      
    </span>
 
    <span class="dostercios">
      <label runat="server" id="label_cod_qad" for="cod_qad">Código Qad</label>
      <input id="idcod_qad" runat="server" name="cod_qad" value="" />
        
     
    </span>
    
      <span class="tercio">
      <label runat="server" id="label_fecha_ingreso" for="fecha_ingreso">Fecha Ingreso</label>
      <input id="idfecha_ingreso" runat="server" name="fecha_ingreso" value="" class=":required" readonly="readonly"/>
      
    </span>
 
  <span class="dostercios">
      <label runat="server" id="label_cantidad_pedida" for="cantidad_pedida">Cantidad Pedida</label>
      <input id="idcantidad_pedida" runat="server" name="cantidad_pedida" value="" class=":required :integer" maxlength="4"/>
      
    </span>
 
  
 
   
   
   
      <span class="completo">
      <label runat="server" id="label_observaciones" for="observaciones">Observaciones</label>
      <input id="idobservaciones" runat="server" name="observaciones" value="" />
      
    </span>
      <span class="tercio">
             <label id="sedronar" for="sedronar">Sedronar</label>
        <asp:CheckBox ID="idsedronar" runat="server" />
       </span>
        <span class="dostercios">
        <label id="aviso" for="sedronar">Notificacion stock mínimo</label>
        <asp:CheckBox ID="idaviso" runat="server" />
         </span>
     <span class="completo">
     <label runat="server" id="label_certificado" for="certificado">Certificado</label>
     	  <input runat="server" id="Hidden1" type="hidden" />
	       <input id="idarch" runat="server" name="arch" type="hidden" value="" />
     <asp:Button ID="Button2" runat="server" Text="Certificado de Análisis" 
            Visible="False"  />
     <asp:Button ID="Button4" runat="server" Text="+" Onclientclick="return confirm('¿Confirma que desea agregar un nuevo certificado?');"/>
     <asp:Button ID="Button13" runat="server" Text="-" Onclientclick="return confirm('¿Confirma que desea quitar el certificado?');"/> <asp:Label ID="confirma_certificado" runat="server"></asp:Label><span></span>
     <asp:FileUpload ID="FileUpload1" runat="server" Visible="False" />
     
      </span>
    
      <span class="completo">
      <label runat="server" id="label_hoja" for="hoja">Hoja de seguridad</label>
      <input runat="server" id="Hidden2" type="hidden" />
	  <input id="idarch1" runat="server" name="arch1" type="hidden" value="" />
      <asp:Button ID="Button3" runat="server" Text="Hoja de Seguridad" 
            Visible="False" />
      <asp:Button ID="Button5" runat="server" Text="+" Onclientclick="return confirm('¿Confirma que desea agregar una nueva hoja de datos?');"/>
      <asp:Button ID="Button6" runat="server" Text="-" Onclientclick="return confirm('¿Confirma que desea quitar la hoja de datos?');"/> <asp:Label ID="confirma_hoja" runat="server"></asp:Label>
      <asp:FileUpload ID="FileUpload2" runat="server" Visible="False" />
      
      </span>

  </div>
  </li>
   <li class="paneles">
    <div>
    
    <span class="completo">
     <label for="proveedor">Proveedor</label>
      <asp:DropDownList ID="proveedor" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="nombre" DataValueField="licenciante_id">
        </asp:DropDownList>
        
     
    </span>
 
  
 
    <span class="tercio">
    <label for="unidad">Unidad</label>
      <select id="idunidad" runat="server" name="unidad" class=":required :only on blur">
        <option value=""></option>
        <option value="mg">mg</option>
        <option value="ml">ml</option>
    
      </select>
   
       
    </span>
    </span> 
    <span class="dostercios">
     <label runat="server" id="label_alarma_stock" runat="server" for="alarma_stock">Alarma Stock (mg/ml)</label>
      <input id="idalarma_stock" runat="server" name="alarma_stock" value="" class=":integer :only_on_blur "/>
        
     
    </span>

     <span class="tercio">
         <label for="lote"> Lote Proveedor</label>
      
      <input id="idlote" runat="server" name="lote" value="" class=":required :only on blur" />
      
    </span>
 
    <span class="dostercios">
  
       <label for="lote"> N° de Catalogo</label>
      
      <input id="idcatalogo" runat="server" name="catalogo" value=""  />
    </span>


   <span class="tercio">
   <label for="acondicionamiento">Acondicionamiento</label>
      <select id="idacondicionamiento" runat="server" name="acondicionamiento" class=":required">
        <option value=""></option>
        <option value="refrigerador">Refrigerador</option>
        <option value="freezer">Freezer</option>
        <option value="ambiente">Ambiente</option>
      </select>
      
    </span>
 
    <span class="dostercios">
  
         <label for="estado">Estado</label>
        <select id="idestado" runat="server" name="estado">
        <option value=""></option>
        <option value="F">F</option>
        <option value="H">H</option>
        <option value="V">V</option>
        <option value="D">D</option>
        <option value="I">I</option>
        </select>
    </span>
    
    <span class="tercio">
<label runat="server" id="label_fecha_creacion" for="fecha_creacion">Fecha de Creación</label>
      <input id="idfecha_creacion"  runat="server" name="fecha_creacion" value="" readonly="readonly" />
      
    </span>
    
 <span class="dostercios">
         <label for="fecha_vencimiento">Fecha de Vencimiento</label>
      <input id="idfecha_vencimiento" runat="server" name="fecha_vencimiento" value="" readonly="readonly"/>
      
    </span>
   
     
    
   
 
    <span class="dostercios">
      <label id="label_stock" runat="server" for="stock">Stock</label>
      <input id="idstock" runat="server" name="stock" value="" />
      
    </span>
           
 


    <span class="tercio">
      <label for="vial_actual">Vial Actual</label>
      <input id="idvial_actual" runat="server" name="vial_actual" value="" />
      
    </span>
    <span class="dostercios">
      <label id="label_remanente" runat="server"  for="remanente">Remanente Vial Actual</label>
      <input id="idremanente" name="remanente" runat="server" value="" />
      
    </span>
    <span class="tercio">
      <label for="viales_cerrados">Viales Cerrados</label>
      <input id="idviales_cerrados" runat="server" name="viales_cerrados" value="" />
      
    </span>
    </div>
  </li>
     
        
     
        
     
  </ul>
    <ul>
   
        
   
    <li class="panel_boton">
     <div>
       
    
    
      <span class="boton">
         <asp:Button ID="Button8" runat="server" Text="Consumos"  />
         <asp:Button ID="Button9" runat="server" Text="Viales" Height="26px"  />
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
        
         <input type="button" value="Volver" id="volver" onclick="location.href='consulta_estandares.aspx'" />
         <asp:Button ID="Button12" runat="server" Text="Borrar" />
         <asp:Button ID="Button10" runat="server" Text="Envios" />
       
         
    </span>
  
    </div>
    </li>
                 <li>
                 </li>
     </ul>
  </div>
  </div>
  <br>
  <div id="notice">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
  <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EstandaresConnectionString %>" 
            SelectCommand="SELECT [licenciante_id], [nombre] FROM [Licenciantes]">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EstandaresConnectionString %>" 
            
          SelectCommand="SELECT [nombre_id], [nombre] FROM [nombres] WHERE ([deshabilitado] = @deshabilitado)">
             <SelectParameters>
                 <asp:QueryStringParameter DefaultValue="N" Name="deshabilitado" 
                     QueryStringField="N" Type="String" />
             </SelectParameters>
        </asp:SqlDataSource>
      </form>
      </asp:Content>
