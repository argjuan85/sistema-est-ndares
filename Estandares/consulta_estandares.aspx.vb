﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports System.Net.Mail


Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Label2.Text = ""
        Dim checks As Boolean = False
        Dim checks2 As Boolean = False
        Dim ult As String

        'sql = "Select * from Estandard where Estandard.Borrado = 'N' and Estandard.sector = '" & Session("sector") & "' "
        'sql = "Select * from Estandard where Estandard.Borrado = 'N' "
        sql = "Select * from Estandard  inner join nombres  on nombre_id=Estandard.Nombre where Estandard.Borrado = 'N' "
        If CheckBox10.Checked = True Then
            sql = sql & "and Estandard.sector = '" & Session("sector") & "' "
        End If

        If TextBox1.Text <> "" Then
            sql = sql & " and nombres.nombre like '%" & TextBox1.Text & "%' "
        End If

        If TextBox2.Text <> "" Then
            sql = sql & " and Estandard.LotePro like '%" & TextBox2.Text & "%' "
        End If

        If TextBox3.Text <> "" Then
            sql = sql & " and Estandard.NumCatalogo like '%" & TextBox3.Text & "%' "
        End If

        If TextBox4.Text <> "" Then
            sql = sql & " and Estandard.CodQAD like '%" & TextBox4.Text & "%' "
        End If


        If (CheckBox1.Checked = True) And (checks = False) Then
            sql = sql & " and (Estandard.Est = 'V'"

            checks = True
        ElseIf (CheckBox1.Checked = True) Then
            sql = sql & " or Estandard.Est = 'V'"
        End If
        If (CheckBox2.Checked = True) And (checks = False) Then
            sql = sql & " and (Estandard.Est = 'D'"

            checks = True
        ElseIf (CheckBox2.Checked = True) Then
            sql = sql & " or Estandard.Est = 'D'"

        End If
        If (CheckBox3.Checked = True) And (checks = False) Then
            sql = sql & " and (Estandard.Est = 'H'"

            checks = True
        ElseIf (CheckBox3.Checked = True) Then
            sql = sql & " or Estandard.Est = 'H'"

        End If
        If (CheckBox4.Checked = True) And (checks = False) Then
            sql = sql & " and (Estandard.Est = 'F'"

            checks = True
        ElseIf (CheckBox4.Checked = True) Then
            sql = sql & " or Estandard.Est = 'F'"

        End If
        If (CheckBox5.Checked = True) And (checks = False) Then
            sql = sql & " and (Estandard.Est = 'I'"
            checks = True
        ElseIf (CheckBox5.Checked = True) Then
            sql = sql & " or Estandard.Est = 'I'"

        End If

        'UltimaLetra = Cadena.Substring(Cadena.Length - 1, 1)
        ult = sql.Substring(sql.Length - 1, 1)
        If ((ult <> ")") And checks = True) Then
            sql = sql & ")"
        End If

        If (CheckBox6.Checked = True) And (checks2 = False) Then
            sql = sql & " and (Estandard.Tipo = 'EP' "

            checks2 = True
        ElseIf (CheckBox6.Checked = True) Then
            sql = sql & " or Estandard.Tipo = 'EP' "
        End If
        If (CheckBox7.Checked = True) And (checks2 = False) Then
            sql = sql & " and (Estandard.Tipo = 'ES' "

            checks2 = True
        ElseIf (CheckBox7.Checked = True) Then
            sql = sql & " or Estandard.Tipo = 'ES' "

        End If
        If (CheckBox8.Checked = True) And (checks2 = False) Then
            sql = sql & " and (Estandard.Tipo = 'ET' "

            checks2 = True
        ElseIf (CheckBox8.Checked = True) Then
            sql = sql & " or Estandard.Tipo = 'ET' "

        End If
        If (CheckBox9.Checked = True) And (checks2 = False) Then
            sql = sql & " and (Estandard.Tipo = 'PP' "
            checks2 = True
        ElseIf (CheckBox9.Checked = True) Then
            sql = sql & " or Estandard.Tipo = 'PP' "

        End If

        ult = sql.Substring(sql.Length - 1, 1)
        If ((ult <> ")") And (checks2 = True)) Then
            sql = sql & ")"
        End If

        If DropDownList1.Text = "1" Then
            buscarestandar(sql)

        ElseIf DropDownList1.Text = "2" Then
            sql = sql & " and (Estandard.Stock < Estandard.AlarmaStock ) and (Estandard.Est = 'V')  "
            buscarestandar(sql)
        Else
            buscarestandar(sql)
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(0, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Dim u As Stock
            Dim aux1 As String
            u = New Stock()
            u.Stock(conexion)
            aux1 = u.obtenerultimostock()
            If (aux1 <> "-1") Then

                label_stock.Text = "Fecha Último Stock de EP: " + aux1

            Else
                label_stock.Text = "Fecha Último Stock de EP: N/A "
            End If

            'solo muestro este label y el boton a control
            'Session("sector")
            'label_stock.Visible = False
            'Button3.Visible = False


            If (Not Page.IsPostBack) Then
                CheckBox10.Checked = True
            End If

            If (DropDownList1.SelectedValue <> "3") Then
                mes.Visible = False
                label_mes.Visible = False
            End If

            If ((Session("nivel") = nivel4) Or (Session("nivel") = nivel2)) Then
                Button2.Visible = False
                Button3.Visible = False
                Button4.Visible = False
            End If
            End If
    End Sub

    Protected Function invertirfecha(ByVal fecha As Object) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function
    Protected Function sedronar(ByVal fecha As Object) As String
       If (fecha = "0") Then
            Return "NO"
        Else
            Return "SI"
        End If


    End Function
    Protected Function nombreestandar(ByVal nombre As Object) As String
        Dim e As nombres
        e = New nombres()
        e.Nombres(conexion)

    
        Return e.obtenernombre(nombre)


    End Function
   

    Sub buscarestandar(ByVal sql As String)
        Dim u As Estandares
        Dim meses_max As String
        Try
            u = New Estandares() ' nuevo objeto instancia
            u.Estandares(conexion) 'invoco el constructor y paso parametros de conexion
            If DropDownList1.Text = "3" Then
                meses_max = meses_vencimiento
                If (mes.Text <> "") Then
                    repeater.DataSource = u.Consultaestandares3(sql, mes.Text)
                Else
                    repeater.DataSource = u.Consultaestandares3(sql, meses_max)
                End If

                repeater.DataBind()

            Else
                repeater.DataSource = u.Consultaestandares(sql)
                repeater.DataBind()
            End If


        Catch ex As Exception
            Label2.Text = "Error al buscar el estándar"

        End Try
    End Sub
    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim strURL As String
        Dim Codigox As String
        If (e.CommandName = "Click") Then
            'get the id of the clicked row
            Codigox = Convert.ToString(e.CommandArgument)

                       strURL = "modifica_estandar.aspx?ID=" + EncryptText(Codigox)
            Response.Redirect(strURL)
        End If

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_estandares.aspx")
    End Sub

    

  
    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        If DropDownList1.SelectedValue = "3" Then
            mes.Visible = True
            label_mes.Visible = True

        End If
    End Sub

    Public Sub EnvioMail()

        Dim message As New MailMessage

        Dim smtp As New SmtpClient

        message.From = New MailAddress("mtvsj@raffo.com.ar")

        message.To.Add("jarganaraz@raffo.com.ar")

        message.Body = "EL CUERPO O EL CONTENIDO DEL MENSAJE"

        message.Subject = "Estandares Proximos a Vencer"

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = "587"

        smtp.Host = "192.168.40.33"

        smtp.Credentials = New Net.NetworkCredential("mtvsj", "mtvsj1234")

        smtp.Send(message)

    End Sub
   
   
    
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.Redirect("../Stock/consultar_stock.aspx")
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        Response.Redirect("../Nombres/consultar_nombres.aspx")
    End Sub
End Class
