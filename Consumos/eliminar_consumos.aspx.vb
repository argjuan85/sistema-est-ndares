﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String
    Public Codigox As String

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim es As Estandares
            Dim u As Consumos
            Dim v As Viales
            Dim r As String
            Dim aux As Boolean
            Dim lk As Double
            Dim kl As Double
            Dim jk As Double
            Dim strURL As String



            'log
            Dim nomreg As String
            Dim nomreg2 As String
            Dim idvia As Integer
            Dim xcanti As String
            Dim xfecha As String
            Dim l As logs
            Dim n As nombres
            l = New logs()
            l.logs(conexion)
            es = New Estandares
            es.Estandares(conexion)
            'obtengo datos del consumo a borrar
            u = New Consumos()
            u.Consumos(conexion)
            n = New nombres()
            n.Nombres(conexion)

            xcanti = u.obtenercant(Codigox)
            nomreg = es.obtenernombre1(codigo) + " Cantidad: " + xcanti
            nomreg2 = n.obtenernombre(es.obtenernombre(codigo)) + " - " + es.obtenerlote(codigo)
            'obtengo los datos del vial al cual se le efectuo el consumo
            v = New Viales()
            v.Viales(conexion)
            idvia = u.obtenercodv(Codigox)




            'actualizo el vial
            lk = v.obtenerremanente(idvia)
            kl = u.obtenercant(Codigox)
            jk = lk + kl

            If ((es.obteneridvialenuso(codigo) <> idvia) Or (es.obtenercantidadvialesuso(codigo) > "1")) Then
                'el vial siempre va pasar a estado U, ya que se penso originalmente que el consumo se vaya dando por orden de vial, entonces por mas q borren un consumo de un vial anterior al actual se le colocara estado u, la funcionalidad se hace solo para salvar errores de carga no para manipular consumos de forma mas compeja
                r = v.Actualizarvial(idvia, codigo, "U", v.obtenerapertura(idvia) - 1, jk, Nothing)
            Else
                'si no hay viales con consumos mas adelante del cual se estan borrando los consumos se vuelve a estado d en caso que correponda)
                'valido que si al elminar el vial queda sin consumos debo colocarlo en estado D
                If (es.obtenerpresentacion(codigo) = jk) Then
                    r = v.Actualizarvial(idvia, codigo, "D", v.obtenerapertura(idvia) - 1, jk, Nothing)
                Else
                    r = v.Actualizarvial(idvia, codigo, "U", v.obtenerapertura(idvia) - 1, jk, Nothing)
                End If
            End If
            If (r) Then
                'Actualizo el stock del estándar
                r = es.actualizarstock(codigo)
                If (r = "1") Then


                    'log actualizacion de stock
                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                    l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por eliminacion de consumo.", "Estandares", codigo, nomreg2)

                    'elimino el consumo
                    r = u.eliminarconsumo(Codigox)
                    If (r = "1") Then
                        'log eliminacion
                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                        aux = l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Consumo, Motivo: " & motivo.Value, "Consumos", Codigox, nomreg)
                        If (aux) Then
                            'redireccion
                            strURL = "consulta_consumos.aspx?ID=" + EncryptText(codigo)
                            Response.Redirect(strURL)
                        Else
                            Label2.Text = "Error al generar log."
                        End If
                    Else
                        Label2.Text = "Error  al eliminar consumo." & r
                        'debug
                        'Label2.Text = "Error  al eliminar consumo." & r
                    End If


                Else
                    Label2.Text = "Error al actualizar stock."
                    'debug
                    'Label2.Text = "Error al actualizar stock." & r
                End If
            Else
                Label2.Text = "Error al actualizar vial."
                'debug
                'Label2.Text = "Error al actualizar vial." & r
            End If

        Catch ex As Exception
            Label2.Text = "Error eliminando consumo."

        End Try



    End Sub

   

 

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(16384, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try

          
                codigo = DecryptText(Request.QueryString("ID")) 'codigo estandar
                Codigox = DecryptText(Request.QueryString("id2")) 'codigo consumo

                'si el estandar se cerro no puedo seguir consumiento
                Dim es As Estandares


                es = New Estandares()
                es.Estandares(conexion)
                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se pueden eliminar consumos"

                End If


                Dim message As String = "Confirma la eliminacion del consumo?"
                Dim sb As New System.Text.StringBuilder()
                sb.Append("return confirm('")
                sb.Append(message)
                sb.Append("');")
                ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
            Catch ex As Exception
                Label2.Text = "Error al cargar datos del estandar"
                Form.Visible = False
                Button1.Visible = False
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
End If
       
    End Sub
End Class
