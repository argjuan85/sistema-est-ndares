﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="cargar_consumos.aspx.vb" Inherits="Consumos_cargar_consumos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <form runat ="server"> 
   
       <!-- librerias calendario y css -->
       <link rel="stylesheet" href="../Estandares/css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="../Estandares/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="../Estandares/js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
         
      <!--  calendarios  --> 
    <script type="text/javascript" >

	
	$(document).ready(function(){
	   $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
	$("#<%= fecha_consumoadm.ClientID %>").datepicker({
	 dateFormat: "dd/mm/yy",  changeMonth: true,changeYear: true , maxDate: 'today', yearRange: '-10:+10', stepMonths: 0
	});
	
	$("#<%= fecha_consumo.ClientID %>").datepicker({
	 dateFormat: "dd/mm/yy",  changeMonth: true,changeYear: true , maxDate: 'today', yearRange: '-10:+10', minDate: '-5',stepMonths: 0
	});
		$("#<%= cantidad.ClientID %>").numeric(".");
		
		
});


function button2_onclick() {

}


   </script>
    <script type="text/javascript">
        function alertMe() {
        
             window.location = "http://www.bufa.es";
        }
    </script>
   
   
   <script type="text/javascript">
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="consulta_consumos.aspx?"+ arr[0] } 

</script>


  
   <div id="contenedor1">
    <div id="contenedorconsumos">
     
    <div id="titulo_seccion">
    <label id="titulo" runat="server" >Ingrese los datos del consumo</label>

 </div>
    <ul>
    <li class="paneles">
    <div>
  
 
    <span class="tercio">
     <label id="labelcantidad" runat="server" for="tipo">Cantidad</label>
        <input id="cantidad" runat="server" name="cantidad" value="" class=":required :max_length;7 :float :only_on_blur"/>
     
    </span>
 
    
   <span class="tercio">
     <label for="tipo">Apertura</label>
        <input id="apertura" runat="server" name="apertura" value="" class=":required :max_length;2 :float :only_on_blur"/>
     
    </span>
    <span class="dostercios">
      <label for="n_cas">Detalle</label>
      <input id="detalle" runat="server" name="detalle" value="" 
            class=":required :only_on_blur" maxlength="50"/>
      
    </span>
    
   <span class="tercio">
     <label for="tipo">Usuario</label>
        <input id="usuario" runat="server" name="usuario" value="" class=" :required :only_on_blur"/>
     
    </span>
 
    </div>
  </li>
                
     
  </ul>
  
  <ul>
   <li class="paneles">
    <div>
    <span class="dostercios">
    <label for="fecha_consumo">Fecha Consumo</label>
     <input id="fecha_consumo" runat="server" name="fecha_consumo" value="" class=":required :only_on_blur" readonly="readonly" />
      <input id="fecha_consumoadm" runat="server" name="fecha_consumoadm" value="" readonly="readonly" />
     
    </span>
       
      
   
    <span class="dostercios">
      <label for="lote">Lote Producto</label>
      <input id="lote" runat="server" name="lote" value="" class=":required :only_on_blur" maxlength="50"/>&nbsp;
        
    &nbsp;</span>
       <span class="completo">
      <label runat="server" id="label_motivo" for="lote">Motivo</label>
      <input id="motivo" runat="server" name="motivo" value="" class=":required :only_on_blur" maxlength="50"/>&nbsp;

        
    &nbsp;</span>
    
  </div>
  </li>
  </ul>
  
    <ul>
   
    <li class="panel_boton">
     <div>
      <span class="boton">
           <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" >
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
      </span></div>
    </li>
        
     </ul>
     
     
        
  </div>
  </div>
  <br>
  <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
  <div id="notice">
     <asp:Label ID="Label3" runat="server" ></asp:Label>
     </div>
      <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div> 
     
     
        
     </form>
     
  </asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="head">

    <style type="text/css">
        #volver
        {
            height: 26px;
        }
    </style>

</asp:Content>

