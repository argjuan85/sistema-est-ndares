﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration


Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0
    Public opcion As String
    Public sql As String
    Public codigo As String
    Public uni As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(256, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try

                codigo = DecryptText(Request.QueryString("ID"))
                Dim es As Estandares
                es = New Estandares()
                es.Estandares(conexion)
                uni = es.obtenerunidad(codigo)
                'no puuedo consumir estandares que no esten vigentes
                'validacion de sector
                If (es.obtenersector(codigo) = Session("sector")) Then
                    If (es.obtenerestado(codigo) <> "V") Then
                        Button2.Visible = False
                    End If


                Else
                    Button1.Visible = False
                    Button2.Visible = False
                    Button3.Visible = False
                    TextBox1.Visible = False
                    btnReport.Visible = False
                    Label2.Text = "El estandar que esta tratando acceder pertenece a otro sector."
                End If

                If (Session("nivel") = nivel2) Then
                    Button2.Visible = False
                    btnReport.Visible = False

                End If
            Catch
                Label2.Text = "Error al cargar datos de los consumos"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If

    End Sub
    Sub buscarconsumos(ByVal vial As String)
        Dim u As Consumos

        Try
            u = New Consumos() ' nuevo objeto instancia
            u.Consumos(conexion) 'invoco el constructor y paso parametros de conexion
            repeater.DataSource = u.Consultarconsumo1(codigo, vial)
            repeater.DataBind()

        Catch ex As Exception
            Label2.Text = "Se produjo un error al realizar la búsqueda."

        End Try
    End Sub
    Protected Function consumos(ByVal cons As Object) As String

        Return cons & " " & uni



    End Function


    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand
        Try
            Dim Codigox As String

            If (e.CommandName = "Click") Then
                'get the id of the clicked row
                Codigox = Convert.ToString(e.CommandArgument)
                'redireccion
                Dim strURL2 As String
                strURL2 = "eliminar_consumos.aspx?ID=" + EncryptText(codigo) + "&id2=" + EncryptText(Codigox)
                Response.Redirect(strURL2)
            End If

        Catch ex As Exception
            Label2.Text = "Error eliminando consumo."

        End Try
               
    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("cargar_consumos.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim es As Estandares
        es = New Estandares
        es.Estandares(conexion)

        buscarconsumos(TextBox1.Text)

        ' Deshabilito el boton eliminar para los usuarios nivel 2 y 4 o si el estandar esta fuera de vigencia
        If (Session("nivel") = nivel2 Or Session("nivel") = nivel4 Or (es.obtenerestado(codigo) = "F")) Then

            Dim ri As RepeaterItem
            Dim boton As Button
            For Each ri In repeater.Items
                boton = ri.FindControl("Button11")
                boton.Enabled = False

            Next
        Else
            Dim ri As RepeaterItem
            Dim boton As Button
            For Each ri In repeater.Items
                boton = ri.FindControl("Button11")
                'boton.Attributes.Add("onclick", " return confirma_elimina();")
            Next


        End If
        

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.Redirect("../Estandares/modifica_estandar.aspx?ID=" + EncryptText(codigo))
    End Sub
    Private Function GetData(ByVal query As String) As DataTable
        Dim conString As String = ConfigurationManager.ConnectionStrings("EstandaresConnectionString").ConnectionString
        Dim cmd As New SqlCommand(query)
        Using con As New SqlConnection(conString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con

                sda.SelectCommand = cmd
                Using dt As New DataTable()
                    sda.Fill(dt)

                    Return dt
                End Using
            End Using
        End Using
    End Function
    Protected Sub GenerateReport(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            Dim sql As String
            Dim pagina As Integer = 1

            'sql = "SELECT CodEst ,nombre, LotePro, Tipo, Prese, Uni, TitSBS, TitSDTC, Humedad, FeVto, Acond, Stock FROM Estandard where Estandard.Tipo = '" & DropDownList1.Text & "' and Estandard.sector = '" & sectoruser & "'"
            sql = "select v.num_vial, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodEst=" + codigo + "order by v.num_vial, c.num_ape asc"
            Dim dr As DataRow = GetData(sql).Rows(0)
            Dim document As New Document(PageSize.A4.Rotate(), 88.0F, 88.0F, 10.0F, 10.0F)
            Dim NormalFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)
            Dim es As Estandares
            Dim n As nombres

            Using memoryStream As New System.IO.MemoryStream()
                Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
                Dim phrase As Phrase = Nothing
                Dim cell As PdfPCell = Nothing
                Dim table As PdfPTable = Nothing
                Dim color__1 As Color = Nothing
                Dim test As Integer = 0
                Dim j, k As Integer
                Dim ds As DataSet
                Dim cn As SqlConnection
                Dim cm As SqlCommand
                Dim da As SqlDataAdapter
                Dim aux As String = 0



                'traigo los valores de la bd
                ' sql = "SELECT CodEst ,nombre, LotePro, Tipo, Prese, Uni, TitSBS, TitSDTC, Humedad, FeVto, Acond, Stock FROM Estandard where Estandard.Tipo = '" & DropDownList1.Text & "' and Estandard.sector = '" & sectoruser & "'"
                sql = "select  v.num_vial, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodEst=" + codigo + "order by v.num_vial, c.num_ape asc"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)


                document.Open()

                ' este bucle determina el numero de paginas del documento, registro actual guarda los registros ya impresos y lo compara versus el total que trae la consulta
                While (registro_actual < ds.Tables(0).Rows.Count)

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F, 0.33F, 0.33F})

                    ' para armar la cabecera
                    Dim nombreestand As String
                    Dim fogl As String
                    es = New Estandares()
                    es.Estandares(conexion)
                    n = New nombres()
                    n.Nombres(conexion)
                    nombreestand = n.obtenernombre(es.obtenernombre(codigo))
                    fogl = informe_EP


                    'Columna 1
                    phrase = New Phrase()
                    'phrase.Add(New Chunk(fogl & vbLf, FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)))
                    phrase.Add(New Chunk("Página " & pagina, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    phrase.Add(New Chunk(vbLf & vbLf & "Lote : " & es.obtenerlote(codigo) & " - Presentacion: " & es.obtenerpresentacion(codigo) & es.obtenerunidad(codigo) & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Departamento : Control de Calidad " & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Fecha de emisión : " & String.Format("{0:dd/MM/yyyy H:mm:ss}", Date.Now()) & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Fecha de Vencimiento : " & invertirfecha(es.obtenervencimiento(codigo)) & vbLf, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)



                    'Columna2



                    phrase = New Phrase()
                    phrase.Add(New Chunk("Consumos de " + nombreestand & vbLf & vbLf, FontFactory.GetFont("Arial", 14, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                    table.AddCell(cell)
                    document.Add(table)

                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador


                    'Linea
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    DrawLine(writer, 25.0F, document.Top - 70.0F, document.PageSize.Width - 25.0F, document.Top - 70.0F, color__1)


                    'Titulos de las columnas
                    table = New PdfPTable(7)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    'Ancho columnas

                    table.SetWidths(New Single() {0.1F, 0.1F, 0.15F, 0.1F, 0.3F, 0.15F, 0.15F})

                    'nombre

                    table.AddCell(PhraseCell(New Phrase("N° de Vial", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("N° de Apertura", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Fecha de Consumo", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Cantidad (" & es.obtenerunidad(codigo) & ")", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Detalle", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Lote", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                    table.AddCell(PhraseCell(New Phrase("Usuario", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))


                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 1, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separacion

                    'Registros 

                    'defino tabla
                    table = New PdfPTable(7)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True

                    'Ancho columnas
                    table.SetWidths(New Single() {0.1F, 0.1F, 0.15F, 0.1F, 0.3F, 0.15F, 0.15F})

                    table.SpacingBefore = 8.0F
                    phrase = New Phrase()

                    For j = 0 To 30

                        'este condicional es para cortar en la ultima pagina cuando tengo menos de 30 registros
                        If (registro_actual + j <= (ds.Tables(0).Rows.Count - 1)) Then


                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                            For k = 0 To ds.Tables(0).Rows(registro_actual + j).ItemArray.Length - 1

                                Select Case k


                                    Case "0"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "1"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "2"

                                        table.AddCell(PhraseCell(New Phrase(Left(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, 10), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "3"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "4"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "5"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                                    Case "6"

                                        table.AddCell(PhraseCell(New Phrase(ds.Tables(0).Rows(registro_actual + j).ItemArray(k).ToString, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))


                                    Case Else
                                        '...
                                End Select


                            Next


                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                        Else
                            'relleno con espacios 

                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase(" ", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'agrego un espacio en blanco

                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))
                            table.AddCell(PhraseCell(New Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT))

                            'fin espacio en blanco
                        End If



                    Next




                    'agrego lo generado en el case
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'solo agrego esto (tabla sin contenido) para separar


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})

                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 8, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'fin separacion



                    'pie de pagina


                    table = New PdfPTable(1)
                    table.TotalWidth = 800.0F
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'agrego una nueva pagina

                    document.NewPage()
                    pagina = pagina + 1

                    registro_actual += 30
                End While

                document.Close()
                Dim bytes As Byte() = memoryStream.ToArray()
                memoryStream.Close()
                cn.Close()
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", "attachment; filename=Estandares.pdf")
                Response.ContentType = "application/pdf"
                Response.Buffer = True
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.BinaryWrite(bytes)
                Response.[End]()
                Response.Close()
            End Using
        Catch ex As Exception
            If (ex.Message = "No hay ninguna fila en la posición 0.") Then
                Label2.Text = "No hay consumos registrados para este estandar"
            Else
                Label2.Text = "Error al generar el informe"
            End If


        End Try
    End Sub


    Private Shared Sub DrawLine(ByVal writer As PdfWriter, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal color As Color)
        Dim contentByte As PdfContentByte = writer.DirectContent
        contentByte.SetColorStroke(color)
        contentByte.MoveTo(x1, y1)
        contentByte.LineTo(x2, y2)
        contentByte.Stroke()
    End Sub
    Private Shared Function PhraseCell(ByVal phrase As Phrase, ByVal align As Integer) As PdfPCell
        Dim cell As New PdfPCell(phrase)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 2.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Private Shared Function ImageCell(ByVal path As String, ByVal scale As Single, ByVal align As Integer) As PdfPCell
        Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path))
        image.ScalePercent(scale)
        Dim cell As New PdfPCell(image)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 0.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function


End Class
