﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" EnableEventValidation="true" CodeFile="consulta_consumos.aspx.vb" Inherits="Estandares_consulta_estandares" title="Sistema Estándares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    
    
    <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />
    <link rel="stylesheet" href="../js/datatable/media/css/demo_page.css"type="text/css" />
    <link rel="stylesheet" href="../js/datatable/ColVis/media/css/ColVis.css"type="text/css" />
    <link rel="stylesheet" type="text/css" href="../js/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../js/DataTables/extensions/TableTools/css/dataTables.tableTools.css">
    
    
    
    <script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/demo.js"></script>


 <script type="text/javascript">
        function confirma_elimina() {
           return confirm('Confirma la eliminacion del consumo?');
        }
    </script>
    
    
    <script type="text/javascript" charset="utf-8"></script>
    
     <style type="text/css">
        	    input {	height: 15px;
}
        #ctl00_ContentPlaceHolder1_Div1 
{


margin:auto;

}
       </style>
    
<script language="javascript" type="text/javascript">


var asInitVals = new Array();
     $(document).ready(function () {
     
     var  oTable = $('#tbl').dataTable( {
    
    
     "dom": 'T<"clear">lfrtip',
      
           "oTableTools": {
                                                                "aButtons": [
                                                                                "xls",
                                                                                
                                                                            ]
                                                                },
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });
</script>
 <div id="titulo_seccion">
Administración de consumos
 </div>
<div  align= "center" >
    <asp:Button ID="Button2" 
            runat="server" Text=" + Cargar  Consumos " />
            <asp:Button ID="Button3" 
            runat="server" Text="Volver " />
    
      <span>
      
      &nbsp; Número de Vial
      <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </span>
<asp:Button ID="Button1" runat="server" Text=" Buscar " />
<asp:Button ID="btnReport" runat="server" Text="Generar Informe" />
 </div>      

  <br>
        <div id="Div1" style="width:80%" runat="server">
          
<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead>
                    <tr>
                        <th></th>
                        <th>Número Vial</th>
                        <th>Número de Apertura</th>
                        <th>Fecha Consumo</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                        <th>Lote</th>
                        <th>Usuario</th>
                        
                    </tr>
                    <tr>
			<td align="center"><input type="hidden" size="8" name="search_engine" value="" /></td>
			<td align="center"><input type="text" size="8" name="search_browser" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_platform" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser1" value="" class="search_init" /></td>
			
		</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
    
            <ItemTemplate>
                <tr>
                  <td align="center"><a><asp:Button ID="Button11" CommandName="Click" Text="Eliminar" runat="server" CommandArgument='<%# Eval("Codcon") %>' /></a></td>
                  <td align="center"><%#Eval("num_vial")%></td>
                  <td align="center"><%#Eval("Num_Ape")%></td>
                  <td align="center"><%#Eval("Fechaco", "{0:dd/MM/yyyy}")%></td>
                  <td align="center"><%#consumos(Eval("Cant"))%></td>
                  <td align="center"><%#Eval("Detalle")%></td>
                  <td align="center"><%#Eval("LoteP")%></td>
                  <td align="center"><%#Eval("Codusu")%></td>
                 
              

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
               <br>   <br>
  </div>  
    
      
        
        
     
  
 

    </form>
      <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
           <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div>
</asp:Content>

