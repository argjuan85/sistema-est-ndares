﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Imports System.Threading
Imports System.EventArgs
Imports FormsAuth
Imports System.Net.Mail



Partial Class Consumos_cargar_consumos
    Inherits System.Web.UI.Page
    Public codigo As String
    Dim band As Integer = 0





    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim aux As String
        Label1.Text = ""
        Label2.Text = ""
        Label3.Text = ""
        aux = fecha_consumo.Value
        nuevoconsumo()
        If (Session("nivel") <> nivel0) Then
            usuario.Value = User.Identity.Name
        End If


    End Sub

    Sub blanqueo()

        fecha_consumo.Value = DateTime.Now.ToString("dd/MM/yyyy")
        detalle.Value = ""
        lote.Value = ""
        cantidad.Value = ""
        apertura.Value = ""
        usuario.Value = User.Identity.Name
        If (Session("nivel") = nivel0) Then
            fecha_consumoadm.Value = ""
            usuario.Value = ""
            motivo.Value = ""
        End If
    End Sub
    'valido que solo puedan haber dos decimales despues del punto
    Sub validaconsumo(ByVal consumo As String)
        Dim indice As Integer = 0
        Dim cantidad As Integer

        cantidad = Len(consumo)
        indice = InStr(consumo, ".")
        If (((indice = cantidad - 1) Or (indice = cantidad - 2) Or (indice = cantidad - 3)) Or (indice = "0")) Then
            'ok
            band = 1
        Else
            'no
        End If
    End Sub
 
    Sub nuevoconsumo()
        Dim e As Consumos
        Dim r As String
        Dim es As Estandares
        Dim v As Viales
        Dim n As nombres
     
        Dim cod_via As String
        Dim ape As Integer = 0
        Dim nuevo_estado As String = "U"
        Dim aux1 As Double
        Dim aux2 As Double
        Dim aux As Integer = 1

        Dim idvialcons As Integer
        'estas lineas las coloco para evitar inconvenientes con el trato de decimales con el tipo de datos double tanto en ambiente de desarrollo como subido en el servidor
        Dim ri As New Globalization.CultureInfo("es-ES")
        ri.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = ri
        'log
        Dim l As logs
        Dim est As String
        Dim nom As String
        Dim lot As String
        Dim xfecha As String
        Dim fecha_guard As String
        Dim usuariocons As String = "0"
        Dim adPath As String = direccionad 'Path to your LDAP directory server
        Dim adAuth As LdapAuthentication = New LdapAuthentication(adPath)
        l = New logs()
        l.logs(conexion)

        Try
            es = New Estandares()
            es.Estandares(conexion)
            n = New nombres()
            n.Nombres(conexion)

            validaconsumo(cantidad.Value)
            If (cantidad.Value > "0.00") Then
                If (band) Then
                    'por seguridad hago doble control del sector al que pertenece el estandar
                    If (es.obtenersector(codigo) = Session("sector")) Then

                        'si el campo usuario esta vacio toma el user logueado, si no el que ingreso un administrador y lo valida via AD
                        If ((usuario.Value = "") Or (LdapAuthentication.validausuario(usuario.Value, Session("sector")))) Then
                            idvialcons = es.Obteneridvialactualuso(codigo)

                            If Not (idvialcons <> "-1") Then

                                idvialcons = es.Obteneridvialdisponible(codigo)

                            End If
                            'obtener remanente
                            v = New Viales()
                            v.Viales(conexion)
                            aux1 = v.obtenerremanente(idvialcons)
                            aux2 = cantidad.Value

                            If (aux2 <= aux1) Then
                                'id
                                cod_via = idvialcons



                                e = New Consumos()
                                e.Consumos(conexion)

                                'verifico que numero de apertura le corresponde, puesto que el vial solo almacena la cantidad de aperturas realizadas y no el proximo numero disponible ( esto es para los casos que se haya borrado una apertura que no sea la ultima)

                                If (e.validaapertura(cod_via, apertura.Value)) Then
                                    If (usuario.Value <> "") Then
                                        'valida que sea un usuario que exista y tenga permisos
                                        usuariocons = usuario.Value
                                    Else
                                        usuariocons = User.Identity.Name
                                    End If
                                    If (fecha_consumo.Value <> "") Then
                                        fecha_guard = fecha_consumo.Value
                                    Else
                                        fecha_guard = fecha_consumoadm.Value
                                    End If

                                    xfecha = cambiaformatofecha2(fecha_guard)
                                    xfecha = xfecha + " 00:00"
                                    r = e.Agregarconsumo(codigo, xfecha, detalle.Value, cod_via, usuariocons, cantidad.Value, lote.Value, apertura.Value, motivo.Value)
                                    'agregar condicion para que no registre mas nada
                                    If (r <> "-1") And (r.Length < "10") Then
                                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "(" + cantidad.Value + " " + es.obtenerunidad(codigo) + ") para estándar: " + es.obtenernombre1(codigo) + "(" + codigo + ")", "Consumos", r, "N/A")

                                        ' incremento la cant de aperturas del vial y cambio el estado de ser necesario



                                        'si se termina el vial lo coloco en estado cerrado

                                        If ((aux1 - cantidad.Value) = "0" Or (v.obtenerapertura(idvialcons) + 1 = "20")) Then
                                            nuevo_estado = "C"
                                            If (aux1 - cantidad.Value) = "0" Then
                                                Label3.Text = "Se consumió la totalidad del vial, Vial Cerrado."
                                            Else
                                                Label3.Text = "Se alcanzaron 20 aperturas para el vial, Vial Cerrado."
                                            End If



                                        End If
                                        v.Actualizarvial(idvialcons, codigo, nuevo_estado, v.obtenerapertura(idvialcons) + 1, aux1 - cantidad.Value, Nothing)


                                        est = es.obtenerestado(codigo)
                                        nom = n.obtenernombre(es.obtenernombre(codigo))
                                        lot = es.obtenerlote(codigo)


                                        'log modificacion vial

                                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion por consumo.", "Viales", idvialcons, "Vial N°: " + v.obtener_numerovial(idvialcons))

                                        'log

                                        'Actualizo el stock del estándar
                                        r = es.actualizarstock(codigo)
                                        If (r = "1") Then
                                            'log actualizacion de stock

                                            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por consumo: " + cantidad.Value + " en el vial n°: " + v.obtener_numerovial(cod_via), "Estandares", codigo, nom + " - " + lot)

                                            Label1.Text = "Se cargó el consumo con éxito"
                                            blanqueo()

                                            'chequeo minimo (mail)

                                            'si no esta desactivada la notificacion envio
                                            If (es.notificamail(codigo)) Then
                                                r = es.chequeominimo(codigo)
                                                If (r = True) Then
                                                    'envio un correo dando aviso
                                                    EnvioMail(es.obtenernombre(codigo), es.obtenerstock(codigo), es.obtenerunidad(codigo))

                                                End If
                                            End If


                                            'chequeo si hay cierre
                                            r = es.chequeocierre(codigo)
                                            If (r = False) Then
                                                'log de cierre


                                                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado de " + est + " a F", "Estandares", codigo, nom + " - " + lot)
                                                Label3.Text = Label3.Text + "Se consumió la totalidad del estándar. Se cambió a estado Cerrado"

                                            End If

                                        Else
                                            Label2.Text = "Error al actualizar el stock del estándar"

                                        End If

                                    Else
                                        Label2.Text = "Error al registrar el consumo en el estándar " + r
                                    End If

                                    Else
                                        Label2.Text = "El número de apertura ingresado ya ha sido cargado o bien ingresó un numero de apertura erróneo"
                                    End If

                                Else
                                    Label2.Text = "El consumo excede el remanente del Vial "
                                End If


                        Else
                            Label2.Text = "El usuario al que quiere imputar el consumo no existe o bien no tiene permisos para utilizar este sistema."

                        End If

                    Else
                        Label2.Text = "El estandar que esta tratando acceder pertenece a otro sector."
                    End If

                Else
                    Label2.Text = "Debe ingresar a lo sumo dos digitos despues del punto."
                End If
            Else
                Label2.Text = "No puede ingresar consumos negativos o nulos."
            End If

        Catch ex As Exception
            Label2.Text = "Error al cargar el consumo!"
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(128, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                Dim es As Estandares


                es = New Estandares()
                es.Estandares(conexion)
                codigo = DecryptText(Request.QueryString("ID"))
                titulo.InnerText = "Ingrese los datos del consumo. Vial Actual N°: " & es.calculavial_actual(codigo)

             

                labelcantidad.InnerText = "Cantidad (" & es.obtenerunidad(codigo) & ")"
                'si el estandar se cerro no puedo seguir consumiento

                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se pueden realizar consumos"

                End If

                'por seguridad hago doble control del sector al que pertenece el estandar

                If (es.obtenersector(codigo) <> Session("sector")) Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "Esta intentando acceder a un estandar que no es de su sector"

                End If

                'por requerimiento los analistas solo pueden cargar consumos el mismo dia( o en su defecto hasta 5 dias hacia atras)
                fecha_consumoadm.Visible = False
                If (Not Page.IsPostBack) Then
                    If (Not validapermiso(262144, Session("nivel"))) Then
                        fecha_consumo.Value = DateTime.Now.ToString("dd/MM/yyyy")
                        motivo.Visible = False
                        label_motivo.Visible = False
                        usuario.Value = User.Identity.Name
                        usuario.Disabled = True
                    Else
                        fecha_consumo.Visible = False
                        fecha_consumoadm.Visible = True

                    End If
                End If

            Catch ex As Exception
                Label2.Text = "Error al cargar datos del estandar"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If


        Dim message As String = "Confirma la carga del consumo?"
        Dim sb As New System.Text.StringBuilder()
        sb.Append("return confirm('")
        sb.Append(message)
        sb.Append("');")
        ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())


        ' Button1.Attributes.Add("onclick", "  alertMe();")
    End Sub

    Public Sub EnvioMail(ByVal estandar As String, ByVal remanente As String, ByVal unidad As String)

        Dim aux As String
        Dim es As Estandares
        Dim aux2 As String
        Dim message As New MailMessage
        es = New Estandares()
        es.Estandares(conexion)
        Dim smtp As New SmtpClient

        message.From = New MailAddress(mailsistema)

        If (Session("sector") = sectoradmin) Then
            message.To.Add(mailadmincc)
            aux = mailadmincc

        Else
            message.To.Add(mailadminve)
            aux = mailadminve
        End If

        message.Body = "El estándar: " & es.obtenernombre1(codigo) & " se encuentra por debajo del stock mínimo." & vbLf & vbLf

        message.Body = message.Body & "Stock Remanente: " & remanente & " " & unidad & "." & vbLf & vbLf

        message.Body = message.Body & "Este es un correo generado automáticamente por el sistema de estándares, no responda este correo."

        message.Subject = "Estándar por debajo del stock mínimo."

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = mailport

        smtp.Host = mailserver

        smtp.Credentials = New Net.NetworkCredential(mailuser, mailpass)

        smtp.Send(message)

    End Sub

    

End Class
