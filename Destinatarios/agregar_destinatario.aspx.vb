﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales

Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre1.Value).Length > minvalida) Then
            If (verificadestinatario(Trim(nombre1.Value))) Then
                nuevodestinatario()
                nombre1.Value = ""
                Label1.Text = "Se registró el destinatario correctamente"

            Else
                Label2.Text = "El nombre del destinatario ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"
        End If


    End Sub

   

    Sub nuevodestinatario()
        Dim u As Destinatarios
        Dim l As logs
        Dim r As Integer
        Dim xfecha As String


        Try
            u = New Destinatarios()
            u.Destinatarios(conexion)
            'log
            l = New logs()
            l.logs(conexion)
            r = u.AgregarDestinatario(Trim(nombre1.Value), "N")

            'log
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Alta de Destinatario", "Destinatario", r, nombre1.Value)



        Catch ex As Exception
            label2.text = "Error al crear destinatario"
        End Try
    End Sub
    Public Shared Function verificadestinatario(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Destinatarios
        u = New Destinatarios()
        u.Destinatarios(conexion)
        dtresultado = u.Consultadestinatarios2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (destinatario.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While

        dtresultado.Close()

        Return (band)


    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(2048, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

            Dim message As String = "Confirma la carga del destinatario?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
        End If

    End Sub

  
End Class
