﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(8192, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo)
                Button2.Attributes.Add("onclick", " return confirma_elimina();")
                Button1.Attributes.Add("onclick", " return confirma_modifica();")

                '  Dim message As String = "Confirma la modificacion del destinatario?"
                '  Dim sb As New System.Text.StringBuilder()
                '  sb.Append("return confirm('")
                '  sb.Append(message)
                '  sb.Append("');")
                '  ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())


                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscardestinatario(codigo) 'carga la interfaz
                End If
            Catch
                Label2.Text = "Error al cargar datos del destinatario"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If
    End Sub



    Sub buscardestinatario(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Destinatarios
        Try

            u = New Destinatarios() ' nuevo objeto instancia
            u.Destinatarios(conexion) 'invoco el constructor y paso parametros de conexion
            dtresultado = u.Consultardestinatarios1(codigo)



            While (dtresultado.Read())

                nombre1.Value = dtresultado(1).ToString()
               

            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda del destinatario"
        End Try
    End Sub
    Sub actualizardestintatario(ByVal codigo As String)
        Dim u As Destinatarios
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)

        Try
            u = New Destinatarios() ' nuevo objeto instancia
            u.Destinatarios(conexion) 'invoco el constructor y paso parametros de conexion

            'Label1.Text = "conexion con exito"

          
            r = u.ActualizarDestinatario(codigo, Trim(nombre1.Value), "S")
            'log
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Destinatario", "Destinatario", codigo, nombre1.Value)
            Label1.Text = "Se registró el destinatario correctamente"

        Catch ex As Exception
            Label2.Text = "Se produjo un error al actualizar el destinatario"
            'Label1.Text = "Conexion Error" + ex.Message.ToString()
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre1.Value).Length > "2") Then
            If (verificadestinatario(Trim(nombre1.Value))) Then
                actualizardestintatario(codigo)



            Else
                Label2.Text = "El nombre del destinatario ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"


        End If

     
    End Sub

    Public Shared Function verificadestinatario(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Destinatarios
        u = New Destinatarios() ' nuevo objeto instancia
        u.Destinatarios(conexion) 'invoco el constructor y paso parametros de conexion
        dtresultado = u.Consultadestinatarios2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (destinatario.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While

        dtresultado.Close()

        Return (band)


    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim u As Destinatarios
        Dim r As Boolean
        Dim dtresultado As SqlDataReader
        Dim dtresultado2 As SqlDataReader
        Dim idreg As Integer
        Dim nomreg As String
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)

        u = New Destinatarios()
        u.Destinatarios(conexion)

        ' reviso que no este asociado
        dtresultado = u.Consultardestinatariosasoc(codigo)
        dtresultado2 = u.Consultardestinatarios1(codigo)
        If (dtresultado2.Read()) Then
            idreg = dtresultado2(0).ToString()
            nomreg = dtresultado2(1).ToString()
        End If
        dtresultado2.Close()
        If (Not dtresultado.Read()) Then

            ' elimino  el lic
            r = u.BorrarDestinatario(codigo)
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Destinatario", "Destinatario", idreg, nomreg)

            Response.Redirect("consultar_destinatarios.aspx")
        Else
            Label1.Text = "El Destinatario esta asociado a envíos, no se puede eliminar"


        End If
        dtresultado.Close()
    End Sub
End Class
