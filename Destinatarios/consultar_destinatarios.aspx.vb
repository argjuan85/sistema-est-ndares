﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Destinatarios_consultar_destinatarios
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(4096, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

            If (Session("nivel") = nivel4 Or Session("nivel") = nivel3 Or Session("nivel") = nivel2) Then
                'Button1.Visible = False
                Button2.Visible = False
                Button4.Visible = False
                labeltit.Visible = False
                'TextBox1.Visible = False
                Label2.Text = " No tiene permisos para acceder a esta página"

            End If
        End If
    End Sub

    Sub buscardestinatarios()
        Dim u As Destinatarios

        Try
            u = New Destinatarios()
            u.Destinatarios(conexion)
            repeater.DataSource = u.Consultardestinatarios()
            repeater.DataBind()
        Catch ex As Exception
            Label2.Text = " Error al realizar la búsqueda de destinatarios " + ex.Message

        End Try
    End Sub



    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim Codigox As String
        

        'si es modificar

        If (e.CommandName = "Click1") Then
            'get the id of the clicked row
            Codigox = Convert.ToString(e.CommandArgument)
            Response.Redirect("editar_destinatario.aspx?ID=" + EncryptText(Codigox))

        End If




    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        buscardestinatarios()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_destinatario.aspx")
    End Sub

 
    Sub nuevodestinatario()
        Dim u As Destinatarios
        Dim l As logs
        Dim r As Integer
        Dim test As String

        Try
            u = New Destinatarios()
            u.Destinatarios(conexion)
            'log
            l = New logs()
            l.logs(conexion)
            test = u.AgregarDestinatariox("cualquiera", "N")

            'log
            ' l.Agregarlog(DateTime.Now.ToString("dd/MM/yyyy HH:mm"),  User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", Nothing, "Alta de Destinatario", "Destinatario", r, nombre1.Value)
            Label2.Text = test

        Catch ex As Exception
            label2.text = "Error al crear destinatario"
        End Try
    End Sub
End Class
