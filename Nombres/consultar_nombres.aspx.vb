﻿Imports System.Data.SqlClient 'no nos va traer problema con la cantidad de conexiones, como si sucede con odbc, odbc no nos trae drama con la paginacion por ejemplo
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_consultar_licenciantes
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))

        ElseIf (Not validapermiso(32768, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else


            If (Session("nivel") = nivel4 Or Session("nivel") = nivel3 Or Session("nivel") = nivel2) Then

                Button2.Visible = False
                Button4.Visible = False
                labeltit.Visible = False
                Label2.Text = " No tiene permisos para acceder a esta página"

            End If
        End If

    End Sub

    Sub buscarnombres()
        Dim u As nombres
        Try
            u = New nombres()
            u.Nombres(conexion)
            repeater.DataSource = u.Consultarnombres()
            repeater.DataBind()
        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda de Nombres"

        End Try
    End Sub


    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim Codigox As String

        'si es modificar


        If (e.CommandName = "Click1") Then
            'get the id of the clicked row

            Codigox = Convert.ToString(e.CommandArgument)
            Response.Redirect("editar_nombre.aspx?ID=" + EncryptText(Codigox))

        End If

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_nombre.aspx")
    End Sub

   
    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        buscarnombres()
    End Sub
End Class
