﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(65536, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                Button2.Attributes.Add("onclick", " return confirma_elimina();")
                Button1.Attributes.Add("onclick", " return confirma_modifica();")

                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarnombre(codigo) 'carga la interfaz
                End If

            Catch
                Label2.Text = "Error al cargar datos del nombre del estandar"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try

        End If
    End Sub


 

    Sub buscarnombre(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As nombres
        Try
            u = New nombres()
            u.Nombres(conexion)
            dtresultado = u.Consultarnombre1(codigo)
            nombre1.Value = ""

            While (dtresultado.Read())

                nombre1.Value = dtresultado(2).ToString()
                If (dtresultado(1).ToString() = "N") Then
                    habilitado.Checked = True
                Else
                    habilitado.Checked = False
                End If

            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al buscar el nombre del estandar"

        End Try
    End Sub
    Sub actualizarnombre(ByVal codigo As String)
        Dim u As nombres
        Dim r As Boolean
        Dim l As logs
        Dim aux As Integer
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)
        Try
            u = New nombres()
            u.Nombres(conexion)
            If (habilitado.Checked = True) Then
                aux = "1"
            Else
                aux = "0"
            End If


            r = u.Actualizarnombre(codigo, nombre1.Value, aux)
            'log
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica nombre de estandar", "Nombres", codigo, nombre1.Value)
            Label1.Text = "Se modificó el nombre del estándar correctamente"

        Catch ex As Exception
            Label2.Text = "Error al actualizar nombre del estandar"

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dtresultado As SqlDataReader
        Dim u As nombres
        u = New nombres()
        u.Nombres(conexion)
        Label1.Text = ""
        Label2.Text = ""


        If (Trim(nombre1.Value).Length > "2") Then
            If (verificanombre(Trim(nombre1.Value))) Then
                ' reviso que no este asociado
                If (habilitado.Checked = False) Then
                    dtresultado = u.Consultarnombresasoc(codigo)

                    If (Not dtresultado.Read()) Then
                        actualizarnombre(codigo)
                    Else
                        Label1.Text = "El Nombre esta asociado a estándares, no se puede deshabilitar"


                    End If
                    dtresultado.Close()
                Else
                    actualizarnombre(codigo)
                End If

            Else
                Label2.Text = "El nombre del estandar ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
            Else
                Label2.Text = "Debe ingresar al menos 3 caracteres"


            End If


    End Sub
    Public Shared Function verificanombre(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As nombres
        u = New nombres()
        u.Nombres(conexion)
        dtresultado = u.Consultanombres2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (destinatario.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While
        dtresultado.Close()
        Return (band)

    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim u As nombres
        Dim r As Boolean
        Dim dtresultado As SqlDataReader
        Dim dtresultado2 As SqlDataReader
        Dim idreg As Integer
        Dim nomreg As String
        Dim xfecha As String
        u = New nombres()
        u.Nombres(conexion)
        Dim l As logs

        'log
        l = New logs()
        l.logs(conexion)
        Try



            dtresultado2 = u.Consultarnombre1(codigo)
            If (dtresultado2.Read()) Then
                idreg = dtresultado2(0).ToString()
                nomreg = dtresultado2(2).ToString()
            End If

            dtresultado2.Close()

            ' reviso que no este asociado
            dtresultado = u.Consultarnombresasoc(codigo)

            If (Not dtresultado.Read()) Then

                ' elimino  el lic
                r = u.Borrarnombre(codigo)
                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Nombre", "Nombres", idreg, nomreg)

                Response.Redirect("consultar_nombres.aspx")
            Else
                Label1.Text = "El Nombre esta asociado a estándares, no se puede eliminar"


            End If
            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al eliminar nombre del estandar"
        End Try

    End Sub
End Class
