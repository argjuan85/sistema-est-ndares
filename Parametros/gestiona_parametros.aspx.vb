﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Partial Class Parametros_gestiona_parametros
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click


        Label1.Text = ""
        Label2.Text = ""

        'valida mail
        If (True) Then

            nuevomail()

            'nombre1.Value = ""

        Else
            Label2.Text = "La dirección ingresada contiene caracteres invalidos"
            Return
        End If





    End Sub


    Sub nuevomail()
        Dim u As Parametros
        Dim r As Integer
        Dim sectorcorreo As String
        Try
            u = New Parametros()
            u.Parametros(conexion)
            If Session("sector") = sectoradmin Then
                sectorcorreo = "mailadmincc"
            Else
                sectorcorreo = "mailadminve"
            End If

            r = u.actualizar_parametro_nombre(sectorcorreo, nombre1.Value)
            Label1.Text = "Se registró la direccion correctamente"
        Catch ex As Exception
            Label2.Text = "Error al guardar direccion"
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(131072, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else



            Dim u As Parametros
            Dim sectorcorreo As String
            If Not (Page.IsPostBack) Then
                If Session("sector") = sectoradmin Then
                    sectorcorreo = "mailadmincc"
                Else
                    sectorcorreo = "mailadminve"
                End If


                u = New Parametros()
                u.Parametros(conexion)
                nombre1.Value = u.consulta_valor_nombre(sectorcorreo)
                Label1.Text = "Si desea ser agregado o quitado de la lista, favor de solicitarlo al sector de Mesa de Ayuda."
            End If
            nombre1.Disabled = True
            Button1.Visible = False
        End If
    End Sub
End Class
