﻿Imports globales

Partial Class Logs_consulta_logs
    Inherits System.Web.UI.Page
    Dim sql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(131072, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

            If (Session("nivel") = nivel4 Or Session("nivel") = nivel3 Or Session("nivel") = nivel2) Then
                Button1.Visible = False
                'labeldetalle.Visible = False
                'labeltipo.Visible = False
                'labeluser.Visible = False
                'labeltit.Visible = False
                'user.Visible = False
                'tipo.Visible = False
                'detalle.Visible = False

                Label2.Text = " No tiene permisos para acceder a esta página"

            Else

            End If


        End If
    End Sub

    Sub buscarlogs(ByVal sql As String)
        Dim u As logs
        Try
            u = New logs()
            u.logs(conexion)
            repeater.DataSource = u.Consultarlogs(sql)
            repeater.DataBind()
        Catch ex As Exception
            Label2.Text = " Hubo un error al buscar logs "

        End Try
    End Sub


    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim aux As Integer = 0
        Dim xfechad As String
        Dim xfechah As String
        sql = "Select * from Audit"

        If detalle.Text <> "" Then
            If (aux = "0") Then
                sql = sql & " where Obs like '%" & detalle.Text & "%' "
                aux = 1
            Else
                sql = sql & " and Obs like '%" & detalle.Text & "%' "
            End If

        End If

        If tipo.Text <> "" Then
            If (aux = "0") Then
                sql = sql & " where [Tran] like '%" & tipo.Text & "%' "
                aux = 1
            Else
                sql = sql & " and [Tran] like '%" & tipo.Text & "%' "
            End If
        End If


        If user.Text <> "" Then
            If (aux = "0") Then
                sql = sql & " where [User] like  '%" & user.Text & "%' "
                aux = 1
            Else
                sql = sql & " and [User] like '%" & user.Text & "%' "
            End If
        End If
        'en el prog, fue necesario cambiar la funcion cambiaformatofecha por cambiaformatofecha2, estono fue necesario en ambiente de pq
        xfechad = cambiaformatofecha2(fecha_desde.Value)
        xfechah = cambiaformatofecha2(fecha_hasta.Value)

        If (aux = "0") Then
            sql = sql & " where ((Fecha >= '" & xfechad & " 00:00:00.000" & "') and (Fecha <= '" & xfechah & " 23:59:00.000" & "'))"
            aux = 1
        Else
            sql = sql & " and ((Fecha >= '" & xfechad & " 00:00:00.000" & "') and (Fecha <= '" & xfechah & " 23:59:00.000" & "'))"
        End If
        buscarlogs(sql)

    End Sub

    Protected Function Pc(ByVal objGrid As Object) As String
        Dim aux As String
        aux = objGrid
        aux = StrReverse(aux)
        aux = Mid(aux, 13)
        aux = StrReverse(aux)
        Return aux


    End Function
End Class
