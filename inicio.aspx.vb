﻿Imports FormsAuth
Imports globales
Partial Class inicio
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' If InStr(Request.ServerVariables("HTTP_USER_AGENT"), "MSIE") Then
        'Login.Visible = False
        'Response.Write("<B>Para optimizar el funcionamiento del sistema, por favor utilizar Chrome o Firefox.</B>")

        'End If
        'valido IE para que no ingrese
        If Request.Browser.Browser = "IE" Or Request.Browser.Browser = "Mozilla" Then
            txtUsername.Visible = False
            txtPassword.Visible = False
            btnLogin.Visible = False
            Label2.Visible = False
            Label3.Visible = False
            errorLabel.Text = " Por favor ejecute el sistema desde Chrome, si no está instalado en el equipo por favor comuníquese con Mesa de Ayuda (Int. 2025) "
        Else


        End If
    End Sub

    Protected Sub Login_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim adPath As String = direccionad 'Path to your LDAP directory server
        Dim adAuth As LdapAuthentication = New LdapAuthentication(adPath)
        Dim loguin As String
        Try
            If (cargaparametros()) Then
                loguin = adAuth.IsAuthenticated(Trim(txtUsername.Text), txtPassword.Text)
                If ("-1" <> loguin) Then
                    Dim groups As String = adAuth.GetGroups()
                     Select loguin
                        Case "1"
                            Session("nivel") = nivel1
                            Session("sector") = sectoradmin
                        Case "2"
                            Session("nivel") = nivel2
                            Session("sector") = sectoradmin
                        Case "3"
                            Session("nivel") = nivel3
                            Session("sector") = sectoradmin
                        Case "4"
                            Session("nivel") = nivel4
                            Session("sector") = sectoradmin
                        Case "5"
                            Session("nivel") = nivel4
                            Session("sector") = sectorsecun
                        Case "6"
                            Session("nivel") = nivel1
                            Session("sector") = sectorsecun
                        Case "7"
                            Session("nivel") = nivel0
                            Session("sector") = sectorsecun
                        Case "8"
                            Session("nivel") = nivel0
                            Session("sector") = sectoradmin
                        Case Else

                            '...
                    End Select
                          
                            'Create the ticket, and add the groups.
                            Dim isCookiePersistent As Boolean = chkPersist.Checked
                            Dim authTicket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, _
                                 txtUsername.Text, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, groups)

                            'Encrypt the ticket.
                            Dim encryptedTicket As String = FormsAuthentication.Encrypt(authTicket)

                            'Create a cookie, and then add the encrypted ticket to the cookie as data.
                            Dim authCookie As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)

                            If (isCookiePersistent = True) Then
                                authCookie.Expires = authTicket.Expiration
                            End If
                            'Add the cookie to the outgoing cookies collection.
                            Response.Cookies.Add(authCookie)


                            'You can redirect now.
                            Response.Redirect(FormsAuthentication.GetRedirectUrl(txtUsername.Text, False))

                Else
                    errorLabel.Text = "Error de conexion o de permisos. Contacte al administrador"
                End If

            Else
                errorLabel.Text = "Error al cargar los parametros."
            End If
        Catch ex As Exception
            errorLabel.Text = ex.Message

        End Try
    End Sub
    Public Function cargaparametros() As Boolean
        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        Try
            nivel0 = p.consulta_valor_nombre("nivel0")
            nivel1 = p.consulta_valor_nombre("nivel1")
            nivel2 = p.consulta_valor_nombre("nivel2")
            nivel3 = p.consulta_valor_nombre("nivel3")
            nivel4 = p.consulta_valor_nombre("nivel4")
            grupoadminsup = Trim(p.consulta_valor_nombre("grupoadminsup"))
            grupoadminsupve = Trim(p.consulta_valor_nombre("grupoadminsupve"))
            grupoadmin = Trim(p.consulta_valor_nombre("grupoadmin"))
            grupoadminve = Trim(p.consulta_valor_nombre("grupoadminve"))
            grupoconsulta = Trim(p.consulta_valor_nombre("grupoconsulta"))
            grupoestandares = Trim(p.consulta_valor_nombre("grupoestandares"))
            grupoanalistas = Trim(p.consulta_valor_nombre("grupoanalistas"))
            grupoanalistasve = Trim(p.consulta_valor_nombre("grupoanalistasve"))
            rutaexcel = Trim(p.consulta_valor_nombre("rutaexcel"))
            rutapdf = Trim(p.consulta_valor_nombre("rutapdf"))
            informe_EP = p.consulta_valor_nombre("informe_EP")
            informe_ES = p.consulta_valor_nombre("informe_ES")
            informe_ET = p.consulta_valor_nombre("informe_ET")
            informe_PP = p.consulta_valor_nombre("informe_PP")
            sops = p.consulta_valor_nombre("sops")
            maxviales = p.consulta_valor_nombre("maxviales")
            mailsistema = p.consulta_valor_nombre("mailsistema")
            mailadmincc = p.consulta_valor_nombre("mailadmincc")
            mailadminve = p.consulta_valor_nombre("mailadminve")
            mailport = p.consulta_valor_nombre("mailport")
            mailserver = p.consulta_valor_nombre("mailserver")
            mailuser = p.consulta_valor_nombre("mailuser")
            mailpass = p.consulta_valor_nombre("mailpass")
            Return True


        Catch ex As Exception
            Return False
            'Throw New Exception("Error obtaining group names. " & ex.Message)
        End Try
    End Function
End Class
