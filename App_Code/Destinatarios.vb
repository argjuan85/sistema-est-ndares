﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Destinatarios
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Destinatarios(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultardestinatarios() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand("select * from Destinatarios order by nombre ", OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception
            MsgBox("hubo un error" + ex.Message)
        End Try
        Return dtable
    End Function
    Public Function Consultardestinatariosasoc(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Destinatarios d INNER JOIN Envios e on e.CodDest = d.destinatario_id where e.CodDest=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function Consultardestinatarios1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Destinatarios where destinatario_id=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'consulta todos los destinatarios en un datareader
    Public Function Consultadestinatarios2() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Destinatarios", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function BuscarDestinatario(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Destinatarios where nombre like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function buscardestinatario_paginacion(ByVal cadena As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable()
        Try
            Dim odbconn As SqlConnection
            odbconn = New SqlConnection(strconexion)
            odbconn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter()
            adapter.SelectCommand = New SqlCommand("Select * from Destinatarios where nombre like '%" + cadena + "%' ", odbconn)
            adapter.Fill(dtable)
            odbconn.Close()
        Catch ex As Exception
        End Try
        Return dtable
    End Function

    Public Function BorrarDestinatario(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Destinatarios where destinatario_id=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function AgregarDestinatario(ByVal nombre As String, ByVal deshabilitado As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into Destinatarios (nombre,deshabilitado) values ('" + nombre + "','" + deshabilitado + "');  SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return -1
        End Try
        Return id
    End Function
    Public Function AgregarDestinatariox(ByVal nombre As String, ByVal deshabilitado As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into Destinatarios (nombre,deshabilitado) values ('" + nombre + "','" + deshabilitado + "');  SELECT SCOPE_IDENTITY();", odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return ex.Message
        End Try
        Return "anduvo bien =/"
    End Function

    Public Function ActualizarDestinatario(ByVal ID As String, ByVal nombre As String, ByVal deshabilitado As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Destinatarios set nombre='" + nombre + "',deshabilitado='" + deshabilitado + "' where destinatario_id='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
