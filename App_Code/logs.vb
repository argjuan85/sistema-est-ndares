﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class logs

#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub logs(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultarlogs(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    Public Function Consultarlogs1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Audit where CodAudit=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function


    Public Function Buscarlog(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Consumos where CodCOn like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

  


    Public Function Agregarlog(ByVal fecha As String, ByVal user As String, ByVal pc As String, ByVal tran As String, ByVal obs As String, ByVal tabla As String, ByVal idregistro As String, ByVal registro As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into Audit (Fecha,[User],Pc,[Tran],Obs, tabla, Idregistro, registro) values ('" + fecha + "','" + user + "','" + pc + "','" + tran + "','" + obs + "','" + tabla + "','" + idregistro + "','" + registro + "');"
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True




    End Function

    
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
