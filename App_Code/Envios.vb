﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Envios


#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Envios(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultarenvios() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand("select * from Envios order by CodEnv ", OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    'Devuelve los datos de un envio en particular
    Public Function Consultarenviox1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String

        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select * from Envios where CodEnv=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception

        End Try
        Return dtresultado
    End Function
    'Devuelve la cantidad enviada dado un id
    Public Function obtenercantidadenviada(ByVal ID As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim cantidad As Integer
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select * from Envios where CodEnv=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                cantidad = dtresultado(2)
            End If
            dtresultado.Close()
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return cantidad
    End Function
    'Devuelve tdos los envios registrados para 1 estandar
    Public Function Consultarenvio1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select e.CodEnv, v.Codvia, e.Detalle, e.Cant, e.fecha, d.nombre from Viales v INNER JOIN Envios e ON e.CodEnv=V.CodEnv INNER JOIN Destinatarios d ON e.CodDest=d.destinatario_id where V.CodEst=" + ID ' + " group by e.CodEnv"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function


    Public Function Buscarconsumo(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Consumos where CodCOn like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function


    Public Function BorrarEnvio(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Envios where CodEnv=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function



    Public Function Agregarenvio(ByVal fecha As String, ByVal detalle As String, ByVal cantidad As String, ByVal Destinatario As String) As Integer

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into Envios (Detalle,Cant,CodDest,Fecha) values ('" + detalle + "','" + cantidad + "','" + Destinatario + "','" + fecha + "'); SELECT SCOPE_IDENTITY();", odbcconn)
            ' deberia actualizar el vial  cambiando estado en caso de ser necesario
            'id = odbccom.ExecuteNonQuery()
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()


        Catch ex As Exception
            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarvial(ByVal ID As String, ByVal CodEst As String, ByVal Estado As String, ByVal num_ape As String, ByVal num_vial As String, ByVal remanente As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Viales set CodEst='" + CodEst + "',Estado='" + Estado + "',num_ape='" + num_ape + "',num_vial='" + num_vial + "',remanente='" + remanente + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


End Class
