﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data


Public Class Stock
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Stock(ByVal strconex As String)
        strconexion = strconex
    End Sub

    'consulta todos los registros cargador en un datareader
    Public Function Consultastock() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from fechastock", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function Agregarstock(ByVal fecha As String, ByVal obs As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As Integer
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into fechastock (tipo_estandar,fecha, observaciones) values ('EP','" + fecha + "','" + obs + "');  SELECT SCOPE_IDENTITY();", odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            Return -1
        End Try
        Return id
    End Function

    Public Function Consultarstock() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Dim sql As String
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            sql = "select * from fechastock order by fecha "
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function

    Public Function ActualizarStock(ByVal ID As String, ByVal fecha As String, ByVal obs As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update fechastock set fecha='" + fecha + "',observaciones='" + obs + "' where id='" + ID + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function Consultarstock1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from fechastock where id=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function Actualizarfechastock(ByVal ID As String, ByVal fecha As String, ByVal obs As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update fechastock set fecha='" + fecha + "',observaciones= '" + obs + "' where id='" + ID + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
   
  
    Public Function obtenerfecha(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim stock As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select fecha from fechastock where id=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                dtresultadof.Read()
                stock = dtresultadof(0)
                dtresultadof.Close()
                odbcconn.Close()
            Else
                stock = "-1"
            End If

        Catch ex As Exception
        End Try
        Return stock
    End Function
    'devuelve la fecha mas recien de control de stock
    Public Function obtenerultimostock() As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim stock As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select fecha from fechastock order by fecha desc"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                dtresultadof.Read()
                stock = dtresultadof(0)
                dtresultadof.Close()
                odbcconn.Close()
            Else
                stock = "-1"
            End If

        Catch ex As Exception
        End Try
        Return stock
    End Function

    Public Function Borrarstock(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from fechastock where id=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

   
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


End Class
