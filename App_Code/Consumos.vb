﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Consumos

#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Consumos(ByVal strconex As String)
        strconexion = strconex
    End Sub
    'Devuelve los datos de un consumo en particular
    Public Function Consultarconsumosx1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String

        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select * from Consumos where CodCon=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception

        End Try
        Return dtresultado
    End Function
    'Devuelve tdos los consumos registrados para 1 estandar
    Public Function Consultarconsumo1(ByVal ID As String, ByVal vial As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (vial <> "") Then
                sql = "select c.Codcon, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, v.num_vial, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodEst=" + ID + " and v.num_vial=" + vial + "order by v.num_vial, c.num_ape asc"
            Else
                sql = "select c.Codcon, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, v.num_vial, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodEst=" + ID + "order by v.num_vial, c.num_ape asc"
            End If

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'Devuelve tdos los consumos registrados para 1 vial en particular
    Public Function Consultarconsumovial(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select c.Codcon, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, v.num_vial, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodVia=" + ID + "order by c.Num_Ape"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'Verifica que el numero de apertura a argar no haya sido ingresado previamente y no se a mayor a 20
    Public Function validaapertura(ByVal ID As String, ByVal ape As Integer) As Boolean
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim resu As Boolean = True

        Dim sql As String
        Try
            If (ape < "21" And ape > "0") Then
                odbcconn = New SqlConnection(strconexion)
                odbcconn.Open()
                sql = "select c.Codcon, c.Num_Ape, c.Fechaco, c.Cant, c.Detalle, c.Lotep, v.num_vial, c.Codusu from Viales V INNER JOIN Consumos c ON c.CodVia=v.CodVia where c.CodVia=" + ID + "order by c.Num_Ape"
                odbccom = New SqlCommand(sql, odbcconn)
                dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
                If (dtresultado.HasRows) Then
                    While (dtresultado.Read())
                        If ((dtresultado(1).ToString = ape) And (resu = True)) Then
                            resu = False

                        End If
                    End While

                End If
                dtresultado.Close()
                odbcconn.Close()
            Else
                resu = False
            End If

        Catch ex As Exception
        End Try
        Return resu
    End Function
    Public Function eliminarconsumo(ByVal ID As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Consumos where Codcon=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception

            Return ex.Message
        End Try
        Return "1"
    End Function


    Public Function Buscarconsumo(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Consumos where CodCOn like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function obtenercant(ByVal ID As String) As Double
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim cantidad As Double
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Cant from Consumos where CodCon=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                cantidad = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return cantidad
    End Function

    Public Function obtenercodv(ByVal ID As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim codigo As Integer
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CodVia from Consumos where CodCon=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                codigo = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return codigo
    End Function



    Public Function BorrarConsumo(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Consumos where CodCon=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function Agregarconsumo(ByVal codest As String, ByVal fecha As String, ByVal detalle As String, ByVal cod_vial As String, ByVal cod_user As String, ByVal cantidad As String, ByVal lote As String, ByVal NumApe As String, ByVal motivo As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (motivo = "") Then
                odbccom = New SqlCommand("insert into Consumos (CodEst,Tipo,FechaCo,Detalle,Cant,LoteP,CodVia,Codusu,Num_Ape) values ('" + codest + "','" + "C" + "','" + fecha + "','" + detalle + "','" + cantidad + "','" + lote + "','" + cod_vial + "','" + cod_user + "','" + NumApe + "'); SELECT SCOPE_IDENTITY();", odbcconn)
            Else
                odbccom = New SqlCommand("insert into Consumos (CodEst,Tipo,FechaCo,Detalle,Cant,LoteP,CodVia,Codusu,Num_Ape,motivo_autorizacion) values ('" + codest + "','" + "C" + "','" + fecha + "','" + detalle + "','" + cantidad + "','" + lote + "','" + cod_vial + "','" + cod_user + "','" + NumApe + "','" + motivo + "'); SELECT SCOPE_IDENTITY();", odbcconn)
            End If

            ' deberia actualizar el vial  cambiando estado en caso de ser necesario
            id = odbccom.ExecuteScalar().ToString()

            odbcconn.Close()
        Catch ex As Exception
            Return -1
            'Return ex.Message si se descomenta esta linea tener cuidado con la longitud de la cadena que se esta validando en el retorno de esta funcion
        End Try
        Return id

    End Function

    Public Function Actualizarvial(ByVal ID As String, ByVal CodEst As String, ByVal Estado As String, ByVal num_ape As String, ByVal num_vial As String, ByVal remanente As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Viales set CodEst='" + CodEst + "',Estado='" + Estado + "',num_ape='" + num_ape + "',num_vial='" + num_vial + "',remanente='" + remanente + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

