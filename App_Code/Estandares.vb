﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports FormsAuth




Public Class Estandares
#Region "Atributos"
    Public strconexion As String
    Public id As String
    Public nombre As String
    Public lote As String
    Public proveedor As String
    Public catalogo As String
    Public cas As String
    Public tipo As String
    Public presentacion As String
    Public unidad As String
    Public fecha_vencimiento As String
    Public acondicionamiento As String
    Public fecha_ingreso As String
    Public cantidad_pedida As String
    Public cod_qad As String
    Public estado As String
    Public arch As String
    Public stock As String
    Public fecha_creacion As String
    Public arch1 As String
    Public observaciones As String
    Public sector As String
    Public alarma_stock As String
    Public sedronar As String
    Public aviso As String
                               
                              
                              

#End Region


#Region "Metodos"
    Public Sub Estandares(ByVal strconex As String)
        strconexion = strconex
    End Sub

 

   
    Public Function Consultaestandares(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            ' adapter.SelectCommand = New SqlCommand("select * from Estandard order by nombre ", OdbcConn)
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            'esto lo agrego por inconvenientes con la bd esta bajo testeo.
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    Public Function Consultarestandares1(ByVal ID As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Estandard where CodEst=" + ID, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function
    Public Function obtenerstock(ByVal ID As String) As Double
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim stock As Double
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select Stock from Estandard where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            stock = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return stock
    End Function
    Public Function obtenerunidad(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim uni As String
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select Uni from Estandard where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            uni = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return uni
    End Function
    Public Function obtenerarch(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim archi As String
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select Arch from Estandard where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            archi = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return archi
    End Function
    Public Function obtenerarch1(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim archi As String
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select Arch1 from Estandard where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            archi = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return archi
    End Function

    

    'devuelvo id de estandares cuyo adjunto tenga un nombre en particular  (certificados y hojas)

    Public Function obtener_estandares_certificados(ByVal ID As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CodEst from Estandard where Arch='" + ID + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function
    Public Function obtener_estandares_hojas(ByVal ID As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim archi As String
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select CodEst from Estandard where Arch1='" + ID + "'"

            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'dtresultadof.Read()
            'archi = dtresultadof(0)
            'dtresultadof.Close()
            'odbcconn.Close()
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function


    Public Function obtenerstockminimo(ByVal ID As String) As Double
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim stock As Double
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select AlarmaStock from Estandard where CodEst=" + ID, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            stock = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return stock
    End Function
    Public Function Consultarestandares1_test(ByVal ID As String) As Estandares
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As Estandares
        es = New Estandares()
        es.Estandares(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Estandard where CodEst=" + ID, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            es.id = dtresultadof(0).ToString()
            es.nombre = dtresultadof(1).ToString()
            es.lote = dtresultadof(2).ToString()
            es.proveedor = dtresultadof(3).ToString()
            es.catalogo = dtresultadof(4).ToString()
            es.cas = dtresultadof(5).ToString()
            es.tipo = dtresultadof(6).ToString()
            es.presentacion = dtresultadof(7).ToString()
            es.unidad = dtresultadof(8).ToString()
            es.fecha_vencimiento = dtresultadof(9).ToString()
            es.acondicionamiento = dtresultadof(10).ToString()
            es.fecha_ingreso = dtresultadof(11).ToString()
            es.alarma_stock = dtresultadof(12).ToString()
            es.cantidad_pedida = dtresultadof(13).ToString()
            es.cod_qad = dtresultadof(14).ToString()
            es.sector = dtresultadof(15).ToString()
            es.estado = dtresultadof(17).ToString()
            es.arch = dtresultadof(18).ToString()
            es.stock = dtresultadof(19).ToString()
            es.fecha_creacion = dtresultadof(20).ToString()
            es.arch1 = dtresultadof(21).ToString()
            es.observaciones = dtresultadof(22).ToString()
            es.sedronar = dtresultadof(23).ToString()
            es.aviso = dtresultadof(24).ToString()
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador

        Catch ex As Exception
        End Try
        Return es
    End Function



    'consulta estandares vencidos
    Public Function Consultavencidos() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim FD As String
        Dim sql As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            ' necesito que este en el formato año mes dia, puesto que los datos viejos fueron guardados como string  y es la unica forma de comparar fechas 
            FD = String.Format("{0:yyyy/MM/dd}", Date.Now())
            sql = "Select * from Estandard "
            sql = sql & " where ((Estandard.Est = 'H') or (Estandard.Est = 'D') or (Estandard.Est = 'V')) "
            sql = sql & " and ((Estandard.FeVto < " & "'" & FD & "'" & ") and (Estandard.FeVto <> ''))"

            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As Exception
        End Try
        Return dtresultado
    End Function



    'devuelve id de estandares vencidos
    Public Function Consultaidvencidos() As String
        Dim dtresultado As SqlDataReader
        Dim FD As String
        Dim sql As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim ids As String = ""
        Try
            ' necesito que este en el formato año mes dia, puesto que los datos viejos fueron guardados como string  y es la unica forma de comparar fechas 
            FD = String.Format("{0:yyyy/MM/dd}", Date.Now())
            sql = "Select * from Estandard "
            sql = sql & " where ((Estandard.Est = 'H') or (Estandard.Est = 'D') or (Estandard.Est = 'V')) "
            sql = sql & " and ((Estandard.FeVto < " & "'" & FD & "'" & "))"

            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    If (ids = "") Then

                    Else
                        ids += "-"
                    End If
                    ids = dtresultado(0)

                End While
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return ids
    End Function



    'devuelve el estado de un estandar dado su id
    Public Function obtenerestado(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select Est from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close() 'se agregó para eliminar un error de maximo de conexiones
        Catch ex As Exception
        End Try
        Return est
    End Function

    'ver funcion de abajo, un requerimiento complico esta funcion
    'devuelve el nombre de un estandar dado su id
    Public Function obtenernombre(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select Nombre from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'devuelve el nombre de un estandar dado su id (con asoc a nombre)
    Public Function obtenernombre1(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select nombre from nombres where nombre_id=" + obtenernombre(ID)
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'devuelve el lote de un estandar dado su id
    Public Function obtenerlote(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select LotePro from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'devuelve el vencimiento un estandar dado su id
    Public Function obtenervencimiento(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select FeVto from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'devuelve la presentacion de un estandar dado su id
    Public Function obtenerpresentacion(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select Prese from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'indica si envia notificacion de stock minimo un estandar dado su id
    Public Function notificamail(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select aviso from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'devuelve el sector de un estandar dado su id
    Public Function obtenersector(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select Sector from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

    'consulta todos los usuarios en un datareader
    Public Function Consultaestandares2() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Estandard where Stock < AlarmaStock and Est = 'V'", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'odbcconn.Close()
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'Actualiza el stock de un estandar ante movimientos
    Public Function actualizarstock(ByVal codest As String) As String
        Dim r As String
        Dim cant_d As String = 0
        Dim stock As Double
        Dim presentacion As String
        Dim xid As Integer = "-1"
        Dim es As Estandares
        es = New Estandares()
        es.Estandares(conexion)
        Dim v As Viales
        v = New Viales()
        v.Viales(conexion)
        Try

            'obtengo la presentacion del estandar

            presentacion = es.obtenerpresentacion(codest)

            'cuento viales disponibles
            cant_d = es.obtenercantidadvialesdisponibles(codest)
            xid = es.Obteneridvialactualuso(codest)

            ' calculo el stock  como viales disponibles x presentacion + remanente del vial en uso
            If (xid <> "-1") Then
                stock = (cant_d * presentacion) + v.obtenerremanente(xid)

            Else
                ' no hay viales en uso
                stock = (cant_d * presentacion)
            End If

            'actualizo el valor en el estandar

            r = es.asignarstock(codest, stock)
            If (r <> "1") Then
                'hubo error
                Return r
            End If

            'OJO REVISAR EL TEMA DE LOS DECIMALEWS EN EL CALCULO DEL STOCK

        Catch ex As Exception
            Return ex.Message
        End Try
        Return r


    End Function

    'verifica si es factible el cambio de estado 
    Public Function validacambioestado(ByVal nuevo As String, ByVal actual As String, ByVal nivel As String) As Boolean
        Dim r As Boolean = False
        If (actual = nuevo) Then
            r = True
        Else
            Select Case nuevo
                Case "D"
                    If (actual = "H") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) Then
                        r = True
                    End If

                Case "V"
                    If (actual = "D") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) Then
                        r = True
                    End If
                Case "I"
                    If (actual = "H") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) Then
                        r = True
                    End If
                Case "F"
                    If (actual = "V") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) Then
                        r = True
                    End If


                Case Else
                    '...
            End Select
        End If


        Return r
    End Function

    'verifica si el estandar tiene viales disponibles o en uso, caso contrario  lo cierra
    Public Function chequeocierre(ByVal codest As String) As Boolean
        Dim r As Boolean = "1"
        Dim es As Estandares
        es = New Estandares()
        es.Estandares(conexion)


        If Not ((es.Obteneridvialactualuso(codest) <> "-1") Or (es.Obteneridvialdisponible(codest) <> "-1")) Then

            'cambiar estado estandar a cerrado
            es.Cerrarestandar(codest)


            r = "0"
        End If
       
        Return r
    End Function

    'verifica si un estandar esta por debajo del stock minimo definido, si no lo esta devuelve 0

    Public Function chequeominimo(ByVal codest As String) As Boolean
        Dim r As Boolean = False
        Dim es As Estandares
        Dim stockactual As Double
        Dim stockminimo As Double
        es = New Estandares()
        es.Estandares(conexion)
        stockactual = es.obtenerstock(codest)
        stockminimo = obtenerstockminimo(codest)
        If (stockactual < stockminimo) Then
            r = True
        End If

        Return r
    End Function

    
    Public Function Consultaestandares3(ByVal sql As String, ByVal meses As Integer) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Dim odbcconn As SqlConnection
        Dim adapter As SqlDataAdapter
        adapter = New SqlDataAdapter

        Dim FD As String
        Dim FH As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            FD = String.Format("{0:yyyy/MM/dd}", Date.Now())
            FH = String.Format("{0:yyyy/MM/dd}", DateAdd("m", meses, FD))

            sql = sql & " and ((Estandard.FeVto >= '" & FD & "') and (Estandard.FeVto <= '" & FH & "')) " 'mysql en lugar de @ usa signo de pregunta

            adapter.SelectCommand = New SqlCommand(sql, odbcconn)


            adapter.Fill(dtable)
            odbcconn.Close()


        Catch ex As Exception
        End Try
        Return dtable
    End Function
    Public Function Consultausu(ByVal usuario As String, ByVal fecha_desde As String, ByVal fecha_hasta As String, ByVal autoriz As String) As DataTable
        'la fechas se cargan invertidas al igual que las consultas, es decir se envian como yyyy-dd-mm  pero se almacenan como yyyy-mm-dd , para la consulta deben ir como yyyy-dd-mm  para que traiga los yyyy-mm-dd
        Dim dtable As DataTable
        Dim sql As String
        dtable = New DataTable
        Dim odbcconn As SqlConnection
        Dim adapter As SqlDataAdapter
        adapter = New SqlDataAdapter


        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (autoriz = False) Then
                sql = "Select n.Nombre, v.Num_vial, c.Num_Ape, c.FechaCo, c.Cant, e.Uni, c.Detalle, c.LoteP, c.Codusu, c.motivo_autorizacion from Consumos c inner join Estandard e on e.CodEst = c.CodEst Inner join Viales v on v.Codvia = c.CodVia Inner join nombres n on n.nombre_id=e.nombre where Codusu ='" + usuario + "' and ((c.FechaCo >= '" & fecha_desde & " 00:00:00.000" & "') and (c.FechaCo <= '" & fecha_hasta & " 00:00:00.000" & "')) order by v.Num_vial, c.Num_Ape asc"
            Else                                                                                                                                                                                                                                                                                                '  sql = sql & " where ((Fecha     >= '" & xfechad     & " 00:00:00.000" & "') and (Fecha     <= '" & xfechah     & " 23:59:00.000" & "'))"

                sql = "Select n.Nombre, v.Num_vial, c.Num_Ape, c.FechaCo, c.Cant, e.Uni, c.Detalle, c.LoteP, c.Codusu, c.motivo_autorizacion from Consumos c inner join Estandard e on e.CodEst = c.CodEst Inner join Viales v on v.Codvia = c.CodVia Inner join nombres n on n.nombre_id=e.nombre where Codusu ='" + usuario + "' and ((c.FechaCo >= '" & fecha_desde & " 00:00:00.000" & "') and (c.FechaCo <= '" & fecha_hasta & " 00:00:00.000" & "')) and motivo_autorizacion <> ''  order by v.Num_vial, c.Num_Ape asc"
            End If

            adapter.SelectCommand = New SqlCommand(sql, odbcconn)


            adapter.Fill(dtable)
            odbcconn.Close()


        Catch ex As Exception
        End Try
        Return dtable
    End Function


    Private Function Generafecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bDia As String
        Dim bMes As String
        Dim bAño As String
        Fechatest = fecha
        bMes = Left(Fechatest, 3)
        bDia = Mid(Fechatest, 5, 2)
        bAño = Mid(Fechatest, 8, 4)
        Fechanueva = bDia & "/" & NumeroMes(bMes) & "/" & bAño
        Return Format(Convert.ToDateTime(Fechanueva), "yyyy/MM/dd")
    End Function


    Private Function NumeroMes(ByVal eNomMes As String) As String
        Dim eMes As String
        Dim M As String
        M = Trim(eNomMes).ToLower
        If M = "jan" Then
            eMes = "01"
        ElseIf M = "feb" Then
            eMes = "02"
        ElseIf M = "mar" Then
            eMes = "03"
        ElseIf M = "apr" Then
            eMes = "04"
        ElseIf M = "may" Then
            eMes = "05"
        ElseIf M = "jun" Then
            eMes = "06"
        ElseIf M = "jul" Then
            eMes = "07"
        ElseIf M = "aug" Then
            eMes = "08"
        ElseIf M = "sep" Then
            eMes = "09"
        ElseIf M = "oct" Then
            eMes = "10"
        ElseIf M = "nov" Then
            eMes = "11"
        ElseIf M = "dec" Then
            eMes = "12"
        End If

        Return Trim(eMes)

    End Function


    Public Function Borrarestandar(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update Estandard set Borrado='S' where CodEst='" + ID + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function AgregarEstandar(ByVal nombre As String, ByVal lote_proveedor As String, ByVal id_licenciente As String, ByVal n_cat As String, ByVal n_cas As String, ByVal tipo As String, ByVal presentacion As String, ByVal unidad As String, ByVal fecha_vencimiento As String, ByVal acondicionamiento As String, ByVal fecha_ingreso As String, ByVal alarma_stock As String, ByVal cantidad_pic As String, ByVal cod_qad As String, ByVal borrado As String, ByVal estado As String, ByVal archivo As String, ByVal stock As String, ByVal fecha_creacion As String, ByVal archivo2 As String, ByVal observaciones As String, ByVal sector As String, ByVal sedronar As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into Estandard (nombre,LotePro,CodLic,NumCatalogo,NumCas,Tipo,Prese,Uni,FeVto,Acond,Feing,AlarmaStock,CantPic,CodQAD,Sector,Borrado,Est,Arch,Stock,FechaCre,Arch1,Obs,sedronar,aviso) values ('" + nombre + "','" + lote_proveedor + "','" + id_licenciente + "','" + n_cat + "','" + n_cas + "','" + tipo + "','" + presentacion + "','" + unidad + "','" + fecha_vencimiento + "','" + acondicionamiento + "','" + fecha_ingreso + "','" + alarma_stock + "','" + cantidad_pic + "','" + cod_qad + "','" + sector + "','" + borrado + "','" + estado + "','" + archivo + "','" + stock + "','" + fecha_creacion + "','" + archivo2 + "','" + observaciones + "','" + sedronar + "','" + "1" + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            'odbccom.ExecuteNonQuery()

            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarestandar(ByVal ID As String, ByVal nombre As String, ByVal lote_proveedor As String, ByVal id_licenciente As String, ByVal n_cat As String, ByVal n_cas As String, ByVal tipo As String, ByVal presentacion As String, ByVal unidad As String, ByVal fecha_vencimiento As String, ByVal acondicionamiento As String, ByVal fecha_ingreso As String, ByVal alarma_stock As String, ByVal cantidad_pic As String, ByVal cod_qad As String, ByVal borrado As String, ByVal estado As String, ByVal archivo As String, ByVal stock As String, ByVal fecha_creacion As String, ByVal archivo2 As String, ByVal observaciones As String, ByVal sector As String, ByVal sedronar As String, ByVal aviso As String) As Boolean


        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Estandard set nombre='" + nombre + "',LotePro='" + lote_proveedor + "',CodLic='" + id_licenciente + "',NumCatalogo='" + n_cat + "',NumCas='" + n_cas + "',Tipo='" + tipo + "',Prese='" + presentacion + "',Uni='" + unidad + "',FeVto='" + fecha_vencimiento + "',Acond='" + acondicionamiento + "',Feing='" + fecha_ingreso + "',AlarmaStock='" + alarma_stock + "',CantPic='" + cantidad_pic + "', CodQAD='" + cod_qad + "', Sector='" + sector + "', Borrado='" + borrado + "',Est='" + estado + "',Arch='" + archivo + "',Stock='" + stock + "', FechaCre='" + fecha_creacion + "',Arch1='" + archivo2 + "',Obs='" + observaciones + "',sedronar='" + sedronar + "',aviso='" + aviso + "' where CodEst='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    'actualiza el estado de un estandar

    Public Function Actualizarestandar(ByVal estado As String, ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Estandard set Est='" + estado + "' where CodEst='" + ID + "'", odbcconn)

            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function Cerrarestandar(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Estandard set Est='F' where CodEst='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'asigna stock pasado por parametro
    Public Function asignarstock(ByVal ID As String, ByVal cantidad As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "update Estandard set Stock=" + cantidad.Replace(",", ".") + " where CodEst='" + ID + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return sql
        End Try
        Return "1"
    End Function
    'asigna cantidad pedida en base a la cantidad de viales asociados a un estandar
    Public Function actualizacantidad(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim v As Viales
        Dim cont As Integer = 0
        Dim cant As String
        Try
            v = New Viales()
            v.Viales(conexion)
            dtresultado = v.Consultarviales2(ID)
            'cuento la cantidad de viales del estandar sin importar el estado
            While (dtresultado.Read())
                cont += 1
            End While
            cant = cont 'asigno por un problema de cast en la bd
            dtresultado.Close()
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Estandard set CantPic='" + cant + "' where CodEst='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    'chequea consistencia del stock de un estandar en particular en base al campo stock de la tabla estandares y el calculo segun stock de sus viales
    Public Function chequeaconsistenciastock(ByVal ID As String) As Boolean

        Dim es As Estandares
        Dim v As Viales
        Dim cont As Double = 0
        Dim xcont As Double = 0
        Dim disp As Integer
        Dim actual As Integer
        Dim stockestandar As Double
        Try

            es = New Estandares()
            es.Estandares(conexion)
            v = New Viales()
            v.Viales(conexion)

            'obtengo todos los viales disponibles y sumo su stock

            disp = es.obtenercantidadvialesdisponibles(ID)
            cont += es.obtenerpresentacion(ID) * disp


            'obtengo vial en uso y su remanente
            actual = es.Obteneridvialactualuso(ID)
            cont += v.obtenerremanente(actual)



            'obtengo el stock del estandar en la tabla estandar

            stockestandar = es.obtenerstock(ID)


        

            If (stockestandar = cont) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    'chequea consistencia del stock de un estandar en particular en base al campo stock de la tabla estandares y el calculo segun stock de sus viales
    Public Function chequeaconsistenciacant(ByVal ID As String) As Boolean

        Dim v As Viales
        Dim es As Estandares
        Dim cont As Integer = 0
        Dim cant As String
        Try

            es = New Estandares()
            es.Estandares(conexion)
            v = New Viales()
            v.Viales(conexion)

            'cuento la cantidad de viales del estandar sin importar el estado
            cont = v.cantidadviales(ID)


            'traigo cantidad pedida cargada en tabla estandares
            cant = es.obtenercantidadpedida(ID)
          

            If (cant = cont) Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function


    Public Function Actualizarestandarmigracion(ByVal ID As String, ByVal nombre As String, ByVal lote_proveedor As String, ByVal tipo_proveedor As String, ByVal id_licenciente As String, ByVal tipo As String, ByVal presentacion As String, ByVal unidad As String, ByVal tsds As String, ByVal tsdc As String, ByVal humedad As String, ByVal total_vial As String, ByVal fecha_vencimiento As String, ByVal acondicionamiento As String, ByVal fecha_ingreso As String, ByVal alarma_stock As String, ByVal alarma_entrega As String, ByVal fecha_pic As String, ByVal nro_pic As String, ByVal cantidad_pic As String, ByVal fecha_estimada As String, ByVal farmacopea As String, ByVal cod_qad As String, ByVal consumo_mp As String, ByVal lotes_mp As String, ByVal consumo_pt As String, ByVal consumo_pt1 As String, ByVal total_estandar As String, ByVal columna As String, ByVal costo_columna As String, ByVal costo_vial As String, ByVal borrado As String, ByVal estado As String, ByVal archivo As String, ByVal stock As String, ByVal fecha_creacion As String, ByVal archivo2 As String, ByVal observaciones As String, ByVal remito As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Estandard set nombre='" + nombre + "',LotePro='" + lote_proveedor + "',TipoPro='" + tipo_proveedor + "',CodLic='" + id_licenciente + "',Tipo='" + tipo + "',Prese='" + presentacion + "',Uni='" + unidad + "',TitSBS='" + tsds + "',TitSDTC='" + tsdc + "',Humedad='" + humedad + "',TotVia='" + total_vial + "',FeVto='" + fecha_vencimiento + "',Acond='" + acondicionamiento + "',Feing='" + fecha_ingreso + "',AlarmaStock='" + alarma_stock + "',AlarmaEnt='" + alarma_entrega + "',FePic='" + fecha_pic + "',NroPic='" + nro_pic + "',CantPic='" + cantidad_pic + "',FeEst='" + fecha_estimada + "',Farmacopea='" + farmacopea + "',CodQAD='" + cod_qad + "',ConsumoMP='" + consumo_mp + "',LotesMP='" + lotes_mp + "',ConsumoPT='" + consumo_pt + "',ConsumoPT1='" + consumo_pt1 + "',TotEst='" + total_estandar + "',Colum='" + columna + "',CostoColum='" + costo_columna + "',CostoVial='" + costo_vial + "',Borrado='" + borrado + "',Est='" + estado + "',Arch='" + archivo + "',Stock='" + stock + "',FechaCre='" + fecha_creacion + "',Arch1='" + archivo2 + "',Obs='" + observaciones + "',Remi='" + remito + "' where CodEst='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception

            Return False
        End Try
        Return True
    End Function
    'obtiene el vial en uso del estandar
    Public Function Obtenervialactualuso(ByVal CodEst As String) As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select * from Viales where CodEst ='" + CodEst + "' and Estado = 'U' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'odbcconn.Close()
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'obtiene el vial en uso del estandar
    Public Function Obtenercantidadpedida(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim cantidad As Integer
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select CantPic from Estandard where CodEst ='" + CodEst + "' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                cantidad = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return cantidad
    End Function
    'obtiene el id del vial en uso del estandar
    Public Function Obteneridvialactualuso(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim id As Integer = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select * from Viales where CodEst ='" + CodEst + "' and Estado = 'U' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                id = dtresultado(0)
            End If

            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return id
    End Function
    'devuelve la cantidad de viales en uso (1 o 0) lo uso para ver q estado coloco a los viales cuando elimino consumos
    Public Function obtenercantidadvialesuso(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim cantidad As Integer = 0
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select * from Viales where CodEst ='" + CodEst + "' and Estado = 'U' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    cantidad = cantidad + "1"
                End While

            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return cantidad
    End Function


    'devuelve la cantidad de viales (sin importar el estado) del estandar
    Public Function obtenercantidadviales(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim cantidad As Integer = 0
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select * from Viales where CodEst ='" + CodEst + "' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    cantidad = cantidad + "1"
                End While

            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return cantidad
    End Function

    'cierra los viales de un estandar
    Public Function cierraviales(ByVal CodEst As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim y As Viales
        Try
            y = New Viales() ' nuevo objeto instancia
            y.Viales(conexion)
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select * from Viales where CodEst ='" + CodEst + "' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    y.Cerrarvial(dtresultado(0))
                End While

            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function



    'devuelve el id del vial en uso del estandar
    Public Function obteneridvialenuso(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim id As Integer = 0
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select CodVIa from Viales where CodEst ='" + CodEst + "' and Estado = 'U' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                id = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return id
    End Function


    'devuelve el numero de vial actual (en uso o disponible) del estandar
    Public Function calculavial_actual(ByVal CodEst As String) As Integer
        Dim es As Estandares
        Dim v As Viales

        Dim numero As Integer
        Dim id As Integer = "-1"
        Try
            es = New Estandares()
            es.Estandares(conexion)
            v = New Viales()
            v.Viales(conexion)

            id = es.Obteneridvialactualuso(CodEst)

            If (id = "-1") Then

                id = es.Obteneridvialdisponible(CodEst)

            End If

            numero = v.obtener_numerovial(id)
        Catch ex As Exception
        End Try
        Return numero
    End Function


    'devuelve el remanente del vial actual en uso por el estanar
    Public Function calcularemanente(ByVal CodEst As String) As Double
        Dim es As Estandares
        Dim v As Viales
        Dim id As Integer
        Dim numero As Double
        Try

            v = New Viales()
            v.Viales(conexion)

            es = New Estandares()
            es.Estandares(conexion)
            id = es.Obteneridvialactualuso(CodEst)

            If (id = "-1") Then

                id = es.Obteneridvialdisponible(CodEst)

            End If
            numero = v.obtenerremanente(id)

        Catch ex As Exception
        End Try
        Return numero
    End Function


    'obtiene el tipo de un estandar en particular
    Public Function Obtenertipoestandar(ByVal CodEst As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim tipo As String = "0"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("Select Tipo from Estandard where CodEst ='" + CodEst + "' ", odbcconn)
            dtresultado = odbccom.ExecuteReader()

            If (dtresultado.HasRows) Then
                dtresultado.Read()
                tipo = dtresultado(0).ToString
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try

        Return tipo
    End Function
    'obtiene un arreglo ordenado por numero de vial,  con los viales disponibles del estandar ( no el que esta en uso si es que lo hay)
    Public Function Obtenervialdisponible(ByVal CodEst As String) As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + CodEst + "' and Estado ='D' order by num_vial"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As Exception

        End Try
        Return dtresultado

    End Function
    'devuelve el id del primer vial disponible del estandar ordenado por numero de vial
    Public Function Obteneridvialdisponibleord(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim sql As String
        Dim numero As Integer
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + CodEst + "' and Estado ='D' order by num_vial"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                numero = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()

        Catch ex As Exception

        End Try
        Return numero

    End Function
    'cuenta la cantidad de viales disponibles de un estandar dado un id
    Public Function obtenercantidadvialesdisponibles(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim sql As String
        Dim cantidad As Integer = "0"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + CodEst + "' and Estado ='D' order by num_vial"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    cantidad += 1
                End While
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception

        End Try
        Return cantidad

    End Function
    'obtiene un arreglo ordenado por numero de vial,  con los viales disponibles del estandar ( no el que esta en uso si es que lo hay)
    Public Function Obteneridvialdisponible(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim sql As String
        Dim id As Integer = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + CodEst + "' and Estado ='D' order by num_vial"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                id = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception

        End Try
        Return id

    End Function
    'devuelve la cantidad de viales disponibles (Cerrados) de un estandar

    Public Function calculaviales_cerrados(ByVal CodEst As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim dtresultado As SqlDataReader
        Dim sql As String
        Dim cant As Integer = 0
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + CodEst + "' and Estado ='D' order by num_vial"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            While (dtresultado.Read())
                cant += 1
            End While
            odbcconn.Close()
            dtresultado.Close()

        Catch ex As Exception

        End Try
        Return cant

    End Function

    Public Function invertirfecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function



#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub
End Class
