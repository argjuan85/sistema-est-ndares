﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Viales
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Viales(ByVal strconex As String)
        strconexion = strconex
    End Sub


    Public Function Consultarviales() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand("select * from Viales order by CodEst ", OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function


    Public Function Consultarviales1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Viales where CodVIa=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)

            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
 
    'dado un id de estandar y un numero de vial , verifica si el mismo ya existe 
    Public Function Consulta_vial(ByVal ID As String, ByVal num As String) As Boolean
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Viales where CodEst=" + ID + "and num_vial=" + num
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                'encontro un vial con ese numero
              
                Return False
            Else
                ' no hay
                Return True
            End If
            dtresultado.Close()
            odbcconn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'obtener los viales de un estandar en particular
    Public Function Consultarviales2(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select v.CodEst, v.num_vial, v.Estado, v.num_ape, v.remanente, d.nombre from ((Viales V LEFT OUTER JOIN Envios e ON V.CodEnv=e.CodEnv) left outer join Destinatarios d ON d.destinatario_id=e.CodDest) where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)

            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function


  
    'obtener la cantidad de viales de un estandar
    Public Function cantidadviales(ByVal ID As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim cantidad As Integer = 0
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select v.CodEst, v.num_vial, v.Estado, v.num_ape, v.remanente, d.nombre from ((Viales V LEFT OUTER JOIN Envios e ON V.CodEnv=e.CodEnv) left outer join Destinatarios d ON d.destinatario_id=e.CodDest) where CodEst=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    cantidad += 1
                End While
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return cantidad
    End Function


    'obtener los viales asociados a un envio en particular
    Public Function vialesenviados(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Viales where CodEnv=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)

            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'obtener los viales asociados a un envio en particular
    Public Function eliminaenvio(ByVal ID As String) As Boolean
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Viales where CodEnv=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)

            'actualizo los viales (estado y quito codigo de envio)
            If (dtresultado.HasRows) Then
                While (dtresultado.Read())
                    Actualizarvial(dtresultado(0).ToString(), dtresultado(1).ToString(), "D", dtresultado(3).ToString(), dtresultado(5).ToString(), Nothing)
                End While
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

  


    Public Function obtener_numerovial(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim num As String = "-1"
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select num_vial from Viales where CodVIa=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                num = dtresultado(0).ToString()
             
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return num
    End Function
    'devuelve el ultimo vial (numero mas alto) de un estandar

    Public Function obtenerultimovial(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select num_vial, Estado, CodVIa from Viales where CodEst=" + ID + "order by num_vial desc ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    'devuelve la cantidad de viales disponibles al final del estandar

    Public Function obtenercantultimovial(ByVal ID As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim band1 As Integer = 0
        Dim cont2 As Integer = 0

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select num_vial, Estado, CodVIa from Viales where CodEst=" + ID + "order by num_vial desc ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                'chequeo que tengo tantos viales disponibles como los que solicitan eliminar al FINAL del estandar, si no, no sigue
                While (dtresultado.Read()) And (band1 = 0)
                    If (dtresultado(1).ToString() = "D") Then
                        cont2 += 1
                        'si encuentra un vial que no este en estado d corta
                    Else
                        band1 = 1
                    End If

                End While
            End If
            dtresultado.Close()
            odbcconn.Close()

        Catch ex As Exception
        End Try
        Return cont2
    End Function




    'devuelve el id del ultimo vial (numero mas alto) de un estandar

    Public Function obteneridultimovial(ByVal ID As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim ide As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select CodVia, num_vial, Estado from Viales where CodEst=" + ID + "order by num_vial desc ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                ide = dtresultado(0)
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return ide
    End Function


    Public Function Buscarvial(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Viales where CodEst like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function



    

    Public Function BorrarVial(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Viales where CodVia=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function generaviales(ByVal cant As String, ByVal codest As String, ByVal prese As String, ByVal indice As Integer) As String

        Dim ids As String = ""
      
        Try

            ids += Agregarvial(codest, "D", "0", prese, cant, indice)

        Catch ex As Exception
            Return False
        End Try
        Return ids
    End Function


    Public Function Agregarvial(ByVal codest As String, ByVal estado As String, ByVal num_ape As String, ByVal remanente As String, ByVal cantidad_a_generar As Integer, ByVal indice As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim ids As String = ""
        Dim sql As String
        Dim xnum_vial As Integer = indice + 1
        Dim num_vial As String
        Dim cont As Integer = "1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (Consulta_vial(codest, indice + 1)) Then
                While (cont <= cantidad_a_generar)
                    num_vial = xnum_vial
                    sql = "insert into Viales (CodEst,Estado,num_ape,num_vial,remanente) values ('" + codest + "','" + estado + "','" + num_ape + "','" + num_vial + "','" + remanente + "');  SELECT SCOPE_IDENTITY(); "
                    odbccom = New SqlCommand(sql, odbcconn)
                    If (ids = "") Then

                    Else
                        ids += "-"
                    End If
                    ids += odbccom.ExecuteScalar().ToString()
                    xnum_vial += "1"
                    cont += "1"
                End While
            End If

            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return ids
    End Function

    Public Function Actualizarvial(ByVal ID As String, ByVal codest As String, ByVal estado As String, ByVal num_ape As String, ByVal remanente As String, ByVal envio As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (envio <> Nothing) Then
                odbccom = New SqlCommand("update Viales set CodEst='" + codest + "',Estado='" + estado + "',num_ape='" + num_ape + "',remanente=" + remanente.Replace(",", ".") + ",CodEnv='" + envio + "' where CodVia='" + ID + "' ", odbcconn)
            Else
                odbccom = New SqlCommand("update Viales set CodEst='" + codest + "',Estado='" + estado + "',num_ape='" + num_ape + "',remanente=" + remanente.Replace(",", ".") + " where CodVia='" + ID + "' ", odbcconn)
            End If
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'cambia el estado a un vial en particular
    Public Function cambiarestadovial(ByVal ID As String, ByVal estado As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Viales set Estado='" + estado + "' where CodVia='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'obtiene el id dado numero de vial y id de estandar
    Public Function obteneridvial(ByVal num_vial As String, ByVal codest As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim id As Integer = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select * from Viales where CodEst ='" + codest + "' and num_vial = '" + num_vial + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                id = dtresultado(0).ToString()
              
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try

        Return id
    End Function

    'devuelve el remanente de un vial dado su id
    Public Function obtenerremanente(ByVal id_vial As String) As Double
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim remanente As Double = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select remanente from Viales where CodVIa ='" + id_vial + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                remanente = dtresultado(0).ToString()

            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try

        Return remanente
    End Function


    'devuelve el numero de aperturas de un vial dado su id
    Public Function obtenerapertura(ByVal id_vial As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim apertura As Integer = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select num_ape from Viales where CodVIa ='" + id_vial + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                apertura = dtresultado(0).ToString()
                
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try

        Return apertura
    End Function

    'devuelve el estado de un vial dado su id
    Public Function obtenerestado(ByVal id_vial As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim estado As String = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "Select Estado from Viales where CodVIa ='" + id_vial + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                estado = dtresultado(0).ToString()
                
            End If
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try

        Return estado
    End Function



    'cierra un vial en particular
    Public Function Cerrarvial(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Viales set Estado='C' where CodVia='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function


#End Region
    Protected Overrides Sub Finalize()

        MyBase.Finalize()
    End Sub
End Class
