﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports globales
Imports System.Net.Mail
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(0, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else






            check_vencidos()

        End If

    End Sub

   

    Public Sub check_vencidos()
        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        Dim FD As String
        FD = String.Format("{0:yyyy/MM/dd}", Date.Now())
        Try
            If (p.consulta_valor_nombre("check vencidos") = FD) Then
                'nada
            Else
                Controlar_Vencidos()
                'actualizar parametro
                If Not (p.actualizar_parametro_nombre("check vencidos", FD)) Then
                    'mensaje error

                End If
            End If

        Catch ex As Exception
        End Try

    End Sub

    Public Sub Controlar_Vencidos()


        Dim dtresultado As SqlDataReader
        Dim dtresultado1 As SqlDataReader
        Dim es As Estandares
        Dim vi As Viales
        Dim b As String
        Dim auxe As String
        Dim a As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)
        Try
            es = New Estandares()
            es.Estandares(conexion)
            dtresultado = es.Consultavencidos()

            While (dtresultado.Read())
                'actualizo estado
                auxe = dtresultado(0)
                'optimizar
                dtresultado1 = es.Consultarestandares1(dtresultado(0))
                'para que no cambie de estado a los estandares 
                If (dtresultado(9).ToString() <> "") Then
                    a = es.Actualizarestandar("F", auxe)
                   
                    'envio un correo dando aviso
                    EnvioMail(es.obtenernombre1(auxe), es.invertirfecha(es.obtenervencimiento(auxe)), es.obtenerstock(auxe), es.obtenerunidad(auxe))
                    
                End If


                'log cambio de estado

                If (dtresultado1.Read()) Then

                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                    l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado (automatico) de " + dtresultado1(17).ToString + " a F", "Estandares", dtresultado1(0), es.obtenernombre1(dtresultado1(0).ToString) + " - " + dtresultado1(2).ToString)

                End If
                dtresultado1.Close()
                'actualizo viales en uso o disponibles del estandar vencido
                vi = New Viales()
                vi.Viales(conexion)
                dtresultado1 = vi.Consultarviales2(auxe)
                While (dtresultado1.Read())
                    b = dtresultado1(2).ToString()
                    If (dtresultado1(2).ToString() = "U" Or dtresultado1(2).ToString() = "D") Then
                        vi.Cerrarvial(vi.obteneridvial(dtresultado1(1).ToString(), auxe))

                    End If

                    'registro log

                End While

                dtresultado1.Close()
            End While
            dtresultado.Close()


        Catch ex As Exception
        End Try
    End Sub

    Public Sub EnvioMail(ByVal estandar As String, ByVal fecha As String, ByVal stock As String, ByVal uni As String)


        Dim message As New MailMessage

        Dim smtp As New SmtpClient

        message.From = New MailAddress(mailsistema)

        If (Session("sector") = sectoradmin) Then
            message.To.Add(mailadmincc)
        Else
            message.To.Add(mailadminve)
        End If

        message.Body = "El estándar: " & estandar & " se encuentra vencido." & vbLf & vbLf

        message.Body = message.Body & "Fecha de Vencimiento: " & fecha & "." & vbLf & vbLf

        message.Body = message.Body & "Stock: " & stock & " " & uni & "." & vbLf & vbLf

        message.Body = message.Body & "Este es un correo generado automáticamente por el sistema de estándares, no responda este correo."

        message.Subject = "Estándar vencido."

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = mailport

        smtp.Host = mailserver

        smtp.Credentials = New Net.NetworkCredential(mailuser, mailpass)

        smtp.Send(message)

    End Sub



End Class
