﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(65536, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                Button2.Attributes.Add("onclick", " return confirma_elimina();")
                Button1.Attributes.Add("onclick", " return confirma_modifica();")

                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarlicenciante(codigo) 'carga la interfaz
                End If

            Catch
                Label2.Text = "Error al cargar datos del licenciante"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try

        End If
    End Sub


 

    Sub buscarlicenciante(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Licenciantes
        Try
            u = New Licenciantes()
            u.Licenciantes(conexion)
            dtresultado = u.Consultarlicenciantes1(codigo)
            nombre1.Value = ""

            While (dtresultado.Read())

                nombre1.Value = dtresultado(1).ToString()

            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.text = "Error al buscar licenciante"

        End Try
    End Sub
    Sub actualizarlicenciante(ByVal codigo As String)
        Dim u As Licenciantes
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)
        Try
            u = New Licenciantes()
            u.Licenciantes(conexion)
            r = u.ActualizarLicenciante(codigo, nombre1.Value)
            'log
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Licenciante", "Licenciantes", codigo, nombre1.Value)
            Label1.Text = "Se modificó el licenciante correctamente"

        Catch ex As Exception
            Label2.text = "Error al actualizar licenciante"

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click


        Label1.Text = ""
        Label2.Text = ""


        If (Trim(nombre1.Value).Length > "2") Then
            If (verificalicenciante(Trim(nombre1.Value))) Then
                actualizarlicenciante(codigo)


            Else
                Label2.Text = "El nombre del destinatario ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar al menos 3 caracteres"


        End If


    End Sub
    Public Shared Function verificalicenciante(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Licenciantes
        u = New Licenciantes()
        u.Licenciantes(conexion)
        dtresultado = u.Consultalicenciantes2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (destinatario.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While
        dtresultado.Close()
        Return (band)

    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim u As Licenciantes
        Dim r As Boolean
        Dim dtresultado As SqlDataReader
        Dim dtresultado2 As SqlDataReader
        Dim idreg As Integer
        Dim nomreg As String
        u = New Licenciantes()
        u.Licenciantes(conexion)
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)




        dtresultado2 = u.Consultarlicenciantes1(codigo)
        If (dtresultado2.Read()) Then
            idreg = dtresultado2(0).ToString()
            nomreg = dtresultado2(1).ToString()
        End If

        dtresultado2.Close()

        ' reviso que no este asociado
        dtresultado = u.Consultarlicenciantesasoc(codigo)

        If (Not dtresultado.Read()) Then

            ' elimino  el lic
            r = u.BorrarLicenciante(codigo)

            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Licenciante", "Licenciantes", idreg, nomreg)

            Response.Redirect("consultar_licenciantes.aspx")
        Else
            Label1.Text = "El Licenciante esta asociado a estándares, no se puede eliminar"


        End If
        dtresultado.Close()

    End Sub
End Class
