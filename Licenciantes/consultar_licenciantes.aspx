﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="consultar_licenciantes.aspx.vb" Inherits="Licenciantes_consultar_licenciantes" title="Sistema Estándares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    
    <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />
<link rel="stylesheet" href="../js/datatable/media/css/demo_page.css"type="text/css" />
<link rel="stylesheet" href="../js/datatable/ColVis/media/css/ColVis.css"type="text/css" />
    <script type="text/javascript" src="../js/datatable/media/js/jquery.js"></script>
    <script type="text/javascript" src="../js/datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
       <style type="text/css">
    	    input {	height: 15px;
}

        #ctl00_ContentPlaceHolder1_Div1 
{


margin:auto;

}
       </style>
<script language="javascript" type="text/javascript">

var asInitVals = new Array();
     $(document).ready(function () {
     
     var  oTable = $('#tbl').dataTable( {
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });
    
    

    
</script>
 <div id="titulo_seccion">
<label runat="server" id="labeltit">Administración de Licenciantes</label>
 </div>

<div align= "center" >
    <asp:Button ID="Button2" 
            runat="server" Text=" + Agregar Licenciante " />
            <asp:Button ID="Button4" 
            runat="server" Text=" Buscar " />
 </div>           
               <br>   
    
    
        <div  id="Div1"  style="width:50%" runat="server">
        
        
<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" align="center" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead>
                    <tr>
                        <th></th>
                      
                        <th>Nombre</th>
                                              
                    </tr>
              
                  </thead>
                <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  <td align = "center"><a><asp:Button ID="Button1" CommandName="Click1" Text="Modificar" runat="server" CommandArgument='<%# Eval("licenciante_id") %>' /></a></td>
                  <td align = "center"><%#Eval("nombre")%></td>
         
                           

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        
    </div>
 

    </form>
       <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
      <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div>
</asp:Content>

