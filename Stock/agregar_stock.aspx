﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_stock.aspx.vb" Inherits="Licenciantes_agregar_licenciante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
   
       <!-- librerias calendario y css -->
       <link rel="stylesheet" href="../Estandares/css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="../Estandares/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="../Estandares/js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
         
      <!--  calendarios  --> 
    <script type="text/javascript" >

	
	$(document).ready(function(){
	   $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	$("#<%= fecha.ClientID %>").datepicker({
	 dateFormat: "dd/mm/yy",  changeMonth: true,changeYear: true , maxDate: 'today', yearRange: '-10:+10', maxDate: '0',stepMonths: 0
	});
		
		 $('#ctl00_ContentPlaceHolder1_Button1').click(function(){
   if($("#<%= fecha.ClientID %>").val().length < 1) {
    alert("El campo 'fecha de stock' es obligatorio");
    return false;
}
   

   });
		
});





   </script>


   <div id="contenedor1">
    <div id="contenedordest">
     <div id="titulo_seccion">
     <label runat="server" id="labeltit">Ingrese Fecha de Stock</label>
 </div>
    <form id="Form1" runat ="server"> 
    <ul>
    <li class="xli">
    <div class="xtest">
  
 
    <span class="dostercios">
     <label for="tipo">Fecha de Stock</label>
        <input id="fecha" runat="server" name="nombre1" value="" style="width:80%"  maxlength="50" readonly="readonly"/>
    </span>
    <span class="dostercios" >  
        <label for="tipo">Observaciones</label>
     
    <input id="observaciones" runat="server" name="observaciones" value="" style="width:80%" maxlength="50"/>
    </span>
 
    
 
    
  </div>
  </li>
  </ul>
  
    <ul>
   
    <li class="panel_boton">
     <div>
      <span class="boton">
           <input type="button" value="Volver" id="volver" onclick="location.href='consultar_stock.aspx'">
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
         
         </span></div>
    </li>
        
     </ul>
    
     
     </form>
        
  </div>
  </div>    
  <br >
  
<div  id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
     <div  id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div>

 

</asp:Content>