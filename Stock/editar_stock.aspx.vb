﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(65536, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                Button2.Attributes.Add("onclick", " return confirma_elimina();")
                Button1.Attributes.Add("onclick", " return confirma_modifica();")

                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarstock(codigo) 'carga la interfaz
                End If

            Catch
                Label2.Text = "Error al cargar datos de fecha de stock"
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try

        End If
    End Sub

    Sub buscarstock(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Stock
        Try
            u = New Stock()
            u.Stock(conexion)
            dtresultado = u.Consultarstock1(codigo)
            nombre1.Value = ""

            While (dtresultado.Read())

                nombre1.Value = String.Format("{0:dd/MM/yyyy}", dtresultado(2))
                obs.Value = dtresultado(3).ToString()
            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al buscar fecha de stock"

        End Try
    End Sub
    Sub actualizarstock(ByVal codigo As String)
        Dim u As Stock
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)
        Try
            u = New Stock()
            u.Stock(conexion)
            'al migrar a sqlserver 2008 tengo q transformar la fecha, si por algun motivo hay q correr en 2005 simplemente pasar fecha.value a la funcion
            xfecha = cambiaformatofecha2(nombre1.Value)
            r = u.Actualizarfechastock(codigo, xfecha, Trim(obs.Value))
            'log
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica fecha de stock", "fechastock", codigo, nombre1.Value)
            Label1.Text = "Se modificó la fecha de stock correctamente"

        Catch ex As Exception
            Label2.Text = "Error al actualizar la fecha de stock"

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim u As Stock
        u = New Stock()
        u.Stock(conexion)

        Label1.Text = ""
        Label2.Text = ""

        If (u.obtenerfecha(codigo) <> nombre1.Value) Then

            If (verificastock(Trim(nombre1.Value))) Then
                actualizarstock(codigo)
            Else
                Label2.Text = "Ya se ha registrado esta fecha de stock, por favor ingrese otra fecha"
                Return
            End If

        Else
            actualizarstock(codigo)
        End If


    End Sub
    Public Shared Function verificastock(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Stock
        u = New Stock()
        u.Stock(conexion)
        dtresultado = u.Consultastock()
        Dim band As String = 1
        While (dtresultado.Read())

            If (destinatario.ToUpper() = dtresultado(2)) Then
                band = 0
                Return (band)
            End If

        End While
        dtresultado.Close()
        Return (band)

    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim u As Stock
        Dim r As Boolean
        Dim idreg As Integer
        Dim nomreg As String
        Dim xfecha As String
        u = New Stock()
        u.Stock(conexion)
        Dim l As logs

        'log
        l = New logs()
        l.logs(conexion)




    
        idreg = codigo
        nomreg = u.obtenerfecha(codigo)




       

        ' elimino  el lic
        r = u.Borrarstock(codigo)
        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Fecha de Stock", "fechastock", idreg, nomreg)

        Response.Redirect("consultar_stock.aspx")
       
    End Sub
End Class
