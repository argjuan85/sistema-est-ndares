﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""


        If (verificastock(Trim(fecha.Value))) Then

            nuevostock()

            fecha.Value = ""
            observaciones.Value = ""

        Else
            Label2.Text = "La fecha ya se encuentra registrada, por favor ingrese otra fecha"
            Return
        End If
      



    End Sub

    Public Shared Function verificastock(ByVal destinatario As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Stock
        u = New Stock()
        u.Stock(conexion)
        dtresultado = u.Consultastock()
        Dim band As String = 1
        If (dtresultado.HasRows()) Then
            While (dtresultado.Read())

                If (destinatario = dtresultado(2)) Then
                    band = 0
                    Return (band)
                End If

            End While
            dtresultado.Close()
        End If



        Return (band)


    End Function

 

    Sub nuevostock()
        Dim u As Stock
        Dim l As logs
        Dim r As Integer
        Dim xfecha As String

        Try
            u = New Stock()
            u.Stock(conexion)
            'log
            l = New logs()
            l.logs(conexion)
            'al migrar a sqlserver 2008 tengo q transformar la fecha, si por algun motivo hay q correr en 2005 simplemente pasar fecha.value a la funcion
            xfecha = cambiaformatofecha2(fecha.Value)
            r = u.Agregarstock(xfecha, Trim(observaciones.Value))
            If (r <> "-1") Then
                'log
                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Carga de fecha de stock de inventario EP", "Stock", r, fecha.Value)
                Label1.Text = "Se registró la fecha correctamente"
            Else
                Label2.Text = "Error al grabar la fecha"
            End If

        Catch ex As Exception
            Label2.Text = "Error al cargar fecha"
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(16384, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else

            Dim message As String = "Confirma la carga de la fecha de stock?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
        End If

    End Sub
End Class
