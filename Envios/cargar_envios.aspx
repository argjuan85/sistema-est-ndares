﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="cargar_envios.aspx.vb" Inherits="Envios_cargar_envios" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   
    <link rel="stylesheet" href="../css/formularios.css" type="text/css" />
       <!-- librerias calendario y css -->
       <link rel="stylesheet" href="../Estandares/css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="../Estandares/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="../Estandares/js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
          
 <link href="../js/vanadium/style.css" rel="Stylesheet" type="text/css" />
      <!--  calendarios  --> 
    <script type="text/javascript" >
	$(document).ready(function(){
		
	   $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
		$("#<%= fecha_envio.ClientID %>").datepicker({
	 dateFormat: "dd/mm/yy", changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0});
	
			 $('#ctl00_ContentPlaceHolder1_Button1').click(function(){
   if($("#<%= fecha_envio.ClientID %>").val().length < 1) {
    alert("El campo 'fecha envío' es obligatorio");
    return false;
}
   
   });
		
		
});


   </script>
   
    <script>
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="consulta_envios.aspx?"+ arr[0] } 

</script>  
   

    <div id="contenedor1">
    <div id="contenedorenvios"> <div id="titulo_seccion">
Ingrese los datos del envío
 </div>
    <form  runat ="server"> 
    
    <ul>
    <li class="paneles">
    <div>
  
 
    <span class="tercio">
     <label for="tipo">Cantidad de viales a enviar</label>
            <input id="cantidad" runat="server" name="cantidad" value="" class=":required :integer :only_on_blur"/>
     
    </span>
 
    <span class="dostercios">
    <label for="fecha_envio">Fecha Envio</label>
     <input id="fecha_envio" runat="server" name="fecha_envio" value="" readonly="readonly"/>
      </span>
 </div>
 </li>
   </ul>
   
                   
    <ul>  
    <li class="paneles">
   
    <div>
    
      <span class="tercio">
         <label for="proveedor">Detinatario</label>
      <asp:DropDownList ID="destinatario" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="nombre" DataValueField="destinatario_id" class=":required">
        </asp:DropDownList>
        
     
    </span>
 
    <span class="tercio">
      <label for="n_cas">Detalle</label>
      <input id="detalle" runat="server" name="detalle" value="" class=":required :only_on_blur" maxlength="50"/>
      
    </span>
  </div>
  </li>
  </ul>
    
  
 
    
    <ul>
    <li class="panel_boton">
        <div>
      <span class="boton">
           <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" >
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
      </span></div>
    </li>
    </ul>
              
                   
   


     <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EstandaresConnectionString %>" 
            SelectCommand="SELECT [destinatario_id], [nombre] FROM [Destinatarios]">
        </asp:SqlDataSource>

    </form>
    
  </div>
  </div>
  <br>
     <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>
      <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div>
      <div id="notice">
     <asp:Label ID="Label3" runat="server"></asp:Label>
     </div>
     </asp:Content>
