﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String
    Public Codigox As String

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim es As Estandares
            Dim u As Envios
            Dim v As Viales
            Dim r As Boolean
            Dim strURL As String
            Dim xfecha As String


            'log
            Dim nomreg As String
            Dim nomreg2 As String
            Dim xenv As String
            Dim l As logs
            Dim n As nombres
            l = New logs()
            l.logs(conexion)
            es = New Estandares
            es.Estandares(conexion)
            n = New nombres
            n.Nombres(conexion)

        

            'si el estandar se cerro no puedo seguir enviando

            If (es.obtenerestado(codigo) = "F") Then
                Form.Visible = False
                Button1.Visible = False
                Label2.Text = "El estandar esta fuera de vigencia, no se pueden eliminar envíos"

            End If


            'obtengo datos del envio a borrar
            u = New Envios()
            u.Envios(conexion)


            xenv = u.obtenercantidadenviada(Codigox)
            nomreg = es.obtenernombre1(codigo) + " Cant enviada: " + xenv
            nomreg2 = n.obtenernombre(es.obtenernombre(codigo)) + " - " + es.obtenerlote(codigo)

            'obtengo los datos de los viales que se habian enviado
            v = New Viales()
            v.Viales(conexion)
            'dtresultado1 = v.vialesenviados(Codigox)


            'actualizo los viales (estado y quito codigo de envio)
            'While (dtresultado1.Read())
            'v.Actualizarvial(dtresultado1(0).ToString(), dtresultado1(1).ToString(), "D", dtresultado1(3).ToString(), dtresultado1(5).ToString(), Nothing)
            'registralog("Viales", dtresultado(0).ToString(), "M", "Actualización de remanente por consumo", Nothing)
            'End While
            v.eliminaenvio(Codigox)
            'Actualizo el stock del estándar
            r = es.actualizarstock(codigo)
            If (r = "1") Then
                'log actualizacion de stock

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por cancelación de envío.", "Estandares", codigo, nomreg2)

                'elimino el envio
                u.BorrarEnvio(Codigox)

                'log eliminacion
                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Envio, Motivo: " & motivo.Value, "Envios", Codigox, nomreg)




                'redireccion
                strURL = "consulta_envios.aspx?ID=" + EncryptText(codigo)
                Response.Redirect(strURL)

            Else
                Label2.Text = "Error al actualizar stock del estandar."
            End If


        Catch ex As Exception
            Label2.Text = "Error eliminando envio."

        End Try



    End Sub

   

 

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(16384, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try

       
                codigo = DecryptText(Request.QueryString("ID")) 'codigo estandar
                Codigox = DecryptText(Request.QueryString("id2")) 'codigo consumo

                'si el estandar se cerro no puedo seguir consumiento
                Dim es As Estandares


                es = New Estandares()
                es.Estandares(conexion)
                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se pueden eliminar envios"

                End If


                Dim message As String = "Confirma la eliminacion del envío?"
                Dim sb As New System.Text.StringBuilder()
                sb.Append("return confirm('")
                sb.Append(message)
                sb.Append("');")
                ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
            Catch ex As Exception
                Label2.Text = "Error al cargar datos del estandar"
                Form.Visible = False
                Button1.Visible = False
            End Try
        End If

    End Sub
End Class
