﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" EnableEventValidation="true" CodeFile="consulta_envios.aspx.vb" Inherits="Estandares_consulta_estandares" title="Sistema Estándares" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    
    <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />
<link rel="stylesheet" href="../js/datatable/media/css/demo_page.css"type="text/css" />
<link rel="stylesheet" href="../js/datatable/ColVis/media/css/ColVis.css"type="text/css" />
    <script type="text/javascript" src="../js/datatable/media/js/jquery.js"></script>
    <script type="text/javascript" src="../js/datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
 
     <style type="text/css">
    	    input {	height: 15px;
}

        #ctl00_ContentPlaceHolder1_Div1 
{


margin:auto;

}
       </style>
       
<script language="javascript" type="text/javascript">

var asInitVals = new Array();
     $(document).ready(function () {
     
     var  oTable = $('#tbl').dataTable( {
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });
</script>
 <div id="titulo_seccion">
Administración de envíos
 </div>
<div align= "center" >
<asp:Button ID="Button1" runat="server" Text=" Buscar " />
    <asp:Button ID="Button2" 
            runat="server" Text=" + Cargar  Envios " />
            <asp:Button ID="Button3" 
            runat="server" Text="Volver " />
 </div>           
               
   
    
        <div id="Div1" style="width:50%" runat="server">
        
        
        
<br>
<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead>
                    <tr>
                        <th></th>
                        <th>Cant. Viales Enviados</th>
                        <th>Fecha Envio</th>
                        <th>Detalle</th>
                        <th>Destinatario</th>
                        
                    </tr>
                    
                    <tr>
			<td align="center"><input type="hidden" name="search_engine" value="" size="8" /></td>
			<td align="center"><input type="text" name="search_browser" value="" size="8" class="search_init" /></td>
			<td align="center"><input type="text" name="search_platform" value="" size="8" class="search_init" /></td>
			<td align="center"><input type="text" name="search_version" value="" size="8" class="search_init" /></td>
			<td align="center"><input type="text" name="search_grade" value="" size="8" class="search_init" /></td>
			
			
		</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
            
            <ItemTemplate>
                <tr>
                  <td align ="center" ><a><asp:Button ID="Button1" CommandName="Click" Text="Eliminar" runat="server" CommandArgument='<%# Eval("CodEnv") %>' /></a></td>
               <td align ="center" ><%#Eval("Cant")%></td>
                  <td align ="center" ><%#Eval("Fecha","{0:dd/MM/yyyy}")%></td>
                  <td align ="center" ><%#Eval("Detalle")%></td>
                  <td align ="center" ><%#Eval("nombre")%></td>
         
                 
              

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
       
    </div>
 
  
    </form>
       
       
  <div id="confirmacion">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>    
    
     <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div>
</asp:Content>

