﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Envios_cargar_envios
    Inherits System.Web.UI.Page
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(512, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                Dim es As Estandares
                codigo = DecryptText(Request.QueryString("ID"))
                es = New Estandares()
                es.Estandares(conexion)

             

                'si el estandar se cerro no puedo seguir consumiento

                If (es.obtenerestado(codigo) = "F") Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "El estandar esta fuera de vigencia, no se pueden realizar envios"

                End If


                'por seguridad hago doble control del sector al que pertenece el estandar
                If (es.obtenersector(codigo) <> Session("sector")) Then
                    Form.Visible = False
                    Button1.Visible = False

                    Label2.Text = "Esta intentando acceder a un estandar que no es de su sector"



                End If


                Label1.Text = ""
                Label2.Text = ""
                Label3.Text = ""
                Dim message As String = "Confirma la carga del Envio?"
                Dim sb As New System.Text.StringBuilder()
                sb.Append("return confirm('")
                sb.Append(message)
                sb.Append("');")
                ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
            Catch
                Label2.Text = "Error al cargar datos del estandar"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoenvio()
        fecha_envio.Value = ""
        detalle.Value = ""
        cantidad.Value = ""
        'Response.Redirect("../Estandares/modifica_estandar.aspx?ID=" + codigo)
    End Sub
 

    Sub nuevoenvio()
        Dim en As Envios
        Dim r As Boolean
        Dim es As Estandares
        Dim l As logs
        Dim v As Viales
        Dim nuevo_estado As String = "U"
        Dim cuenta As Integer = 0
        Dim i As Integer = 0
        Dim ide As String
        Dim est As String
        Dim nom As String
        Dim estado_viejo As String
        Dim id_v As Integer
        Dim lot As String
        Dim xfecha As String
     
        Try

            'log
            l = New logs()
            l.logs(conexion)
           
            v = New Viales()
            v.Viales(conexion)
          
            es = New Estandares()
            es.Estandares(conexion)

            'por seguridad hago doble control del sector al que pertenece el estandar

            If (es.obtenersector(codigo) = Session("sector")) Then

                'valido que no se ingresen numeros negativos o 0
                If (cantidad.Value > "0") Then

                    'debo verificar que tengo la cantidad de viales disponibles suficientes para realizar el envio
                    cuenta = es.obtenercantidadvialesdisponibles(codigo)

                    If (cantidad.Value <= cuenta) Then
                        'creo el envio
                        en = New Envios()
                        en.Envios(conexion)
                        xfecha = cambiaformatofecha2(fecha_envio.Value)
                        ide = en.Agregarenvio(xfecha, detalle.Value, cantidad.Value, destinatario.Text)
                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "(" + cantidad.Value + ") viales para el estándar:" + es.obtenernombre1(codigo), "Envios", ide, "N/A")

                        'asocio el envio a los viales y actualizo estado
                        While (i < cantidad.Value)
                            id_v = es.Obteneridvialdisponibleord(codigo)
                            estado_viejo = v.obtenerestado(id_v)
                            v.Actualizarvial(id_v, codigo, "E", v.obtenerapertura(id_v), v.obtenerremanente(id_v), ide)
                            i += 1
                        End While


                        est = es.obtenerestado(codigo)
                        nom = es.obtenernombre1(codigo)
                        lot = es.obtenerlote(codigo)


                        'Actualizo el stock del estándar

                        r = es.actualizarstock(codigo)
                        If (r = "1") Then
                            'log actualizacion de stock

                            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Actualizacion de stock por envio de: " + cantidad.Value + " vial(es).", "Estandares", codigo, nom + " - " + lot)

                            'chequeo si se acabo el estándar con este envio
                            r = es.chequeocierre(codigo)
                            If (r = False) Then
                                'log

                                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                                l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Cambio de estado de " + est + " a F", "Estandares", codigo, nom + " - " + lot)

                                Label3.Text = "Se consumió la totalidad del estándar. Se cambió a estado Cerrado"
                            End If
                            Label1.Text = "Se realizó el envío correctamente."
                        Else
                            Label2.Text = "El estándar no dispone de suficientes viales para realizar el envio"
                        End If

                    Else
                        Label2.Text = "Error al actualizar stock del estandar"
                    End If
            Else
                Label2.Text = "No puede ingresar valores negativos o nulos."

            End If
            Else
                Label2.Text = "El estandar que esta tratando acceder pertenece a otro sector."

            End If
        Catch ex As Exception
            Label2.Text = "Hubo un error en el proceso de registro del envio"
        End Try
    End Sub

  
End Class
