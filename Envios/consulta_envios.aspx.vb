﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Encryption64
Imports Excel = Microsoft.Office.Interop.Excel
Imports ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat
Imports globales
Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(1024, Session("nivel"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else
            Try
                codigo = DecryptText(Request.QueryString("ID"))
                Dim es As Estandares
                es = New Estandares()
                es.Estandares(conexion)
                If (es.obtenersector(codigo) <> Session("sector")) Then
                    Button1.Visible = False
                    Button2.Visible = False
                    Button3.Visible = False


                    Label2.Text = "El estandar que esta tratando acceder pertenece a otro sector."
                End If
            Catch
                Label2.Text = "Error al cargar datos del estandar"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If

    End Sub
    Sub buscarenvios()
        Dim u As Envios
        Try
            u = New Envios() ' nuevo objeto instancia
            u.Envios(conexion) 'invoco el constructor y paso parametros de conexion
            repeater.DataSource = u.Consultarenvio1(codigo)
            repeater.DataBind()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda de envios"


        End Try
    End Sub
   
   
    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand
        Try
            Dim Codigox As String
            If (e.CommandName = "Click") Then
                'get the id of the clicked row
                Codigox = Convert.ToString(e.CommandArgument)
                'redireccion
                Dim strURL2 As String
                strURL2 = "eliminar_envios.aspx?ID=" + EncryptText(codigo) + "&id2=" + EncryptText(Codigox)
                Response.Redirect(strURL2)
            End If
        Catch ex As Exception
            Label2.Text = "Error eliminando Envio."

        End Try
    End Sub


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("cargar_envios.aspx?ID=" + EncryptText(codigo))
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        buscarenvios()
        ' Deshabilito el boton eliminar para los usuarios nivel 2 y 4
        If (Session("nivel") = nivel2 Or Session("nivel") = nivel4) Then

            Dim ri As RepeaterItem
            Dim boton As Button
            For Each ri In repeater.Items
                boton = ri.FindControl("Button1")
                boton.Enabled = False
            Next
        Else
            Dim ri As RepeaterItem
            Dim boton As Button
            For Each ri In repeater.Items
                boton = ri.FindControl("Button1")
                ' boton.Attributes.Add("onclick", " return confirma_elimina();")
            Next
        End If
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.Redirect("../Estandares/modifica_estandar.aspx?ID=" + EncryptText(codigo))
    End Sub
End Class
